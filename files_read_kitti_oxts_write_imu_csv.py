import numpy as np
import os 

import csv

def generate_imu_csv(meas_folder, timestamp_path, out_file_path):

    with open(out_file_path, mode = 'w') as write_to:
        
        writer = csv.writer(write_to, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['timestamp','omega_x','omega_y','omega_z','alpha_x','alpha_y','alpha_z'])
        
        file_name_list = os.listdir(meas_folder)
        file_name_list = sorted(file_name_list)

        tstamp_list = []
        with open(timestamp_path) as tstamp_file:
            for line in tstamp_file.readlines():
                time = np.datetime64(line)
                tstamp = time.astype('O')
                tstamp_list.append(tstamp)

        for i, file_name in enumerate(file_name_list):
            with open(os.path.join(meas_folder,file_name)) as file_measure:
                line = file_measure.readline()
                words = line.split()
                ax = words[11]
                ay = words[12]
                az = words[13]
                wx = words[17]
                wy = words[18]
                wz = words[19]

            num_part = str(tstamp_list[i])
            writer.writerow([num_part, wx, wy, wz, ax, ay, az])
            print(file_name)
        

def main(): 
    files_parent_folder = '/media/minghanz/Seagate_Backup_Plus_Drive2/KITTI/raw_for_IMUcalib/2011_09_26/2011_09_26_drive_0001_extract/oxts'
    meas_folder = os.path.join(files_parent_folder, 'data')
    timestamp_path = os.path.join(files_parent_folder, 'timestamps.txt')
    out_file_path = os.path.join(files_parent_folder, 'imu0.csv')
    
    generate_imu_csv(meas_folder, timestamp_path, out_file_path)

if __name__ == "__main__":
    main()