########### This file is to extract points in images using the method of dso
########### subject to constraint in terms of maximum distance. 

## load bgr image and depth image
## mask the part of bgr image that is too far away (large depth)
## convert the bgr to a nparray
## 

import numpy as np
import cv2
import os
import math
from utils_load import loadFiles, loadFrameDepth

def trimPts(map_out, ys_pick, xs_pick, n_, n_wanted, rand_pattern):
    n_to_rm = n_ - n_wanted
    charTH = (n_wanted / n_)*0.95 * 255
    w = map_out.shape[1]
    h = map_out.shape[0]
    idx_to_rm = []
    i = 0
    n_added = 0
    for y, x in zip(ys_pick, xs_pick):
        
        if rand_pattern[y*w+x] > charTH:
            idx_to_rm.append(i)
            map_out[y, x] = 0
            n_added += 1
        i += 1
        if n_added >= n_to_rm:
            break
    
    ys_pick = np.delete(ys_pick, idx_to_rm, 0)
    xs_pick = np.delete(xs_pick, idx_to_rm, 0)
    
    if n_added < n_to_rm:
        n_to_rm_2 = n_to_rm - n_added
        idx_to_rm_2 = list(range(-1, -n_to_rm_2 -1, -1))
        for i in range(n_to_rm_2):
            y = ys_pick[idx_to_rm_2[i]]
            x = xs_pick[idx_to_rm_2[i]]
            map_out[y, x] = 0

        ys_pick = np.delete(ys_pick, idx_to_rm_2, 0)
        xs_pick = np.delete(xs_pick, idx_to_rm_2, 0)

    return map_out, ys_pick, xs_pick

def calcThre(hist):
    cutoff_ratio = 0.5
    cutoff_num = cutoff_ratio * hist[0] + 0.5
    acc = 0
    for i in range(len(hist)-1):
        acc += hist[i+1]
        if acc > cutoff_num:
            return i
    return len(hist) - 1

def calcHistThre(grad_mag):
    grid_w = 32
    grid_h = 32
    hist_bin = 100
    w = grad_mag.shape[1]
    h = grad_mag.shape[0]
    num_grid_w = int(w/grid_w)
    num_grid_h = int(h/grid_h)
    
    thre = np.zeros((num_grid_h, num_grid_w), np.float32)
    thre_smoothed = np.zeros((num_grid_h, num_grid_w))
    
    for i in range(num_grid_h):
        for j in range(num_grid_w):
            hist = np.zeros(hist_bin)
            for y in range(grid_h):
                for x in range(grid_w):
                    x_true = x + j * grid_w
                    y_true = y + i * grid_h
                    if x_true == 0 or x_true == w-1 or y_true == 0 or y_true == h-1:
                        continue
                    grad_cur = int(math.sqrt(grad_mag[y_true, x_true]) )
                    if grad_cur > hist_bin-2:
                        grad_cur = hist_bin-2
                    hist[grad_cur + 1] += 1
                    hist[0] += 1
            thre[i, j] = calcThre(hist)
    
    ker_smooth = np.ones((3,3), np.float32)/9.0
    
    thre_smoothed = cv2.filter2D(thre, cv2.CV_32F, ker_smooth)
    thre_smoothed = thre_smoothed * thre_smoothed
    return thre, thre_smoothed

def selectPts(img_lvls, grad_x_lvls, grad_y_lvls, grad_mag_lvls, w_lvls, h_lvls, thre_smooth, rand_pattern, img_dep = None, thFactor = 1):
    pot = 2
    use_direction = True
    h = h_lvls[0]
    w = w_lvls[0]
    h1 = h_lvls[1]
    w1 = w_lvls[1]
    h2 = h_lvls[2]
    w2 = w_lvls[2]

    map_out = np.zeros((h, w))

    ys_pick = -np.ones(40000, np.int)
    xs_pick = -np.ones(40000, np.int)
    
    n_ = 0
    n2 = 0
    n3 = 0
    n4 = 0
    directions = np.zeros((16, 2))
    directions[0] = [0, 1.0000]
    directions[1] = [0.3827, 0.9239]
    directions[2] = [0.1951, 0.9808]
    directions[3] = [0.9239, 0.3827]
    directions[4] = [0.7071, 0.7071]
    directions[5] = [0.3827, -0.9239]
    directions[6] = [0.8315, 0.5556]
    directions[7] = [0.8315, -0.5556]
    directions[8] = [0.5556, -0.8315]
    directions[9] = [0.9808, 0.1951]
    directions[10] = [0.9239, -0.3827]
    directions[11] = [0.7071, -0.7071]
    directions[12] = [0.5556, 0.8315]
    directions[13] = [0.9808, -0.1951]
    directions[14] = [1.0000, 0.0000]
    directions[15] = [0.1951, -0.9808]

    dw1 = 0.75
    dw2 = dw1*dw1
    for y4 in range(0, h, 4*pot):
        for x4 in range(0, w, 4*pot):
            my3 = min(4*pot, h - y4)
            mx3 = min(4*pot, w - x4)
            bestIdx4 = -1
            bestVal4 = 0
            dir4 = directions[rand_pattern[n2] % 16]
            for y3 in range(0, my3, 2*pot):
                for x3 in range(0, mx3, 2*pot):
                    x34 = x3 + x4
                    y34 = y3 + y4
                    my2 = min(2*pot, h - y34)
                    mx2 = min(2*pot, w - x34)
                    bestIdx3 = -1
                    bestVal3 = 0
                    dir3 = directions[rand_pattern[n2] % 16]
                    for y2 in range(0, my2, pot):
                        for x2 in range(0, mx2, pot):
                            x234 = x2 + x34
                            y234 = y2 + y34
                            my1 = min(pot, h - y234)
                            mx1 = min(pot, w - x234)
                            bestIdx2 = -1
                            bestVal2 = 0
                            dir2 = directions[rand_pattern[n2] % 16]
                            for y1 in range(0, my1):
                                for x1 in range(0, mx1):
                                    xf = x1+x234
                                    yf = y1+y234
                                    assert xf < w
                                    assert yf < h
                                    if xf<4 or xf>=w-5 or yf<4 or yf>h-4:
                                        continue
                                    if img_dep[yf, xf] > 500:
                                        continue
                                    grid_x = int(xf / 32)
                                    grid_y = int(yf / 32)
                                    pixelTH0 = thre_smooth[grid_y, grid_x]
                                    pixelTH1 = pixelTH0*dw1
                                    pixelTH2 = pixelTH1*dw2

                                    g_vec = np.array([grad_x_lvls[0][yf, xf],grad_y_lvls[0][yf, xf]])
                                    
                                    g_mag_0 = grad_mag_lvls[0][yf, xf]
                                    if g_mag_0 > pixelTH0 * thFactor:
                                        dirNorm = np.dot(g_vec, dir2)
                                        if not use_direction:
                                            dirNorm = g_mag_0
                                        if dirNorm > bestVal2:
                                            bestVal2 = dirNorm
                                            bestIdx2 = xf + w * yf
                                            bestIdx3 = -2
                                            bestIdx4 = -2
                                    if bestIdx3 == -2:
                                        continue
                                    
                                    g_mag_1 = grad_mag_lvls[1][int(yf*0.5+0.25), int(xf*0.5+0.25)]
                                    if g_mag_1 > pixelTH1*thFactor:
                                        dirNorm = np.dot(g_vec, dir3)
                                        if not use_direction:
                                            dirNorm = g_mag_1
                                        if dirNorm > bestVal3:
                                            bestVal3 = dirNorm
                                            bestIdx3 = xf + w * yf
                                            bestIdx4 = -2
                                    if bestIdx4 == -2:
                                        continue
                                    
                                    g_mag_2 = grad_mag_lvls[2][int(yf*0.25+0.125), int(xf*0.25+0.125)]
                                    if g_mag_2 >pixelTH2 * thFactor:
                                        dirNorm = np.dot(g_vec, dir4)
                                        if not use_direction:
                                            dirNorm = g_mag_2
                                        if dirNorm > bestVal4:
                                            bestVal4 = dirNorm
                                            bestIdx4 = xf + w * yf

                            if bestIdx2 > 0:
                                y_picked, x_picked = np.divmod(bestIdx2, w)
                                ys_pick[n_] = int(y_picked)
                                xs_pick[n_] = int(x_picked)
                                map_out[y_picked, x_picked] = 1
                                bestVal3 = 1e10
                                n2 += 1
                                n_ += 1
                    
                    if bestIdx3 > 0:
                        y_picked, x_picked = np.divmod(bestIdx3, w)
                        ys_pick[n_] = int(y_picked)
                        xs_pick[n_] = int(x_picked)
                        map_out[y_picked, x_picked] = 2
                        bestVal4 = 1e10
                        n3 += 1
                        n_ += 1

            if bestIdx4 > 0:
                y_picked, x_picked = np.divmod(bestIdx4, w)
                ys_pick[n_] = int(y_picked)
                xs_pick[n_] = int(x_picked)
                map_out[y_picked, x_picked] = 4
                n4 += 1
                n_ += 1
                                    
    # what to return?
    ys_pick = ys_pick[:n_]
    xs_pick = xs_pick[:n_]
    return map_out, ys_pick, xs_pick, n2, n3, n4

    


def visualize_array(img, name='img'):
    grad_x_vi = abs(img)
    max_grad_x = np.amax(grad_x_vi)
    grad_x_vi = grad_x_vi / max_grad_x*255
    grad_x_vi = np.uint8(grad_x_vi)
    
    cv2.imshow(name, grad_x_vi)
    cv2.waitKey(0)

def calcGradX(gray):
    ker_grad_x = np.array([[-1, 0, 1]])*0.5 # notice how the kernel is created
    grad_x = cv2.filter2D(gray, cv2.CV_32F, ker_grad_x)
    return grad_x

def calcGradY(gray):
    ker_grad_y = np.array([-1, 0, 1]).reshape(-1, 1)*0.5
    grad_y = cv2.filter2D(gray, cv2.CV_32F, ker_grad_y)
    return grad_y

def calcGrad(gray):
    grad_x = calcGradX(gray)
    grad_y = calcGradY(gray)
    grad_mag = grad_x * grad_x + grad_y * grad_y
    return grad_x, grad_y, grad_mag

path_carla = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0010_02')

files_depth_img, _ = loadFiles(os.path.join(path_carla, 'CameraDepth') )
frame_seqs = [int((a.split('/')[-1]).split('.')[0]) for a in files_depth_img]

files_bgr_img, _ = loadFiles(os.path.join(path_carla, 'CameraRGB') )

for i, file_bgr in enumerate(files_bgr_img):
    # Create histogram and decide threshold for each of 32*32 tiles on the image
    # Loop in each level of blobs, and find the pixel with largest gradient larger than the threshold. 
    # The gradient is calculated in different scale (image subsampled). 
    # Large gradient on finer scale is of higher priority than coarse scale. 
    # If more than needed points are selected, remove some of them by random. 
    # If less than needed points are selected, redo the sampling with smaller blobs. 

    img_dep = loadFrameDepth(files_depth_img[i])
    img_bgr = cv2.imread(file_bgr)
    img_gray = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)
    img_gray = cv2.resize(img_gray, (256, 192)) # (width, heightt), not the same as np shape sequence
    img_dep = cv2.resize(img_dep, (256, 192))

    grad_x, grad_y, grad_mag = calcGrad(img_gray)

    num_levels = 3
    img_lvls = [None]*num_levels
    grad_x_lvls = [None]*num_levels
    grad_y_lvls = [None]*num_levels
    grad_mag_lvls = [None]*num_levels
    w_lvls = [None]*num_levels
    h_lvls = [None]*num_levels

    img_lvls[0] = img_gray
    grad_x_lvls[0] = grad_x
    grad_y_lvls[0] = grad_y
    grad_mag_lvls[0] = grad_mag
    w_lvls[0] = img_gray.shape[1]
    h_lvls[0] = img_gray.shape[0]
    # cv2.imshow('img_bgr', img_lvls[0])
    # cv2.waitKey(0)
    
    for j in range(num_levels - 1):
        w_lvls[j+1] = int(w_lvls[j] *0.5)
        h_lvls[j+1] = int(h_lvls[j] *0.5)
        img_lvls[j+1] = cv2.resize(img_gray, (w_lvls[j+1], h_lvls[j+1]) )
        grad_x_lvls[j+1], grad_y_lvls[j+1], grad_mag_lvls[j+1] = calcGrad(img_lvls[j+1] )

    thre, thre_smooth = calcHistThre(grad_mag)
    rand_pattern = np.random.randint(0, 256, h_lvls[0]* w_lvls[0] )
    
    map_out, ys_pick, xs_pick, n2, n3, n4 = selectPts(img_lvls, grad_x_lvls, grad_y_lvls, grad_mag_lvls, w_lvls, h_lvls, thre_smooth, rand_pattern, img_dep)

    if n2+n3+n4 > 3500:
        map_out, ys_pick, xs_pick = trimPts(map_out, ys_pick, xs_pick, n2+n3+n4, 3500, rand_pattern)
    else:
        print('not enough pts picked! ')
        break

    img_bgr_small = cv2.resize(img_bgr, (256, 192))
    img_bgr_small[map_out==1, 2] =255
    img_bgr_small[map_out>=2, 1] =255

    idx_pick = ys_pick * w_lvls[0] + xs_pick
    idx_pick = idx_pick.astype(np.int32)
    np.save(os.path.join(path_carla, 'Top_grad_idxs', '%06d'%(frame_seqs[i]) ) , idx_pick)
    # map_vi = np.zeros_like(map_out)
    # map_vi[map_out>0]=255
    print(n2, '+', n3, '+', n4, '=', n2+n3+n4)
    print(i, ys_pick.shape[0], xs_pick.shape[0])
    
    cv2.imshow('img_bgr_small', img_bgr_small)
    cv2.waitKey(1)

    
    
    # visualize_array(map_vi, 'picked_pts')


    