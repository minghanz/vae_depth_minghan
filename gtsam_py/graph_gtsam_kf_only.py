from __future__ import print_function
import gtsam
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
from PIL import Image
import gtsam_utils

# from utils_load import *

def Vector3(x, y, z): return np.array([x, y, z])
def loadCarlaPoses(Carla_pose_file):
    with open(Carla_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames = len(list_of_lines)
        x = [None]*num_frames
        y = [None]*num_frames
        z = [None]*num_frames
        pitch_y = [None]*num_frames
        yaw_z = [None]*num_frames
        roll_x = [None]*num_frames
        pose3s = [None]*num_frames
        
        for i, line in enumerate(list_of_lines):
            words = line.split()
            x[i] = float(words[0])
            y[i] = float(words[1])
            z[i] = float(words[2])
            pitch_y[i] = float(words[3])
            roll_x[i] = float(words[4])
            yaw_z[i] = float(words[5])
            cy = math.cos(np.radians(yaw_z[i] ))
            sy = math.sin(np.radians(yaw_z[i] ))
            cr = math.cos(np.radians(roll_x[i] ))
            sr = math.sin(np.radians(roll_x[i] ))
            cp = math.cos(np.radians(pitch_y[i] ))
            sp = math.sin(np.radians(pitch_y[i] ))
            pose_cur = np.matrix(np.identity(4))
            pose_cur[0, 3] = x[i] 
            pose_cur[1, 3] = y[i]
            pose_cur[2, 3] = z[i]
            pose_cur[0, 0] = (cp * cy)
            pose_cur[0, 1] =  (cy * sp * sr - sy * cr)
            pose_cur[0, 2] = - (cy * sp * cr + sy * sr)
            pose_cur[1, 0] =  (sy * cp)
            pose_cur[1, 1] = (sy * sp * sr + cy * cr)
            pose_cur[1, 2] = (cy * sr - sy * sp * cr)
            pose_cur[2, 0] = (sp)
            pose_cur[2, 1] = -(cp * sr)
            pose_cur[2, 2] = (cp * cr)
            pose3s[i] = pose_cur
    return pose3s

def loadKeyframePoses(Keyframe_pose_file, num_frames):
    with open(Keyframe_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames_w_pose = len(list_of_lines)
        x = [None]*num_frames
        y = [None]*num_frames
        z = [None]*num_frames
        w = [None]*num_frames
        px = [None]*num_frames
        py = [None]*num_frames
        pz = [None]*num_frames
        pose3s = [None]*num_frames
        frame_idxs = [None]*num_frames_w_pose
        i = 0
        for line in list_of_lines:
            words = line.split()
            timestamp = float(words[0])
            frame_idx = int(10*timestamp)
            px[frame_idx] = float(words[1]) * 10 # the original scale from monocular odometry is not compatible with depth factor
            py[frame_idx] = float(words[2]) * 10
            pz[frame_idx] = float(words[3]) * 10
            x[frame_idx] = float(words[4])
            y[frame_idx] = float(words[5])
            z[frame_idx] = float(words[6])
            w[frame_idx] = float(words[7])
            # print("%f %f %f %f %f %f %f %f"%(timestamp[frame_idx], px[frame_idx], py[frame_idx], pz[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx], w[frame_idx]))
            rot3_current = gtsam.Rot3(w[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx])
            t3_current = gtsam.Point3(px[frame_idx], py[frame_idx], pz[frame_idx])
            pose3s[frame_idx] = gtsam.Pose3(rot3_current, t3_current)
            frame_idxs[i] = frame_idx
            i = i + 1
        return pose3s, frame_idxs, num_frames_w_pose

def loadFrameRGB(rgb_file_path, out_width = 0, out_height = 0):
    im_rgb = Image.open(rgb_file_path)
    if out_width != 0:
        im_rgb = im_rgb.resize([out_width, out_height])
    im_gray = im_rgb.convert(mode='L')
    im_gray_array = np.array(im_gray).astype(np.float32)
    # print(im_gray_array.dtype)
    # print(im_gray_array.shape)
    # print(np.amax(im_gray_array))
    return im_gray_array

def loadFrameDepth(dep_file_path, out_width = 0, out_height = 0):
    im = Image.open(dep_file_path)
    # r, g, b = im.split()
    im_array = np.array(im)
    im_array = np.float32(im_array)

    code = im_array[...,0:1] + im_array[...,1:2]*256 + im_array[...,2:3]*256*256   # 600*800*1
    num = code * 1000/(256*256*256 - 1) 
    # avg = 20.0
    # num = avg / (num + avg)
    num = np.reshape(num, (600, 800))  # 600*800

    im_depth = Image.fromarray(np.float32(num), 'F')
    if out_width != 0:
        im_depth = im_depth.resize([out_width, out_height]) # 256, 192
    im_depth_array = np.array(im_depth)    
    
    # im_depth_out = Image.fromarray(np.uint8(im_depth_array*255), 'L')
    # new_file_path = os.path.join(path_depth_imgs, 'for_visualization', depth_file)
    # im_depth_out.save(new_file_path)
    # print(depth_file, 'saved.')
    return im_depth_array

def loadFiles(path):
    files = [ f for f in os.listdir(path)  if os.path.isfile(os.path.join(path, f)) ]
    files = sorted(files)
    num_files = len(files)
    return files, num_files

def addDenseFactors(graph, pho_model, list_of_frame, list_of_key, start_idx, end_idx, path_depth_imgs, path_rgb_imgs, im_depth_array=None, im_gray_array=None):
    for i in range(start_idx, end_idx):
        if i > start_idx or im_depth_array is None:
            depth_file = '%06d.png'%(list_of_frame[i])
            rgb_file = depth_file

            dep_file_path = os.path.join(path_depth_imgs, depth_file)
            rgb_file_path = os.path.join(path_rgb_imgs, rgb_file)
            # im_depth_array = loadFrameDepth(dep_file_path, out_width = 128, out_height = 96)
            # im_gray_array = loadFrameRGB(rgb_file_path, out_width = 128, out_height = 96)
            im_depth_array = loadFrameDepth(dep_file_path, out_width = 256, out_height = 192)
            im_gray_array = loadFrameRGB(rgb_file_path, out_width = 256, out_height = 192)

        if i > start_idx:
            graph.add(gtsam.CodeCamFactor(list_of_key[i-1], list_of_key[i], 
            im_depth_array_last, im_depth_array, im_gray_array_last, im_gray_array, pho_model, 0, num_sampts))
            print('codecam factor between %d and %d added'%(list_of_key[i-1], list_of_key[i]))

        im_depth_array_last = im_depth_array.copy()
        im_gray_array_last = im_gray_array.copy()

    return graph, im_depth_array_last, im_gray_array_last

def addDensePoseFactors(graph, priorNoise, pho_model, list_of_frame, list_of_key, start_idx, end_idx, path_depth_imgs, path_rgb_imgs):
    im_depth_array_last = None
    im_gray_array_last = None

    for i in range(1, len(list_of_frame)):
        if list_of_frame[i-1] < start_idx:
            continue
        if list_of_frame[i] >= end_idx:
            break
        frame_idx_1 = list_of_frame[i-1]
        frame_idx_2 = list_of_frame[i]

        if frame_idx_2 == frame_idx_1 + 1:
            ## Add a dense factor to the large graph
            list_of_frame_cur = [ list_of_frame[i-1], list_of_frame[i] ]
            list_of_key_cur = [ list_of_key[i-1], list_of_key[i] ]
            graph, im_depth_array_last, im_gray_array_last = addDenseFactors(graph, pho_model, list_of_frame_cur, list_of_key_cur, 
            0, 2, path_depth_imgs, path_rgb_imgs, im_depth_array_last, im_gray_array_last)
        else:
            ## Initialize a temporary graph and add all frames between (including) two keyframes
            graph_cur = gtsam.NonlinearFactorGraph()
            graph_cur.add(gtsam.PriorFactorPose3(1, gtsam.Pose3(), priorNoise))
            num_frames_cur = frame_idx_2 - frame_idx_1 + 1
            list_of_frame_cur = list(range(frame_idx_1, frame_idx_2 + 1) )
            list_of_key_cur = list(range(1, num_frames_cur + 1) )
            graph_cur, _, _ = addDenseFactors(graph_cur, pho_model, list_of_frame_cur, list_of_key_cur, 
            0, num_frames_cur, path_depth_imgs, path_rgb_imgs, im_depth_array_last, im_gray_array_last)

            ## Add a dense factor between the two keyframes to the temporary graph
            list_of_frame_cur = [ list_of_frame[i-1], list_of_frame[i] ]
            list_of_key_cur = [ 1, num_frames_cur ]
            graph_cur, im_depth_array_last, im_gray_array_last = addDenseFactors(graph_cur, pho_model, list_of_frame_cur, list_of_key_cur, 
            0, 2, path_depth_imgs, path_rgb_imgs, im_depth_array_last, im_gray_array_last)
            # graph_cur.print("\nTemporal Factor Graph:\n")  # print
            
            ## Initial guess for the poses
            initialEstimate_cur = gtsam.Values()
            pose0 = gtsam.Pose3()
            pose1 = pose3s[frame_idx_1].between(pose3s[frame_idx_2])
            x_sep = pose1.x()/(num_frames_cur-1)
            y_sep = pose1.y()/(num_frames_cur-1)
            z_sep = pose1.z()/(num_frames_cur-1)
            
            for j in range(0 , num_frames_cur): #num_frames_w_pose):
                initialEstimate_cur.insert(j+1, pose0)
                pose0_t = gtsam.Point3(pose0.x() + x_sep, pose0.y() + y_sep, pose0.z() + z_sep)
                pose0 = gtsam.Pose3(pose0.rotation(), pose0_t)

            ## Set up the solver
            parameters = gtsam.GaussNewtonParams()
            parameters.relativeErrorTol = 1e-5 # Stop iterating once the change in error between steps is less than this value
            parameters.maxIterations = 100 # Do not perform more than N iteration steps
            optimizer = gtsam.GaussNewtonOptimizer(graph_cur, initialEstimate_cur, parameters) # Create the optimizer ...
            result = optimizer.optimize() # ... and optimize

            ## Add the relative pose between two keyframes to the large graph as pose factor
            marginals = gtsam.Marginals(graph_cur, result)
            cov_cur = marginals.marginalCovariance(num_frames_cur)
            pose_cur = result.atPose3(num_frames_cur)
            # print("x1 covariance:\n", cov_cur)
            # print("pose_cur:\n", pose_cur)
            model_cur = gtsam.noiseModel.Gaussian.Covariance(cov_cur)
            graph.add(gtsam.BetweenFactorPose3(list_of_key[i-1], list_of_key[i], pose_cur, model_cur))
            print('pose factor between %d  and %d added'%(list_of_key[i-1], list_of_key[i]))


    return graph

def addPoseFactors(graph, model, pose3s, list_of_frame, list_of_key, start_idx, end_idx, mode):
    if mode == 1:
        for i in range(start_idx, end_idx):
            frame_idx_1 = list_of_frame[i-1]
            frame_idx_2 = list_of_frame[i]

            pose3_between = pose3s[frame_idx_1].between( pose3s[frame_idx_2] )
            graph.add(gtsam.BetweenFactorPose3(list_of_key[i-1], list_of_key[i], pose3_between, model))
            print('pose factor between %d  and %d added'%(list_of_key[i-1], list_of_key[i]))
    else:
        for i in range(1, len(list_of_frame)):
            if list_of_frame[i-1] < start_idx:
                continue
            if list_of_frame[i] >= end_idx:
                break
            frame_idx_1 = list_of_frame[i-1]
            frame_idx_2 = list_of_frame[i]

            pose3_between = pose3s[frame_idx_1].between( pose3s[frame_idx_2] )
            graph.add(gtsam.BetweenFactorPose3(list_of_key[i-1], list_of_key[i], pose3_between, model))
            print('pose factor between %d  and %d added'%(list_of_key[i-1], list_of_key[i]))

    return graph

mode = 1 # 1: keyframe only, 2: all frames, non-keyframes with dense tracking factors, keyframes with orb pose factors
# 1. Create a factor graph container and add factors to it
graph = gtsam.NonlinearFactorGraph()

# 2a. Add a prior on the first pose, setting it to the origin
# A prior factor consists of a mean and a noise model (covariance matrix)
# priorNoise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.3, 0.3, 0.3, 0.1, 0.1, 0.1]))
priorNoise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.001, 0.001, 0.001, 0.001, 0.001, 0.001]))
graph.add(gtsam.PriorFactorPose3(1, gtsam.Pose3(), priorNoise))

# For simplicity, we will use the same noise model for odometry and loop closures
# model = gtsam.noiseModel.Diagonal.Sigmas(Vector3(0.2, 0.2, 0.1))
model = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.3, 0.3, 0.3, 0.1, 0.1, 0.1]))

# load pose file
# path_carla = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0010_02' )
path_carla = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA_frames', 'town02', 'episode_0002_01' )

path_depth_imgs = os.path.join(path_carla, 'CameraDepth')
files_depth = [ f for f in os.listdir(path_depth_imgs)  if os.path.isfile(os.path.join(path_depth_imgs, f)) ]
num_frames = len(files_depth)
print(num_frames)
path_rgb_imgs = os.path.join(path_carla, 'CameraRGB', 'image_0') ## for carla_frames
# path_rgb_imgs = os.path.join(path_carla, 'CameraRGB') ## for carla_frames

path_high_grad_pts =  os.path.join(path_carla, 'Top_grad_idxs')

## load key frame poses from ORB-SLAM
Keyframe_pose_file = '../KeyFrameTrajectory_town02.txt'
pose3s, frame_idxs, num_frames_w_pose = loadKeyframePoses(Keyframe_pose_file, num_frames)

# ## load true poses of all frames from CARLA
# Carla_pose_file = os.path.join(path_carla, 'poses.txt')
# pose3s_true = loadCarlaPoses(Carla_pose_file)
# pose3s = pose3s_true[:300:3]
# frame_idxs = list(range(0, 300, 3))

# path_jacobs = os.path.join(path_carla, 'Jacobians')
# files_jacobs = sorted(os.listdir(path_jacobs))



# 2b. Add odometry factors
# Create odometry (Between) factors between consecutive poses
# graph.add(gtsam.BetweenFactorPose2(1, 2, gtsam.Pose2(2, 0, 0), model))
# graph.add(gtsam.BetweenFactorPose2(2, 3, gtsam.Pose2(2, 0, math.pi / 2), model))
# graph.add(gtsam.BetweenFactorPose2(3, 4, gtsam.Pose2(2, 0, math.pi / 2), model))
# graph.add(gtsam.BetweenFactorPose2(4, 5, gtsam.Pose2(2, 0, math.pi / 2), model))
list_of_frame = frame_idxs
# num_frames_w_pose = 100
if mode == 1:
    start_idx = 20
    end_idx = 20 # num_frames_w_pose
    list_of_key = [None]*num_frames_w_pose
    for i in range(num_frames_w_pose):
        list_of_key[i] = i + 1
else:
    start_idx = 0
    end_idx = 50
    list_of_key = [None]*num_frames_w_pose
    for i in range(num_frames_w_pose):
        list_of_key[i] = frame_idxs[i] + 1

graph = addPoseFactors(graph, model, pose3s, list_of_frame, list_of_key, start_idx, end_idx, mode)

# codecam factor
num_sampts = 1000
measure_dim = 1000 # num_sampts
end_idx = 100 #num_frames_w_pose# num_frames#num_frames_w_pose # 121
print("num_frames_w_pose%d"%(num_frames_w_pose))
pho_model = gtsam.noiseModel.Diagonal.Sigmas(np.ones(measure_dim)) #49152 #12288 #3072
# pho_rob_model =  gtsam.noiseModel.Robust.Create(gtsam.noiseModel.mEstimator.Huber(1), pho_model)

if mode == 1:
    list_of_frame = frame_idxs
else:
    list_of_frame = [None]*end_idx
    for i in range(end_idx):
        list_of_frame[i] = i

list_of_key = [None]*end_idx
for i in range(end_idx):
    list_of_key[i] = i + 1
graph, _, _ = addDenseFactors(graph, pho_model, list_of_frame, list_of_key, 0, end_idx, path_depth_imgs, path_rgb_imgs)
# graph = addDensePoseFactors(graph, priorNoise, pho_rob_model, list_of_frame, list_of_key, 0, end_idx, path_depth_imgs, path_rgb_imgs)

# graph.add(gtsam.PriorFactorPose3(frame_idxs[num_frames_w_pose-1] + 1, pose3s[frame_idxs[num_frames_w_pose-1]], priorNoise))
# graph.print("\nFactor Graph:\n")  # print


# 2c. Add the loop closure constraint
# This factor encodes the fact that we have returned to the same pose. In real
# systems, these constraints may be identified in many ways, such as appearance-based
# techniques with camera images. We will use another Between Factor to enforce this constraint:
# graph.add(gtsam.BetweenFactorPose2(5, 2, gtsam.Pose2(2, 0, math.pi / 2), model))
# graph.print("\nFactor Graph:\n")  # print


# 3. Create the data structure to hold the initialEstimate estimate to the
# solution. For illustrative purposes, these have been deliberately set to incorrect values
# initialEstimate = gtsam.Values()
# initialEstimate.insert(1, gtsam.Pose2(0.5, 0.0, 0.2))
# initialEstimate.insert(2, gtsam.Pose2(2.3, 0.1, -0.2))
# initialEstimate.insert(3, gtsam.Pose2(4.1, 0.1, math.pi / 2))
# initialEstimate.insert(4, gtsam.Pose2(4.0, 2.0, math.pi))
# initialEstimate.insert(5, gtsam.Pose2(2.1, 2.1, -math.pi / 2))
# initialEstimate.print("\nInitial Estimate:\n")  # print
initialEstimate = gtsam.Values()
for i in range(0 , num_frames_w_pose): #num_frames_w_pose):
    initialEstimate.insert(i+1, pose3s[ frame_idxs[i] ])
    # if i == 0 and frame_idxs[i] > 0:
    #     initialEstimate.insert(1, gtsam.Pose3())
    #     if frame_idxs[i] > 1:
    #         for j in range(1, frame_idxs[i]):
    #             initialEstimate.insert(j + 1, gtsam.Pose3() ) # pose3s[ frame_idxs[i] ])
    # elif i > 0 and frame_idxs[i] > frame_idxs[i-1] + 1:
    #     for j in range(frame_idxs[i-1]+1, frame_idxs[i]):
    #         initialEstimate.insert(j + 1, pose3s[ frame_idxs[i-1] ])
    # if i == num_frames_w_pose - 1 and frame_idxs[i] < num_frames - 1:
    #     for j in range(frame_idxs[i] + 1, num_frames):
    #         initialEstimate.insert(j + 1, pose3s[ frame_idxs[i] ])
# initialEstimate.print("\nInitial Estimate:\n")  # print


# 4. Optimize the initial values using a Gauss-Newton nonlinear optimizer
# The optimizer accepts an optional set of configuration parameters,
# controlling things like convergence criteria, the type of linear
# system solver to use, and the amount of information displayed during
# optimization. We will set a few parameters as a demonstration.
parameters = gtsam.GaussNewtonParams()

# Stop iterating once the change in error between steps is less than this value
parameters.relativeErrorTol = 1e-5
# Do not perform more than N iteration steps
parameters.maxIterations = 100
# Create the optimizer ...
optimizer = gtsam.GaussNewtonOptimizer(graph, initialEstimate, parameters)
# ... and optimize
result = optimizer.optimize()
# result.print("Final Result:\n")

# 5. Calculate and print marginal covariances for all variables
marginals = gtsam.Marginals(graph, result)
# print("x1 covariance:\n", marginals.marginalCovariance(1))
# print("x2 covariance:\n", marginals.marginalCovariance(2))
# print("x3 covariance:\n", marginals.marginalCovariance(3))
# print("x4 covariance:\n", marginals.marginalCovariance(4))
# print("x5 covariance:\n", marginals.marginalCovariance(5))
# print("x1 pose:\n", result.atPose3(1))
# print("x2 pose:\n", result.atPose3(2))
# print("x3 pose:\n", result.atPose3(3))
# print("x4 pose:\n", result.atPose3(4))
# print("x5 pose:\n", result.atPose3(5))

# 6. Plot for visualization
# Declare an id for the figure
fignum = 0

fig = plt.figure(fignum)
ax = fig.gca(projection='3d')
plt.cla()

# # Plot points
# # Can't use data because current frame might not see all points
# # marginals = Marginals(isam.getFactorsUnsafe(), isam.calculateEstimate()) # TODO - this is slow
# # gtsam.plot3DPoints(result, [], marginals)
# gtsam_utils.plot3DPoints(fignum, result, 'rx')

# Plot cameras
i = 1
while result.exists(i):
    pose_i = result.atPose3(i)
    gtsam_utils.plotPose3(fignum, pose_i, axisLength=1) 
    i += 1

# draw
# ax.set_xlim3d(-40, 40)
# ax.set_ylim3d(-40, 40)
# ax.set_zlim3d(-40, 40)
plt.axis('equal')
# plt.gca().set_aspect('equal')
# plt.show()
# plt.pause(100)


fignum = 1

fig = plt.figure(fignum)
ax = fig.gca(projection='3d')
plt.cla()

# Plot cameras
i = 0
while i < num_frames_w_pose:
    pose_i = pose3s[frame_idxs[i]]
    gtsam_utils.plotPose3(fignum, pose_i, axisLength=1)
    i += 1

# draw
# ax.set_xlim3d(-40, 40)
# ax.set_ylim3d(-40, 40)
# ax.set_zlim3d(-40, 40)
plt.axis('equal')
# plt.gca().set_aspect('equal')
plt.show()
# plt.pause(100)