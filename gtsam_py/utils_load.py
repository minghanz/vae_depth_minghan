import os
import numpy as np
import math
import gtsam
from PIL import Image

def loadCarlaPoses(Carla_pose_file):
    with open(Carla_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames = len(list_of_lines)
        x = [None]*num_frames
        y = [None]*num_frames
        z = [None]*num_frames
        pitch_y = [None]*num_frames
        yaw_z = [None]*num_frames
        roll_x = [None]*num_frames
        pose3s = [None]*num_frames
        
        for i, line in enumerate(list_of_lines):
            words = line.split()
            x[i] = float(words[0])
            y[i] = float(words[1])
            z[i] = float(words[2])
            pitch_y[i] = float(words[3])
            roll_x[i] = float(words[4])
            yaw_z[i] = float(words[5])
            cy = math.cos(np.radians(yaw_z[i] ))
            sy = math.sin(np.radians(yaw_z[i] ))
            cr = math.cos(np.radians(roll_x[i] ))
            sr = math.sin(np.radians(roll_x[i] ))
            cp = math.cos(np.radians(pitch_y[i] ))
            sp = math.sin(np.radians(pitch_y[i] ))
            pose_cur = np.matrix(np.identity(4))
            pose_cur[0, 3] = x[i] 
            pose_cur[1, 3] = y[i]
            pose_cur[2, 3] = z[i]
            pose_cur[0, 0] = (cp * cy)
            pose_cur[0, 1] =  (cy * sp * sr - sy * cr)
            pose_cur[0, 2] = - (cy * sp * cr + sy * sr)
            pose_cur[1, 0] =  (sy * cp)
            pose_cur[1, 1] = (sy * sp * sr + cy * cr)
            pose_cur[1, 2] = (cy * sr - sy * sp * cr)
            pose_cur[2, 0] = (sp)
            pose_cur[2, 1] = -(cp * sr)
            pose_cur[2, 2] = (cp * cr)
            pose3s[i] = gtsam.Pose3(pose_cur)
    return pose3s

def loadKeyframePoses(Keyframe_pose_file, num_frames):
    with open(Keyframe_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames_w_pose = len(list_of_lines)
        x = [None]*num_frames
        y = [None]*num_frames
        z = [None]*num_frames
        w = [None]*num_frames
        px = [None]*num_frames
        py = [None]*num_frames
        pz = [None]*num_frames
        pose3s = [None]*num_frames
        frame_idxs = [None]*num_frames_w_pose
        i = 0
        for line in list_of_lines:
            words = line.split()
            timestamp = float(words[0])
            frame_idx = int(10*timestamp)
            px[frame_idx] = float(words[1]) * 10 # the original scale from monocular odometry is not compatible with depth factor
            py[frame_idx] = float(words[2]) * 10
            pz[frame_idx] = float(words[3]) * 10
            x[frame_idx] = float(words[4])
            y[frame_idx] = float(words[5])
            z[frame_idx] = float(words[6])
            w[frame_idx] = float(words[7])
            # print("%f %f %f %f %f %f %f %f"%(timestamp[frame_idx], px[frame_idx], py[frame_idx], pz[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx], w[frame_idx]))
            rot3_current = gtsam.Rot3(w[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx])
            t3_current = gtsam.Point3(px[frame_idx], py[frame_idx], pz[frame_idx])
            pose3s[frame_idx] = gtsam.Pose3(rot3_current, t3_current)
            frame_idxs[i] = frame_idx
            i = i + 1
        return pose3s, frame_idxs, num_frames_w_pose

def loadFrameRGB(rgb_file_path, out_width = 0, out_height = 0):
    im_rgb = Image.open(rgb_file_path)
    if out_width != 0:
        im_rgb = im_rgb.resize([out_width, out_height])
    im_gray = im_rgb.convert(mode='L')
    im_gray_array = np.array(im_gray).astype(np.float32)
    # print(im_gray_array.dtype)
    # print(im_gray_array.shape)
    # print(np.amax(im_gray_array))
    return im_gray_array

def loadFrameDepth(dep_file_path, out_width = 0, out_height = 0):
    im = Image.open(dep_file_path)
    # r, g, b = im.split()
    im_array = np.array(im)
    im_array = np.float32(im_array)

    code = im_array[...,0:1] + im_array[...,1:2]*256 + im_array[...,2:3]*256*256   # 600*800*1
    num = code * 1000/(256*256*256 - 1) 
    # avg = 20.0
    # num = avg / (num + avg)
    num = np.reshape(num, (600, 800))  # 600*800

    im_depth = Image.fromarray(np.float32(num), 'F')
    if out_width != 0:
        im_depth = im_depth.resize([out_width, out_height]) # 256, 192
    im_depth_array = np.array(im_depth)    
    
    # im_depth_out = Image.fromarray(np.uint8(im_depth_array*255), 'L')
    # new_file_path = os.path.join(path_depth_imgs, 'for_visualization', depth_file)
    # im_depth_out.save(new_file_path)
    # print(depth_file, 'saved.')
    return im_depth_array

def loadFiles(path):
    files = [ os.path.join(path, f) for f in os.listdir(path)  if os.path.isfile(os.path.join(path, f)) ]
    files = sorted(files)
    num_files = len(files)
    return files, num_files
