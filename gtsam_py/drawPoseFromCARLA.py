from __future__ import print_function
import gtsam
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
from PIL import Image
import gtsam_utils

from utils_load import loadFiles

def Vector3(x, y, z): return np.array([x, y, z])

def loadKeyframePoses(Keyframe_pose_file):
    with open(Keyframe_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames = len(list_of_lines)
        x = [None]*num_frames
        y = [None]*num_frames
        z = [None]*num_frames
        pitch_y = [None]*num_frames
        yaw_z = [None]*num_frames
        roll_x = [None]*num_frames
        pose3s = [None]*num_frames
        i = 0
        for line in list_of_lines:
            words = line.split()
            x[i] = float(words[0])
            y[i] = float(words[1])
            z[i] = float(words[2])
            pitch_y[i] = float(words[3])
            roll_x[i] = float(words[4])
            yaw_z[i] = float(words[5])
            cy = math.cos(np.radians(yaw_z[i] ))
            sy = math.sin(np.radians(yaw_z[i] ))
            cr = math.cos(np.radians(roll_x[i] ))
            sr = math.sin(np.radians(roll_x[i] ))
            cp = math.cos(np.radians(pitch_y[i] ))
            sp = math.sin(np.radians(pitch_y[i] ))
            pose_cur = np.matrix(np.identity(4))
            pose_cur[0, 3] = x[i] 
            pose_cur[1, 3] = y[i]
            pose_cur[2, 3] = z[i]
            pose_cur[0, 0] = (cp * cy)
            pose_cur[0, 1] =  (cy * sp * sr - sy * cr)
            pose_cur[0, 2] = - (cy * sp * cr + sy * sr)
            pose_cur[1, 0] =  (sy * cp)
            pose_cur[1, 1] = (sy * sp * sr + cy * cr)
            pose_cur[1, 2] = (cy * sr - sy * sp * cr)
            pose_cur[2, 0] = (sp)
            pose_cur[2, 1] = -(cp * sr)
            pose_cur[2, 2] = (cp * cr)
            pose3s[i] = gtsam.Pose3(pose_cur)
            # print("%f %f %f %f %f %f %f %f"%(timestamp[frame_idx], px[frame_idx], py[frame_idx], pz[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx], w[frame_idx]))
            # rot3_current = gtsam.Rot3.RzRyRx(yaw_z[i], pitch_y[i], roll_x[i] )
            # t3_current = gtsam.Point3(x[i], y[i], z[i])
            # pose3s[i] = gtsam.Pose3(rot3_current, t3_current)
            i = i + 1
        return pose3s, num_frames
        # return x, y, z, num_frames

path_carla = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02')
# Keyframe_pose_file = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02', 'poses.txt')
Keyframe_pose_file = os.path.join(path_carla, 'poses.txt')

pose3s, num_frames = loadKeyframePoses(Keyframe_pose_file)
# x, y, z, num_frames = loadKeyframePoses(Keyframe_pose_file)
files_depth_img, _ = loadFiles(os.path.join(path_carla, 'CameraDepth') )
frame_seqs = [int((a.split('/')[-1]).split('.')[0]) for a in files_depth_img]
pose3s = [pose3s[i] for i in frame_seqs]
num_frames = len(pose3s)

fignum = 0

fig = plt.figure(fignum)
ax = fig.gca(projection='3d')
plt.cla()

# ax.plot(x, y, z)
# Plot cameras
i = 0
while i < num_frames:
    if i % 5 == 0:
        pose_i = pose3s[i]
        gtsam_utils.plotPose3(fignum, pose_i, axisLength=0.5)
    i += 1

plt.axis('equal')
plt.gca().set_aspect('equal')
plt.gca().set_zlim(-10, 10)
plt.show()