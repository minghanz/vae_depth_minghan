from __future__ import print_function
import gtsam
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
from PIL import Image
import gtsam_utils

from utils_load import loadFiles

def Vector3(x, y, z): return np.array([x, y, z])

def poseMatFromEulerAndT(yaw_z, pitch_y, roll_x, x, y, z, radian_input=False):
    if not radian_input:
        yaw_z = np.radians(yaw_z)
        pitch_y = np.radians(pitch_y)
        roll_x = np.radians(roll_x)
        
    cy = math.cos(yaw_z )
    sy = math.sin(yaw_z )
    cr = math.cos(roll_x )
    sr = math.sin(roll_x )
    cp = math.cos(pitch_y )
    sp = math.sin(pitch_y )
    # The 4*4 pose matrix is standard (following right-handed coordinate, and all angles are counter-clockwise when positive)
    pose_cur = np.matrix(np.identity(4)) 
    pose_cur[0, 3] = x
    pose_cur[1, 3] = y
    pose_cur[2, 3] = z
    pose_cur[0, 0] = (cp * cy)
    pose_cur[0, 1] =  (cy * sp * sr - sy * cr)
    pose_cur[0, 2] = (cy * sp * cr + sy * sr)
    pose_cur[1, 0] =  (sy * cp)
    pose_cur[1, 1] = (sy * sp * sr + cy * cr)
    pose_cur[1, 2] = (-cy * sr + sy * sp * cr)
    pose_cur[2, 0] = (-sp)
    pose_cur[2, 1] = (cp * sr)
    pose_cur[2, 2] = (cp * cr)
    return pose_cur

def poseMatFromQuatAndT(qw, qx, qy, qz, x, y, z):
    pose_cur = np.matrix(np.identity(4)) 
    pose_cur[0, 3] = x
    pose_cur[1, 3] = y
    pose_cur[2, 3] = z
    pose_cur[0, 0] = 1 - 2*qy*qy - 2*qz*qz
    pose_cur[0, 1] = 2*qx*qy - 2*qz*qw
    pose_cur[0, 2] = 2*qx*qz + 2*qy*qw
    pose_cur[1, 0] = 2*qx*qy + 2*qz*qw
    pose_cur[1, 1] = 1 - 2*qx*qx - 2*qz*qz
    pose_cur[1, 2] = 2*qy*qz - 2*qx*qw
    pose_cur[2, 0] = 2*qx*qz - 2*qy*qw
    pose_cur[2, 1] = 2*qy*qz + 2*qx*qw
    pose_cur[2, 2] = 1 - 2*qx*qx - 2*qy*qy
    return pose_cur

def loadKeyframeEulerPoses(Keyframe_pose_file):
    with open(Keyframe_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames = len(list_of_lines)
        print('number of frames: %d'%(num_frames))
        x = [None]*num_frames
        y = [None]*num_frames
        z = [None]*num_frames
        pitch_y = [None]*num_frames
        yaw_z = [None]*num_frames
        roll_x = [None]*num_frames
        pose3s = [None]*num_frames
        i = 0
        for line in list_of_lines:
            words = line.split()
            x[i] = float(words[1])
            y[i] = float(words[2])
            z[i] = float(words[3])
            yaw_z[i] = float(words[4])
            pitch_y[i] = float(words[5])
            roll_x[i] = float(words[6])
            pose_cur = poseMatFromEulerAndT(yaw_z[i], pitch_y[i], roll_x[i], x[i], y[i], z[i])
            pose3s[i] = gtsam.Pose3(pose_cur)
            # print("%f %f %f %f %f %f %f %f"%(timestamp[frame_idx], px[frame_idx], py[frame_idx], pz[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx], w[frame_idx]))
            # rot3_current = gtsam.Rot3.RzRyRx(yaw_z[i], pitch_y[i], roll_x[i] )
            # t3_current = gtsam.Point3(x[i], y[i], z[i])
            # pose3s[i] = gtsam.Pose3(rot3_current, t3_current)
            i = i + 1
        return pose3s
        # return x, y, z, num_frames

def loadKeyframeQuatPoses(Keyframe_pose_file, delimiter=None, to_gtsam=True):
    with open(Keyframe_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        if delimiter is not None:
            list_of_lines = list_of_lines[1::200]
        num_frames = len(list_of_lines)
        print('number of frames: %d'%(num_frames))
        t = [None]*num_frames
        x = [None]*num_frames
        y = [None]*num_frames
        z = [None]*num_frames
        qw = [None]*num_frames
        qx = [None]*num_frames
        qy = [None]*num_frames
        qz = [None]*num_frames
        pose3s = [None]*num_frames
        i = 0
        for line in list_of_lines:
            words = line.split(delimiter)
            if delimiter is None:
                t[i] = float(words[0])
                x[i] = float(words[1])
                y[i] = float(words[2])
                z[i] = float(words[3])
                qx[i] = float(words[4])
                qy[i] = float(words[5])
                qz[i] = float(words[6])
                qw[i] = float(words[7])
            else:
                t[i] = float(words[0])
                x[i] = float(words[1])
                y[i] = float(words[2])
                z[i] = float(words[3])
                qw[i] = float(words[4])
                qx[i] = float(words[5])
                qy[i] = float(words[6])
                qz[i] = float(words[7])
            pose_cur = poseMatFromQuatAndT(qw[i], qx[i], qy[i], qz[i], x[i], y[i], z[i])
            if to_gtsam:
                pose3s[i] = gtsam.Pose3(pose_cur)
            else:
                pose3s[i] = pose_cur
            # print("%f %f %f %f %f %f %f %f"%(timestamp[frame_idx], px[frame_idx], py[frame_idx], pz[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx], w[frame_idx]))
            # rot3_current = gtsam.Rot3.RzRyRx(yaw_z[i], pitch_y[i], roll_x[i] )
            # t3_current = gtsam.Point3(x[i], y[i], z[i])
            # pose3s[i] = gtsam.Pose3(rot3_current, t3_current)
            i = i + 1
        return t, pose3s

def loadKeyframeQuatPosesSim3(Keyframe_pose_file, delimiter=None):
    with open(Keyframe_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames = len(list_of_lines)
        print('number of frames: %d'%(num_frames))
        t = [None]*num_frames
        x = [None]*num_frames
        y = [None]*num_frames
        z = [None]*num_frames
        qw = [None]*num_frames
        qx = [None]*num_frames
        qy = [None]*num_frames
        qz = [None]*num_frames
        scale = [None]*num_frames
        pose3s = [None]*num_frames
        i = 0
        for line in list_of_lines:
            words = line.split(delimiter)
            t[i] = float(words[0])
            x[i] = float(words[1])
            y[i] = float(words[2])
            z[i] = float(words[3])
            qx[i] = float(words[4])
            qy[i] = float(words[5])
            qz[i] = float(words[6])
            qw[i] = float(words[7])
            scale[i] = float(words[8])
            pose_cur = poseMatFromQuatAndT(qw[i], qx[i], qy[i], qz[i], x[i], y[i], z[i])
            print(pose_cur)
            print(pose_cur.shape)
            
            pose_cur[0:3, 0:3] = pose_cur[0:3, 0:3] * scale[i]
            pose3s[i] = pose_cur
            # print("%f %f %f %f %f %f %f %f"%(timestamp[frame_idx], px[frame_idx], py[frame_idx], pz[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx], w[frame_idx]))
            # rot3_current = gtsam.Rot3.RzRyRx(yaw_z[i], pitch_y[i], roll_x[i] )
            # t3_current = gtsam.Point3(x[i], y[i], z[i])
            # pose3s[i] = gtsam.Pose3(rot3_current, t3_current)
            i = i + 1
        return t, pose3s

def loadKeyframeMatrixPoses(Keyframe_pose_file, delimiter=None):
    # LDSO output is in this format
    with open(Keyframe_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames = len(list_of_lines)
        print('number of frames: %d'%(num_frames))
        pose3s = [None]*num_frames
        for i, line in enumerate(list_of_lines):
            words = line.split(delimiter)
            for j in range(len(words)):
                words[j] = float(words[j])
            pose_cur = np.array([ [words[1], words[2], words[3], words[4] ], 
                                  [words[5], words[6], words[7], words[8] ], 
                                  [words[9], words[10], words[11], words[12]], 
                                  [0, 0, 0, 1] ] )
            pose_cur = np.matrix(pose_cur)
            print(pose_cur)
            print(pose_cur.dtype)
            print(type(pose_cur))
            
            pose3s[i] = gtsam.Pose3(pose_cur)
    return pose3s

def loadKeyframeXYZ(Keyframe_pose_file, delimiter=None):
    # GPS position
    with open(Keyframe_pose_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames = len(list_of_lines) - 1 # exclude header
        print('number of frames: %d'%(num_frames))
        t = [None]*num_frames
        x = [None]*num_frames
        y = [None]*num_frames
        z = [None]*num_frames
        for i, line in enumerate(list_of_lines):
            if i == 0:
                continue
            words = line.split(delimiter)
            t[i-1] = float(words[0])
            x[i-1] = float(words[1])
            y[i-1] = float(words[2])
            z[i-1] = float(words[3])
            
    return t, x, y, z 

def loadCalibKITTI(calib_file):
    with open(calib_file, 'r') as file_read:
        posemat = np.identity(4)
        file_read.readline()
        R_line = file_read.readline()
        words = [ float(word) for word in R_line.split()[1:]]
        posemat[0,0] = words[0]
        posemat[0,1] = words[1]
        posemat[0,2] = words[2]
        posemat[1,0] = words[3]
        posemat[1,1] = words[4]
        posemat[1,2] = words[5]
        posemat[2,0] = words[6]
        posemat[2,1] = words[7]
        posemat[2,2] = words[8]
        T_line = file_read.readline()
        words = [ float(word) for word in T_line.split()[1:]]
        posemat[0,3] = words[0]
        posemat[1,3] = words[1]
        posemat[2,3] = words[2]
    
    print(posemat)
    return posemat

def loadIMUMeasurement(imu_file, delimiter=None):
    with open(imu_file, 'r') as file_pose:
        list_of_lines = file_pose.readlines()
        num_frames = len(list_of_lines) - 1 # exclude header
        print('number of frames: %d'%(num_frames))
        t = [None]*num_frames
        ax = [None]*num_frames
        ay = [None]*num_frames
        az = [None]*num_frames
        wx = [None]*num_frames
        wy = [None]*num_frames
        wz = [None]*num_frames
        for i, line in enumerate(list_of_lines):
            if i == 0:
                continue
            words = line.split(delimiter)
            t[i-1] = float(words[0])
            wx[i-1] = float(words[1])
            wy[i-1] = float(words[2])
            wz[i-1] = float(words[3])
            ax[i-1] = float(words[4])
            ay[i-1] = float(words[5])
            az[i-1] = float(words[6])
    return t, wx, wy, wz, ax, ay, az

def from_xyz_to_dist(x, y, z):
    len_pts = len(x)
    s = [None]*len_pts
    s[0] = 0
    for i in range(len_pts):
        if i == 0: 
            continue
        s[i] = s[i-1] + np.sqrt( (x[i] - x[i-1])**2 + (y[i] - y[i-1])**2 + (z[i] - z[i-1])**2 )
    return s

def from_pose_to_xyz(poses3d):    
    x = [pose.x() for pose in poses3d]
    y = [pose.y() for pose in poses3d]
    z = [pose.z() for pose in poses3d]
    return x, y, z

def plotTraj(fignum, x, y, z):
    fig = plt.figure(fignum)
    ax = fig.gca(projection='3d')
    plt.cla()
    ax.plot3D(x, y, z)
    ax.set_zlabel('z')
    # plt.xlabel('x')
    # plt.ylabel('y')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.axis('equal')

def plotft(fignum, t, x, on_exist=False):
    fig = plt.figure(fignum)
    ax = fig.gca()
    if not on_exist:
        plt.cla()
    ax.plot(t,x)
    # plt.axis('equal')

def plotPoses(fignum, poses, on_exist=False, axis_length=0.1):
    fig = plt.figure(fignum)
    ax = fig.gca(projection='3d')
    if not on_exist:
        plt.cla()

    # Plot cameras
    num_frames = len(poses)
    x = [None]*num_frames
    y = [None]*num_frames
    z = [None]*num_frames

    gtsam_utils.plotPose3(fignum, poses[0], axisLength=axis_length*2) # 1.5 for camOdoCal
    x[0] = poses[0].x()
    y[0] = poses[0].y()
    z[0] = poses[0].z()
    
    i = 1
    while i < num_frames:
        pose_i = poses[i]
        gtsam_utils.plotPose3(fignum, pose_i, axisLength=axis_length) # 0.5 for CamOdoCal
        x[i] = poses[i].x()
        y[i] = poses[i].y()
        z[i] = poses[i].z()
        i += 10  # 1

    # ax.plot3D(x, y, z)

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    plt.axis('equal')
    # plt.gca().set_aspect('equal')
    # plt.gca().set_zlim(-10, 10)

    # if fignum == 1:
    #     plt.gca().set_zlim(0, 3.5)

# ### This main function is for plotting pose trajectory outputted by CamOdoCal
# def main(): 
#     path_carla = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02')
#     # Keyframe_pose_file = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02', 'poses.txt')
#     pose_file = os.path.join(path_carla, 'data_for_CamOdoCal', 'cam_poses_inv.txt')

#     pose3s = loadKeyframeEulerPoses(pose_file)

#     plotPoses(0, pose3s)

#     pose_2_file = os.path.join(path_carla, 'data_for_CamOdoCal', 'odo_poses.txt')
#     pose3s_2 = loadKeyframeEulerPoses(pose_2_file)
#     plotPoses(1, pose3s_2)
#     plt.gca().set_zlim(-10, 10)
#     plt.show()

def main(): 
    # path_EuRoc = '/media/minghanz/Seagate_Backup_Plus_Drive/EuRoC_MAV_Dataset/V1_01_easy/mav0'
    # pose_VO_file = os.path.join(path_EuRoc, 'cam0', 'KeyFrameTrajectory.txt')
    # print('loading pose file 1...')
    # _, pose3s = loadKeyframeQuatPoses(pose_VO_file)
    # plotPoses(0, pose3s)

    # pose_GT_file = os.path.join(path_EuRoc, 'state_groundtruth_estimate0', 'data.csv')
    # print('loading pose file 2...')
    # _, pose3s_2 = loadKeyframeQuatPoses(pose_GT_file, ',')
    # plotPoses(1, pose3s_2)
    

    # pose_LDSO_file = '/media/minghanz/Seagate_Backup_Plus_Drive2/cam_IMU_calib_data/small/result/results_by_LDSO_4.txt'
    # print('loading pose file 1...')
    # pose3s = loadKeyframeMatrixPoses(pose_LDSO_file)
    # plotPoses(0, pose3s)

    # pose_GT_file = '/media/minghanz/Seagate_Backup_Plus_Drive2/cam_IMU_calib_data/medium/VINS_mono_fmt/pos.csv'
    # print('loading pose file 2...')
    # t, x, y, z = loadKeyframeXYZ(pose_GT_file, ',')
    # t = [ti*1e-9 for ti in t]
    # plotTraj(1, x, y, z)
    # s = from_xyz_to_dist(x, y, z)
    # plotft(1, t, s)

    # imu_file = '/media/minghanz/Seagate_Backup_Plus_Drive2/cam_IMU_calib_data/small/VINS_mono_fmt/imu0.csv'
    # # imu_file = '/media/minghanz/Seagate_Backup_Plus_Drive2/EuRoC_MAV_Dataset/V1_02_medium/mav0/imu0/data.csv'
    # print('loading imu file 2...')
    # t, wx, wy, wz, ax, ay, az = loadIMUMeasurement(imu_file, delimiter=',')
    # plotft(2, t, ax)
    # plotft(3, t, ay)
    # plotft(4, t, az)

    # pose_VO_file = '/home/minghanz/VI-Stereo-DSO/data/nt_small_C_Dframe.txt'
    # # pose_VO_file = '/home/minghanz/camIMUcalib/IMU_trajectory.txt'
    # print('loading pose file...')
    # t, pose3s = loadKeyframeQuatPoses(pose_VO_file)
    # plotPoses(0, pose3s, axis_length=0.05)
    # # x, y, z = from_pose_to_xyz(pose3s)
    # # plotTraj(0, x, y, z)
    # # s = from_xyz_to_dist(x, y, z)
    # # # s = [si*15.5 for si in s]
    # # plotft(1, t, s, on_exist=False)

    # pose_VO_file = '/home/minghanz/VI-Stereo-DSO/data/nt_small_B_Wframe.txt'
    # # pose_VO_file = '/home/minghanz/camIMUcalib/IMU_trajectory.txt'
    # print('loading pose file...')
    # t, pose3s = loadKeyframeQuatPoses(pose_VO_file)
    # plotPoses(1, pose3s, axis_length=0.5)
    # # x, y, z = from_pose_to_xyz(pose3s)
    # # plotTraj(1, x, y, z)
    # plt.show()

    # pose_VO_file = '/home/minghanz/VI-Stereo-DSO/data/nt_V1_01_easy.txt'
    # print('loading pose file...')
    # _, pose3s = loadKeyframeQuatPoses(pose_VO_file)
    # plotPoses(0, pose3s, True, axis_length=0.02 )

    # pose_VO_file = '/home/minghanz/VI-Stereo-DSO/data/nt_V1_01_easy_B_Wframe.txt'
    # print('loading pose file...')
    # _, pose3s = loadKeyframeQuatPoses(pose_VO_file)
    # plotPoses(0, pose3s, True, axis_length=0.04)

    # pose_CB_file = '/home/minghanz/VI-Stereo-DSO/data/nt_V1_01_easy_T_CB.txt'
    # print('loading pose file...')
    # _, pose3s_CB = loadKeyframeQuatPoses(pose_CB_file, to_gtsam=False)
    # for i in range(0,len(pose3s_CB),10):
    #     print(pose3s_CB[i])

    # pose_MD_file = '/home/minghanz/VI-Stereo-DSO/data/nt_V1_01_easy_T_WD.txt'
    # print('loading pose file...')
    # _, pose3s_MD = loadKeyframeQuatPosesSim3(pose_MD_file)
    # for i in range(0,len(pose3s_MD),10):
    #     print(pose3s_MD[i])

    ### generate T_BC file IMU_info in euroc style for VI-DSO from KITTI calibration file
    calib_file = '/media/minghanz/Seagate_Backup_Plus_Drive2/KITTI/raw_for_IMUcalib/2011_09_26/calib_velo_to_cam.txt'
    pose_cam_velo = loadCalibKITTI(calib_file)
    calib_file = '/media/minghanz/Seagate_Backup_Plus_Drive2/KITTI/raw_for_IMUcalib/2011_09_26/calib_imu_to_velo.txt'
    pose_velo_imu = loadCalibKITTI(calib_file)
    pose_cam_imu = np.matmul(pose_cam_velo, pose_velo_imu)
    print(pose_cam_imu)
    pose_imu_cam = np.linalg.inv(pose_cam_imu)
    print(pose_imu_cam)
    file_to_write = '/media/minghanz/Seagate_Backup_Plus_Drive2/KITTI/raw_for_IMUcalib/2011_09_26/IMU_info_imu_cam.txt'
    np.savetxt(file_to_write, pose_imu_cam)

def printmat():
    pose = poseMatFromEulerAndT(yaw_z=0.0674321, pitch_y=0.00923559 , roll_x=-1.54444, x=0.357133 , y=0.770424, z=0.508658, radian_input=True)
    print(pose)
# Driver Code 
if __name__ == '__main__': 
      
    # Calling main() function 
    main() 
    # printmat()