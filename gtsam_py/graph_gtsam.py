from __future__ import print_function
import gtsam
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
from PIL import Image
import gtsam_utils

def Vector3(x, y, z): return np.array([x, y, z])


    


# # 1. Create a factor graph container and add factors to it
# graph = gtsam.NonlinearFactorGraph()

# # 2a. Add a prior on the first pose, setting it to the origin
# # A prior factor consists of a mean and a noise model (covariance matrix)
# # priorNoise = gtsam.noiseModel.Diagonal.Sigmas(Vector3(0.3, 0.3, 0.1))
# # graph.add(gtsam.PriorFactorPose2(1, gtsam.Pose2(0, 0, 0), priorNoise))
# priorNoise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.3, 0.3, 0.3, 0.1, 0.1, 0.1]))
# graph.add(gtsam.PriorFactorPose3(1, gtsam.Pose3(), priorNoise))

# # For simplicity, we will use the same noise model for odometry and loop closures
# # model = gtsam.noiseModel.Diagonal.Sigmas(Vector3(0.2, 0.2, 0.1))
# model = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.3, 0.3, 0.3, 0.1, 0.1, 0.1]))
# pho_model = gtsam.noiseModel.Diagonal.Sigmas(np.ones(3072)) #49152 #12288
# path_depth_imgs = os.path.join('/media', 'minghanz', 'Seagate Backup Plus Drive', 'CARLA_frames', 'town01_2', 'episode_0003_02', 'CameraDepth')
path_depth_imgs = os.path.join('/media', 'minghanz', 'Seagate Backup Plus Drive', 'CARLA_frames', 'town02', 'episode_0002_01', 'CameraDepth')
depth_files = os.listdir(path_depth_imgs)
i = 1
for depth_file in depth_files:
    file_path = os.path.join(path_depth_imgs, depth_file)
    if os.path.isdir(file_path):
        continue
    im = Image.open(file_path)
    # r, g, b = im.split()
    im_array = np.array(im)
    im_array = np.float32(im_array)

    code = im_array[...,0:1] + im_array[...,1:2]*256 + im_array[...,2:3]*256*256   # 600*800*1
    num = code * 1000/(256*256*256 - 1) 
    # avg = 20.0
    # num = avg / (num + avg)
    num = np.reshape(num, (600, 800))  # 600*800

    # im_depth = Image.fromarray(np.float32(num), 'F')
    im_depth = Image.fromarray(np.int32(60*num), 'I')
    im_depth = im_depth.resize([128, 96]) # 256, 192
    im_depth_array = np.array(im_depth)
    
    # im_depth_out = Image.fromarray(np.uint8(im_depth_array*255), 'L')
    new_file_path = os.path.join(path_depth_imgs, 'for_visualization', depth_file)
    # im_depth_out.save(new_file_path)
    im_depth.save(new_file_path)
    print(depth_file, 'saved.')
    # if i >= 2:
    #     graph.add(gtsam.CodeCamFactor(i-1, i, im_depth_array_last, im_depth_array, pho_model))
    #     print('codecam factor %d added'%(i))
    # im_depth_array_last = im_depth_array.copy()
    i = i + 1

num_frames = i - 1

# # load pose file
# with open('KeyFrameTrajectory_town02.txt', 'r') as file_pose:
#     list_of_lines = file_pose.readlines()
#     num_frames_w_pose = len(list_of_lines)
#     x = [None]*num_frames
#     y = [None]*num_frames
#     z = [None]*num_frames
#     w = [None]*num_frames
#     px = [None]*num_frames
#     py = [None]*num_frames
#     pz = [None]*num_frames
#     pose3s = [None]*num_frames
#     frame_idxs = [None]*num_frames_w_pose
#     i = 0
#     for line in list_of_lines:
#         words = line.split()
#         timestamp = float(words[0])
#         frame_idx = int(10*timestamp)
#         px[frame_idx] = float(words[1])
#         py[frame_idx] = float(words[2])
#         pz[frame_idx] = float(words[3])
#         x[frame_idx] = float(words[4])
#         y[frame_idx] = float(words[5])
#         z[frame_idx] = float(words[6])
#         w[frame_idx] = float(words[7])
#         # print("%f %f %f %f %f %f %f %f"%(timestamp[frame_idx], px[frame_idx], py[frame_idx], pz[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx], w[frame_idx]))
#         rot3_current = gtsam.Rot3(w[frame_idx], x[frame_idx], y[frame_idx], z[frame_idx])
#         t3_current = gtsam.Point3(px[frame_idx], py[frame_idx], pz[frame_idx])
#         pose3s[frame_idx] = gtsam.Pose3(rot3_current, t3_current)
#         frame_idxs[i] = frame_idx
#         i = i + 1

# # 2b. Add odometry factors
# # Create odometry (Between) factors between consecutive poses
# # graph.add(gtsam.BetweenFactorPose2(1, 2, gtsam.Pose2(2, 0, 0), model))
# # graph.add(gtsam.BetweenFactorPose2(2, 3, gtsam.Pose2(2, 0, math.pi / 2), model))
# # graph.add(gtsam.BetweenFactorPose2(3, 4, gtsam.Pose2(2, 0, math.pi / 2), model))
# # graph.add(gtsam.BetweenFactorPose2(4, 5, gtsam.Pose2(2, 0, math.pi / 2), model))
# for i in range(1, num_frames_w_pose):
#     framw_idx_1 = frame_idxs[i-1]
#     framw_idx_2 = frame_idxs[i]

#     pose3_between = pose3s[framw_idx_1].between(pose3s[framw_idx_2])
#     graph.add(gtsam.BetweenFactorPose3(framw_idx_1 + 1, framw_idx_2 + 1, pose3_between, model))

# # graph.add(gtsam.PriorFactorPose3(frame_idxs[num_frames_w_pose-1] + 1, pose3s[frame_idxs[num_frames_w_pose-1]], priorNoise))
# graph.print("\nFactor Graph:\n")  # print


# # 2c. Add the loop closure constraint
# # This factor encodes the fact that we have returned to the same pose. In real
# # systems, these constraints may be identified in many ways, such as appearance-based
# # techniques with camera images. We will use another Between Factor to enforce this constraint:
# # graph.add(gtsam.BetweenFactorPose2(5, 2, gtsam.Pose2(2, 0, math.pi / 2), model))
# # graph.print("\nFactor Graph:\n")  # print


# # 3. Create the data structure to hold the initialEstimate estimate to the
# # solution. For illustrative purposes, these have been deliberately set to incorrect values
# # initialEstimate = gtsam.Values()
# # initialEstimate.insert(1, gtsam.Pose2(0.5, 0.0, 0.2))
# # initialEstimate.insert(2, gtsam.Pose2(2.3, 0.1, -0.2))
# # initialEstimate.insert(3, gtsam.Pose2(4.1, 0.1, math.pi / 2))
# # initialEstimate.insert(4, gtsam.Pose2(4.0, 2.0, math.pi))
# # initialEstimate.insert(5, gtsam.Pose2(2.1, 2.1, -math.pi / 2))
# # initialEstimate.print("\nInitial Estimate:\n")  # print
# initialEstimate = gtsam.Values()
# for i in range(0 , num_frames_w_pose):
#     initialEstimate.insert(frame_idxs[i] + 1, pose3s[ frame_idxs[i] ])
#     if i == 0 and frame_idxs[i] > 0:
#         initialEstimate.insert(1, gtsam.Pose3())
#         if frame_idxs[i] > 1:
#             for j in range(1, frame_idxs[i]):
#                 initialEstimate.insert(j + 1, gtsam.Pose3() ) # pose3s[ frame_idxs[i] ])
#     elif i > 0 and frame_idxs[i] > frame_idxs[i-1] + 1:
#         for j in range(frame_idxs[i-1]+1, frame_idxs[i]):
#             initialEstimate.insert(j + 1, pose3s[ frame_idxs[i-1] ])
#     if i == num_frames_w_pose - 1 and frame_idxs[i] < num_frames - 1:
#         for j in range(frame_idxs[i] + 1, num_frames):
#             initialEstimate.insert(j + 1, pose3s[ frame_idxs[i] ])
# initialEstimate.print("\nInitial Estimate:\n")  # print


# # 4. Optimize the initial values using a Gauss-Newton nonlinear optimizer
# # The optimizer accepts an optional set of configuration parameters,
# # controlling things like convergence criteria, the type of linear
# # system solver to use, and the amount of information displayed during
# # optimization. We will set a few parameters as a demonstration.
# parameters = gtsam.GaussNewtonParams()

# # Stop iterating once the change in error between steps is less than this value
# parameters.relativeErrorTol = 1e-5
# # Do not perform more than N iteration steps
# parameters.maxIterations = 100
# # Create the optimizer ...
# optimizer = gtsam.GaussNewtonOptimizer(graph, initialEstimate, parameters)
# # ... and optimize
# result = optimizer.optimize()
# result.print("Final Result:\n")

# # 5. Calculate and print marginal covariances for all variables
# marginals = gtsam.Marginals(graph, result)
# print("x1 covariance:\n", marginals.marginalCovariance(1))
# print("x2 covariance:\n", marginals.marginalCovariance(2))
# print("x3 covariance:\n", marginals.marginalCovariance(3))
# print("x4 covariance:\n", marginals.marginalCovariance(4))
# print("x5 covariance:\n", marginals.marginalCovariance(5))

# # 6. Plot for visualization
# # Declare an id for the figure
# fignum = 0

# fig = plt.figure(fignum)
# ax = fig.gca(projection='3d')
# plt.cla()

# # # Plot points
# # # Can't use data because current frame might not see all points
# # # marginals = Marginals(isam.getFactorsUnsafe(), isam.calculateEstimate()) # TODO - this is slow
# # # gtsam.plot3DPoints(result, [], marginals)
# # gtsam_utils.plot3DPoints(fignum, result, 'rx')

# # Plot cameras
# i = 1
# while result.exists(i):
#     pose_i = result.atPose3(i)
#     gtsam_utils.plotPose3(fignum, pose_i, 10)
#     i += 1

# # draw
# # ax.set_xlim3d(-40, 40)
# # ax.set_ylim3d(-40, 40)
# # ax.set_zlim3d(-40, 40)
# plt.axis('equal')
# # plt.gca().set_aspect('equal')
# plt.show()
# # plt.pause(100)


# fignum = 1

# fig = plt.figure(fignum)
# ax = fig.gca(projection='3d')
# plt.cla()

# # Plot cameras
# i = 0
# while i < num_frames:
#     pose_i = pose3s[i]
#     gtsam_utils.plotPose3(fignum, pose_i, 10)
#     i += 1

# # draw
# # ax.set_xlim3d(-40, 40)
# # ax.set_ylim3d(-40, 40)
# # ax.set_zlim3d(-40, 40)
# plt.axis('equal')
# # plt.gca().set_aspect('equal')
# plt.show()
# # plt.pause(100)