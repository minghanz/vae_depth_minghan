from __future__ import print_function
# import gtsam
# import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# import os
# from PIL import Image
import gtsam_utils

from utils_load import *

def Vector3(x, y, z): return np.array([x, y, z])

class DenseFacGraph():
    def __init__(self):
        self.path_carla = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0010_02' )

        print('1')
        self.files_depth_img, _ = loadFiles(os.path.join(self.path_carla, 'CameraDepth') )
        print('1')
        self.files_rgb_img, _ = loadFiles(os.path.join(self.path_carla, 'CameraRGB') )
        # path_rgb_imgs = os.path.join(path_carla, 'CameraRGB', 'image_0') ## for carla_frames
        self.files_high_grad_pts, _ = loadFiles(os.path.join(self.path_carla, 'Top_grad_idxs') )
        self.files_jacob, _ = loadFiles(os.path.join(self.path_carla, 'Jacobians') )

        self.pose_source = 'CARLA' # 'ORB-SLAM' 'CARLA'
        self.graph_mode = 1     # 1: keyframe only(keys are continuous), 
                                # 2: all frames, non-keyframes with dense tracking factors, keyframes with orb pose factors

        self.gen_keys()
        print('1')
        graph = self.graph_construct()
        print('1')
        self.graph_solve(graph)
        print('1')

    def gen_keys(self):
        if self.pose_source is 'CARLA':
            ## find out the frames with image logged:
            frame_seqs = [int((a.split('/')[-1]).split('.')[0]) for a in self.files_depth_img]
            ## load true poses of all frames from CARLA
            self.pose3s_true = loadCarlaPoses( os.path.join(self.path_carla, 'poses.txt') )
            self.pose3s_true = [self.pose3s_true[i] for i in frame_seqs]
            self.num_frames_w_true = len(self.pose3s_true)
            self.frame_idxs_true = list(range(0, self.num_frames_w_true) )

            self.frame_idxs_key = list(range(0, 300, 3))
            self.pose3s_key = self.pose3s_true[:300:3]

        elif self.pose_source is 'ORB-SLAM':
            ## load key frame poses from ORB-SLAM
            Keyframe_pose_file = 'KeyFrameTrajectory_town02.txt'
            self.pose3s_true, self.frame_idxs_true, self.num_frames_w_true = loadKeyframePoses(Keyframe_pose_file, num_frames)

            self.frame_idxs_key = self.frame_idxs_true[:100]
            self.pose3s_key = self.pose3s_true[:100]

        self.keys = list(range(1, 100 + 1) )
        self.num_frames_key = len(self.pose3s_key )
        
    def graph_construct(self):
        graph = gtsam.NonlinearFactorGraph()
        # 2a. Add a prior on the first pose, setting it to the origin
        # A prior factor consists of a mean and a noise model (covariance matrix)
        priorNoise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.001, 0.001, 0.001, 0.001, 0.001, 0.001]))
        # graph.add(gtsam.PriorFactorPose3(1, self.pose3s_key[0], priorNoise))

        # For simplicity, we will use the same noise model for odometry and loop closures
        # model = gtsam.noiseModel.Diagonal.Sigmas(Vector3(0.2, 0.2, 0.1))
        # pose_noise_model = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.3, 0.3, 0.3, 0.1, 0.1, 0.1]))
        # graph = self.addPoseFactors(graph, pose_noise_model, self.pose3s_key, self.keys)

        # codecam factor
        num_sampts = 1000
        measure_dim = 1000 # num_sampts
        pho_noise_model = gtsam.noiseModel.Diagonal.Sigmas(np.ones(measure_dim)) #49152 #12288 #3072
        # pho_rob_model =  gtsam.noiseModel.Robust.Create(gtsam.noiseModel.mEstimator.Huber(1), pho_model)
        list_of_image_file = [self.files_rgb_img[i] for i in self.frame_idxs_key]
        list_of_depth_file = [self.files_depth_img[i] for i in self.frame_idxs_key]
        graph, _, _ = self.addDenseFactors(graph, pho_noise_model, list_of_image_file, list_of_depth_file, self.keys, num_sampts)

        return graph

    def graph_solve(self, graph):
        initialEstimate = gtsam.Values()
        for i, pose in enumerate(self.pose3s_key):
            initialEstimate.insert(self.keys[i], pose)

        parameters = gtsam.GaussNewtonParams()
        # Stop iterating once the change in error between steps is less than this value
        parameters.relativeErrorTol = 1e-5
        # Do not perform more than N iteration steps
        parameters.maxIterations = 100
        # Create the optimizer ...
        optimizer = gtsam.GaussNewtonOptimizer(graph, initialEstimate, parameters)
        # ... and optimize
        result = optimizer.optimize()
        # result.print("Final Result:\n")

        # 5. Calculate and print marginal covariances for all variables
        marginals = gtsam.Marginals(graph, result)

        # 6. Plot for visualization
        # Declare an id for the figure
        fignum = 0
        fig = plt.figure(fignum)
        ax = fig.gca(projection='3d')
        plt.cla()
        # Plot cameras
        i = 1
        while result.exists(i):
            pose_i = result.atPose3(i)
            gtsam_utils.plotPose3(fignum, pose_i, axisLength=1) 
            i += 1
        plt.axis('equal')

        fignum = 1
        fig = plt.figure(fignum)
        ax = fig.gca(projection='3d')
        plt.cla()
        # Plot cameras
        i = 0
        while i < self.num_frames_key:
            pose_i = self.pose3s_key[i]
            gtsam_utils.plotPose3(fignum, pose_i, axisLength=1)
            i += 1
        plt.axis('equal')
        
        plt.show()

    def addPoseFactors(self, graph, pose_model, list_of_pose, list_of_key):
        for i in range(len(list_of_pose) - 1):
            pose3_between = list_of_pose[i].between( list_of_pose[i+1] )
            graph.add(gtsam.BetweenFactorPose3(list_of_key[i], list_of_key[i+1], pose3_between, pose_model))
            print('pose factor between %d  and %d added'%(list_of_key[i], list_of_key[i+1]) )
        return graph

    def addCodeFactors(self, graph, pho_model, list_of_image_file, list_of_depth_file, list_of_pose, list_of_key, num_sampts):
        for i in range(len(list_of_depth_file) ):
            im_depth_array = loadFrameDepth(list_of_depth_file[i], out_width = 256, out_height = 192)
            im_gray_array = loadFrameRGB(list_of_image_file[i], out_width = 256, out_height = 192)
            if i > 0:
                graph.add(gtsam.CodeCamFactor(list_of_key[i-1], list_of_key[i], 
                im_depth_array_last, im_depth_array, im_gray_array_last, im_gray_array, pho_model, 0, num_sampts))
                print('codecam factor between %d and %d added'%(list_of_key[i-1], list_of_key[i]))
            im_depth_array_last = im_depth_array.copy()
            im_gray_array_last = im_gray_array.copy()

        return graph, im_depth_array_last, im_gray_array_last

graph_instance = DenseFacGraph()