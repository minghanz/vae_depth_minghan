from __future__ import print_function
# import gtsam
# import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# import os
# from PIL import Image
import gtsam_utils

from utils_load import *

def Vector3(x, y, z): return np.array([x, y, z])

class DenseFacGraph():
    def __init__(self):
        self.pose_source = 'CARLA' # 'ORB-SLAM' 'CARLA' # currently ORB-SLAM mode does not have high_grad_pts
        self.graph_mode = 1     # 1: keyframe only(keys are continuous), 
                                # 2: all frames, non-keyframes with dense tracking factors, keyframes with orb pose factors
        self.var_mode = 'CODE' # 'CODE' 'POSE'

        if self.pose_source == 'CARLA':
            self.path_carla = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0010_02' )

            self.files_depth_img, _ = loadFiles(os.path.join(self.path_carla, 'CameraDepth') )
            self.files_rgb_img, _ = loadFiles(os.path.join(self.path_carla, 'CameraRGB') )
            self.files_high_grad_pts, _ = loadFiles(os.path.join(self.path_carla, 'Top_grad_idxs') )
            self.files_jacob, _ = loadFiles(os.path.join(self.path_carla, 'Jacobians') )
            self.files_code, _ = loadFiles(os.path.join(self.path_carla, 'Depth_code') )
        else:
            self.path_carla = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA_frames', 'town02', 'episode_0002_01' )

            self.files_depth_img, _ = loadFiles(os.path.join(self.path_carla, 'CameraDepth') )
            self.files_rgb_img, _ = loadFiles(os.path.join(self.path_carla, 'CameraRGB', 'image_0') )
        


        self.gen_keys()
        print('3')

        # factor
        num_sampts = 2000
        # The geo and pho model are the same because handled by t-distribution weighting 
        geo_noise_model = gtsam.noiseModel.Diagonal.Sigmas(np.ones(num_sampts)) #49152 #12288 #3072
        pho_noise_model = gtsam.noiseModel.Diagonal.Sigmas(np.ones(num_sampts)) #49152 #12288 #3072
        # pho_rob_model =  gtsam.noiseModel.Robust.Create(gtsam.noiseModel.mEstimator.Huber(1), pho_model)
        geo_pho_model = gtsam.noiseModel.Diagonal.Sigmas(np.ones(num_sampts*2))

        if self.var_mode == 'POSE':
            graph, initialEstimate = self.graph_construct(geo_noise_model, pho_noise_model, geo_pho_model, num_sampts)
        elif self.var_mode == 'CODE':
            graph, initialEstimate = self.graph_code_construct(geo_noise_model, pho_noise_model, geo_pho_model, num_sampts)
        print('4')
        result = self.graph_solve(graph, initialEstimate)
        print('5')

        if self.var_mode == 'POSE':
            self.pose_result_visualize(result)
        elif self.var_mode == 'CODE':
            self.code_log_txt(result)

    def gen_keys(self):
        num_k = 12
        if self.pose_source == 'CARLA':
            ## From CARLA simulation, the pose of every timestamp is logged, but only a subset of frames (with non-trivial speed) have 
            ## corresponding images logged. The timestamp of the images are reflected on their names. 

            ## find out the frames with image logged:
            frame_seqs = [int((a.split('/')[-1]).split('.')[0]) for a in self.files_depth_img] ## image numbers from CARLA simulater
            ## load true poses of all frames from CARLA
            self.pose3s_true = loadCarlaPoses( os.path.join(self.path_carla, 'poses.txt') )
            ## only preserve the frames with images, and from now on we re-number the preserved frames with continuous number. 
            ## frame_idxs_true to index pose3s_true
            ## frame_idxs_key is a subset of frame_idxs_true
            self.pose3s_true = [self.pose3s_true[i] for i in frame_seqs]
            self.num_frames_w_true = len(self.pose3s_true)
            self.frame_idxs_true = list(range(0, self.num_frames_w_true) )

            ## sample the frames every k images to go into graph.
            self.frame_idxs_key = self.frame_idxs_true[:num_k*1:1] 
            self.pose3s_key = [self.pose3s_true[i] for i in self.frame_idxs_key ] 

        elif self.pose_source == 'ORB-SLAM':
            ## In this mode, the frames with images are considered continuous in time with 0.1s interval (similar to above case after 
            ## re-numbering). However, only a subset of images have pose estimation by ORB-SLAM (keyframes). 

            ## load key frame poses from ORB-SLAM
            Keyframe_pose_file = 'KeyFrameTrajectory_town02.txt'
            ## pose3s_true are of the length of the number of images, but some of the elements are uninitialized. 
            ## frame_idxs_true of the length of the number of keyframes. 
            ## frame_idxs_true to index pose3s_true
            ## frame_idxs_key is a subset of frame_idxs_true
            self.pose3s_true, self.frame_idxs_true, self.num_frames_w_true = loadKeyframePoses(Keyframe_pose_file, len(self.files_rgb_img))

            self.frame_idxs_key = self.frame_idxs_true[:num_k]
            self.pose3s_key = [self.pose3s_true[i] for i in self.frame_idxs_key ]

        self.keys = list(range(1, num_k + 1) )
        self.num_frames_key = len(self.pose3s_key )
        
    def graph_construct(self, geo_noise_model, pho_noise_model, geo_pho_model, num_sampts):
        graph = gtsam.NonlinearFactorGraph()
        # 2a. Add a prior on the first pose, setting it to the origin
        # A prior factor consists of a mean and a noise model (covariance matrix)
        priorNoise = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.001, 0.001, 0.001, 0.001, 0.001, 0.001]))
        # graph.add(gtsam.PriorFactorPose3(1, gtsam.Pose3(), priorNoise))
        graph.add(gtsam.PriorFactorPose3(1, self.pose3s_key[0], priorNoise))

        # For simplicity, we will use the same noise model for odometry and loop closures
        # model = gtsam.noiseModel.Diagonal.Sigmas(Vector3(0.2, 0.2, 0.1))
        # pose_noise_model = gtsam.noiseModel.Diagonal.Sigmas(np.array([0.3, 0.3, 0.3, 0.1, 0.1, 0.1]))
        # graph = self.addPoseFactors(graph, pose_noise_model, self.pose3s_key, self.keys)

        list_of_image_file = [self.files_rgb_img[i] for i in self.frame_idxs_key]
        list_of_depth_file = [self.files_depth_img[i] for i in self.frame_idxs_key]

        meas_mode = 1 # 1: geo, 2: pho, 3: geo_pho
        if meas_mode == 1:
            obs_noise_model = geo_noise_model
            print('Using geo measurement.')
        elif meas_mode == 2:
            obs_noise_model = pho_noise_model
            print('Using pho measurement.')
        elif meas_mode == 3:
            obs_noise_model = geo_pho_model
            print('Using geo_pho measurement.')
        else:
            print("No correct obs mode found!")

        if self.pose_source == 'CARLA':
            list_of_high_grad_pts_file = [self.files_high_grad_pts[i] for i in self.frame_idxs_key]
            graph, _, _ = self.addDenseFactors(graph, meas_mode, obs_noise_model, list_of_image_file, list_of_depth_file, self.keys, num_sampts, list_of_high_grad_pts_file)
        elif self.pose_source == 'ORB-SLAM':
            graph, _, _ = self.addDenseFactors(graph, meas_mode, obs_noise_model, list_of_image_file, list_of_depth_file, self.keys, num_sampts)

        initialEstimate = gtsam.Values()
        for i, pose in enumerate(self.pose3s_key):
            initialEstimate.insert(self.keys[i], pose)

        return graph, initialEstimate
    
    def graph_code_construct(self, geo_noise_model, pho_noise_model, geo_pho_model, num_sampts):
        graph = gtsam.NonlinearFactorGraph()

        list_of_image_file = [self.files_rgb_img[i] for i in self.frame_idxs_key]
        list_of_depth_file = [self.files_depth_img[i] for i in self.frame_idxs_key]
        list_of_high_grad_pts_file = [self.files_high_grad_pts[i] for i in self.frame_idxs_key]
        list_of_jacob_file = [self.files_jacob[i] for i in self.frame_idxs_key]
        list_of_code_file = [self.files_code[i] for i in self.frame_idxs_key]

        meas_mode = 2 # 1: geo, 2: pho, 3: geo_pho
        if meas_mode == 1:
            obs_noise_model = geo_noise_model
            print('Using geo measurement.')
        elif meas_mode == 2:
            obs_noise_model = pho_noise_model
            print('Using pho measurement.')
        elif meas_mode == 3:
            obs_noise_model = geo_pho_model
            print('Using geo_pho measurement.')
        else:
            print("No correct obs mode found!")
        
        graph, _, _ = self.addCodeFactors(graph, meas_mode, obs_noise_model, list_of_image_file, list_of_depth_file, list_of_high_grad_pts_file, 
        list_of_jacob_file, list_of_code_file, self.pose3s_key, self.keys, num_sampts)

        graph = self.addCodeFactors_by(graph, meas_mode, obs_noise_model, list_of_image_file, list_of_depth_file, list_of_high_grad_pts_file, 
        list_of_jacob_file, list_of_code_file, self.pose3s_key, self.keys, num_sampts, inter_num=2)

        # initialEstimate = gtsam.VectorValues()
        initialEstimate = gtsam.Values()
        for i, code_file in enumerate(list_of_code_file):
            code = np.load(code_file) #loadtxt
            initialEstimate.insert(self.keys[i], code )

        return graph, initialEstimate

    def graph_solve(self, graph, initialEstimate):

        print('solve 1')
        parameters = gtsam.GaussNewtonParams()
        # Stop iterating once the change in error between steps is less than this value
        parameters.relativeErrorTol = 1e-5
        # Do not perform more than N iteration steps
        parameters.maxIterations = 100

        print('solve 2')
        # Create the optimizer ...
        optimizer = gtsam.GaussNewtonOptimizer(graph, initialEstimate, parameters)

        print('solve 3')

        # inspect the jacobian
        linear_graph = graph.linearize(initialEstimate)
        # jac_graph = linear_graph.sparseJacobian_()
        # jac_graph_file = os.path.join(self.path_carla, 'jac_graph_sparse.txt')
        # np.savetxt(jac_graph_file, jac_graph)

        # jac_graph = gtsam.Jacobian(linear_graph)
        # jac_graph_file = os.path.join(self.path_carla, 'jac_graph.txt')
        # np.savetxt(jac_graph_file, jac_graph)
        # print("jac_graph saved ", jac_graph.shape)

        # ... and optimize
        result = optimizer.optimize()
        # result.print("Final Result:\n")

        # # 5. Calculate and print marginal covariances for all variables
        # marginals = gtsam.Marginals(graph, result)

        return result

    def pose_result_visualize(self, result):
        fignum = 0
        fig = plt.figure(fignum)
        ax = fig.gca(projection='3d')
        plt.cla()
        # Plot cameras
        i = 1
        while result.exists(i):
            pose_i = result.atPose3(i)
            gtsam_utils.plotPose3(fignum, pose_i, axisLength=1) 
            i += 1
        plt.axis('equal')

        fignum = 1
        fig = plt.figure(fignum)
        ax = fig.gca(projection='3d')
        plt.cla()
        # Plot cameras
        i = 0
        while i < self.num_frames_key:
            pose_i = self.pose3s_key[i]
            gtsam_utils.plotPose3(fignum, pose_i, axisLength=1)
            i += 1
        plt.axis('equal')
        
        plt.show()
    def code_log_txt(self, result):
        i = 1
        while result.exists(i):
            # code_i = result.at(i)
            code_i = result.atVector(i)
            # gtsam_utils.plotPose3(fignum, pose_i, axisLength=1) 
            result_file = os.path.join(self.path_carla, 'Opti_code', '%06d'%(self.frame_idxs_key[i-1]))
            np.save(result_file, code_i) # savetxt
            i += 1
        
        print('Code files saved')

    def addPoseFactors(self, graph, pose_model, list_of_pose, list_of_key):
        for i in range(len(list_of_pose) - 1):
            pose3_between = list_of_pose[i].between( list_of_pose[i+1] )
            graph.add(gtsam.BetweenFactorPose3(list_of_key[i], list_of_key[i+1], pose3_between, pose_model))
            print('pose factor between %d  and %d added'%(list_of_key[i], list_of_key[i+1]) )
        return graph

    def addDenseFactors(self, graph, meas_mode, meas_err_model, list_of_image_file, list_of_depth_file, list_of_key, num_sampts, list_of_high_grad_pts_file = None):
        for i in range(len(list_of_depth_file) ):
            im_depth_array = loadFrameDepth(list_of_depth_file[i], out_width = 256, out_height = 192)
            im_gray_array = loadFrameRGB(list_of_image_file[i], out_width = 256, out_height = 192)
            if list_of_high_grad_pts_file is not None:
                high_grad_pts = np.load(list_of_high_grad_pts_file[i]) # loadtxt
                if i > 0:
                    graph.add(gtsam.CamPoseFactor(list_of_key[i-1], list_of_key[i], 
                    im_depth_array_last, im_depth_array, im_gray_array_last, im_gray_array, high_grad_pts, meas_err_model, meas_mode, num_sampts))
                    print('posecam factor between %d and %d added'%(list_of_key[i-1], list_of_key[i]))
                im_depth_array_last = im_depth_array.copy()
                im_gray_array_last = im_gray_array.copy()
                high_grad_pts_last = high_grad_pts.copy()
            else:
                if i > 0:
                    graph.add(gtsam.CamPoseFactor(list_of_key[i-1], list_of_key[i], 
                    im_depth_array_last, im_depth_array, im_gray_array_last, im_gray_array, meas_err_model, meas_mode, num_sampts))
                    print('posecam factor between %d and %d added'%(list_of_key[i-1], list_of_key[i]))
                im_depth_array_last = im_depth_array.copy()
                im_gray_array_last = im_gray_array.copy()

        return graph, im_depth_array_last, im_gray_array_last
    
    def addCodeFactors(self, graph, meas_mode, meas_err_model, list_of_image_file, list_of_depth_file, list_of_high_grad_pts_file, 
        list_of_jacob_file, list_of_code_file, list_of_pose3, list_of_key, num_sampts):
        for i in range(len(list_of_depth_file) ):
            im_depth_array = loadFrameDepth(list_of_depth_file[i], out_width = 256, out_height = 192)
            im_gray_array = loadFrameRGB(list_of_image_file[i], out_width = 256, out_height = 192)
            high_grad_pts = np.load(list_of_high_grad_pts_file[i]) # loadtxt
            jacob = np.load(list_of_jacob_file[i]) # 2000 * 256 # loadtxt
            code = np.load(list_of_code_file[i]) # loadtxt

            if i > 0:
                graph.add(gtsam.CamCodeFactor(list_of_key[i], 
                im_depth_array_last, im_depth_array, im_gray_array_last, im_gray_array, high_grad_pts, 
                jacob, code, list_of_pose3[i-1], list_of_pose3[i], meas_err_model, meas_mode, num_sampts))
                print('codecam factor between %d and %d added'%(list_of_key[i-1], list_of_key[i]))

                graph.add(gtsam.CamCodeFactor(list_of_key[i-1], 
                im_depth_array, im_depth_array_last, im_gray_array, im_gray_array_last, high_grad_pts_last, 
                jacob_last, code_last, list_of_pose3[i], list_of_pose3[i-1], meas_err_model, meas_mode, num_sampts))
                print('codecam factor between %d and %d added'%(list_of_key[i], list_of_key[i-1]))

            im_depth_array_last = im_depth_array.copy()
            im_gray_array_last = im_gray_array.copy()
            high_grad_pts_last = high_grad_pts.copy()
            jacob_last = jacob.copy()
            code_last = code.copy()



        return graph, im_depth_array_last, im_gray_array_last

    def addCodeFactors_by(self, graph, meas_mode, meas_err_model, list_of_image_file, list_of_depth_file, list_of_high_grad_pts_file, 
        list_of_jacob_file, list_of_code_file, list_of_pose3, list_of_key, num_sampts, inter_num):
        for i in range(len(list_of_depth_file) -inter_num ):
            im_depth_array_1 = loadFrameDepth(list_of_depth_file[i], out_width = 256, out_height = 192)
            im_gray_array_1 = loadFrameRGB(list_of_image_file[i], out_width = 256, out_height = 192)
            high_grad_pts_1 = np.load(list_of_high_grad_pts_file[i]) # loadtxt
            jacob_1 = np.load(list_of_jacob_file[i]) # 2000 * 256 # loadtxt
            code_1 = np.load(list_of_code_file[i]) # loadtxt

            im_depth_array_2 = loadFrameDepth(list_of_depth_file[i+inter_num], out_width = 256, out_height = 192)
            im_gray_array_2 = loadFrameRGB(list_of_image_file[i+inter_num], out_width = 256, out_height = 192)
            high_grad_pts_2 = np.load(list_of_high_grad_pts_file[i+inter_num]) # loadtxt
            jacob_2 = np.load(list_of_jacob_file[i+inter_num]) # 2000 * 256 # loadtxt
            code_2 = np.load(list_of_code_file[i+inter_num]) # loadtxt

            graph.add(gtsam.CamCodeFactor(list_of_key[i+inter_num], 
            im_depth_array_1, im_depth_array_2, im_gray_array_1, im_gray_array_2, high_grad_pts_2, 
            jacob_2, code_2, list_of_pose3[i], list_of_pose3[i+inter_num], meas_err_model, meas_mode, num_sampts))
            print('codecam factor between %d and %d added'%(list_of_key[i], list_of_key[i+inter_num]))

            graph.add(gtsam.CamCodeFactor(list_of_key[i], 
            im_depth_array_2, im_depth_array_1, im_gray_array_2, im_gray_array_1, high_grad_pts_1, 
            jacob_1, code_1, list_of_pose3[i+inter_num], list_of_pose3[i], meas_err_model, meas_mode, num_sampts))
            print('codecam factor between %d and %d added'%(list_of_key[i+inter_num], list_of_key[i]))

        return graph


graph_instance = DenseFacGraph()