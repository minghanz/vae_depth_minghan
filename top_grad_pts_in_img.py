########### This file is to extract the coordinate of top x points with largest gradient magnitude
########### in images subject to constraint in terms of maximum distance. 

## load rgb image and depth image
## mask the part of rgb image that is too far away (large depth)
## convert the rgb to a nparray
## 
import cv2
import numpy as np
import os

home = os.path.expanduser('~')
carla_path = os.path.join(home, 'Data', 'CARLA', '_out', 'episode_0010_02') # on mcity server')
rgb_folder = os.path.join(carla_path, 'CameraRGB')
depth_folder = os.path.join(carla_path, 'CameraDepth')
pt_coord_folder = os.path.join(carla_path, 'Top_grad_pts')
pt_idx_folder = os.path.join(carla_path, 'Top_grad_idxs')

files_rgb = os.listdir(rgb_folder)
files_rgb = sorted(files_rgb)
files_depth = os.listdir(depth_folder)
files_depth = sorted(files_depth)

for i, (file_rgb, file_depth) in enumerate(zip(files_rgb, files_depth)):
    ## calculate gradient
    file_path = os.path.join(rgb_folder, file_rgb)
    img = cv2.imread(file_path)
    img = cv2.resize( img, (256, 192) )
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    sobel_x = cv2.Sobel(img_gray, cv2.CV_32F, 1, 0, ksize=3 )
    sobel_y = cv2.Sobel(img_gray, cv2.CV_32F, 0, 1, ksize=3 )
    # cv2.imshow("sobel_x", sobel_x)
    # cv2.imshow("sobel_y", sobel_y)
    # cv2.waitKey(0)
    sobel_mag = np.sqrt(sobel_x**2 + sobel_y**2)

    ## mask out far points
    file_depth_path = os.path.join(depth_folder, file_depth)
    img_depth = cv2.imread(file_depth_path).astype(np.float32)
    img_depth = (img_depth[:,:,0]*256*256 + img_depth[:,:,1]*256 + img_depth[:,:,2])/(256*256*256)*1000
    img_depth = cv2.resize(img_depth, (256, 192) )
    max_depth = np.amax(img_depth)
    # print(max_depth)
    mask = img_depth > 200
    mask_img = np.zeros( (192, 256) ).astype(np.uint8)
    mask_img[mask]= 255
    mask_img = cv2.dilate(mask_img, kernel = np.ones((5,5),np.uint8) )
    mask = mask_img == 255
    sobel_mag[mask] = 0
    # cv2.imshow("mask", mask_img)
    # cv2.waitKey(0)

    ## sort the gradient
    sobel_mag_flat = sobel_mag.reshape(-1)
    sobel_mag_sort = np.argsort(sobel_mag_flat)[::-1]
    sobel_mag_sort = sobel_mag_sort[:2000].astype(int)

    ## visualize selected points
    # sobel_mag_row, sobel_mag_col = np.divmod(sobel_mag_sort, sobel_mag.shape[1] )
    # sobel_mag_coord = np.stack((sobel_mag_row, sobel_mag_col), -1)
    # for j in range(sobel_mag_row.shape[0]):
    #     cv2.circle(img, (sobel_mag_col[j], sobel_mag_row[j]), 2, (0,0,255) )
    # cv2.imshow("output", img)
    # cv2.waitKey(0)
    
    # np.savetxt(os.path.join(pt_coord_folder, file_rgb.split('.')[0]+'.txt'),sobel_mag_coord, fmt='%i' )
    np.savetxt(os.path.join(pt_idx_folder, file_rgb.split('.')[0]+'.txt'), sobel_mag_sort, fmt='%i' )
    print(file_rgb)
    

    
