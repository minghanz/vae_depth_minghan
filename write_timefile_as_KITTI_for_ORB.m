% KITTI mode
fid = fopen('times.txt', 'w');

for i = 1:4146
    fprintf(fid, "%f\n", (i-1)*0.1);
end
fclose(fid);

% %TUM mode
% fid = fopen('rgb.txt', 'w');
% for i = 1:1301
%     fprintf(fid, "%f CameraRGB/image_0/%06d.png\n", (i-1)*0.1, i-1);
% end
% fclose(fid);
% 
% fid = fopen('depth.txt', 'w');
% for i = 1:1301
%     fprintf(fid, "%f CameraDepth/for_visualization/%06d.png\n", (i-1)*0.1, i-1);
% end
% fclose(fid);
% 
% fid = fopen('CARLA_town02.txt', 'w');
% for i = 1:1301
%     fprintf(fid, "%f CameraRGB/image_0/%06d.png %f CameraDepth/for_visualization/%06d.png\n", (i-1)*0.1, i-1, (i-1)*0.1, i-1);
% end
% fclose(fid);