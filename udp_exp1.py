import socket
import os
import struct

def chunkstring(string, length):
    return (string[0+i:length+i] for i in range(0, len(string), length))

my_socket= socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
my_socket.bind(('127.0.0.1', 8821))
my_socket.connect(('127.0.0.1', 8822))

img_folder = '/media/minghanz/Seagate Backup Plus Drive/CARLA_generated_data/_out/episode_0001/CameraRGB'
files_img = os.listdir(img_folder)
for file in files_img:
    file_path = os.path.join(img_folder, file)
    file_id =open(file_path, "rb")               #read image
    file_data = file_id.read()
    size = len(file_data)
    print(size)
    print(type(file_data))

    file_splits = list(chunkstring(file_data, 8192))

    num_split = len(file_splits)
    print(num_split)
    print(type(struct.pack(">i", num_split)))
    my_socket.send(struct.pack(">i", num_split))  # should use big-endian to let matlab properly understand the number (the sequence of four bytes)
                                                  # strangely we do not have to specify this in FIFO case
    for i in range(num_split):
        my_socket.send(file_splits[i])

    # my_socket.send(file_data[0:60000])
    # my_socket.send(file_data[60000:120000])
    # my_socket.send(file_data[120000:size])
    
    print(file_path)
    data, addr = my_socket.recvfrom(1024)
    print(data)
    

# # in Python3, a str object should be converted to bytes object before sent through UDP.
# MESSAGE='test1'
# for i in range(10):
#     print(type(MESSAGE))
#     print(len(MESSAGE))
#     my_socket.send(MESSAGE)
#     print(i)
#     data, addr = my_socket.recvfrom(1024)
#     print(data)


my_socket.close