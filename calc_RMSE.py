
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
# from scipy.misc import imsave as ims
# from utils import *

# for saving depth prediction to disk
from PIL import Image

def loadFrameDepth(dep_file_path, out_width = 0, out_height = 0):
    im = Image.open(dep_file_path)
    # r, g, b = im.split()
    im_array = np.array(im)
    im_array = np.float32(im_array)

    code = im_array[...,0:1] + im_array[...,1:2]*256 + im_array[...,2:3]*256*256   # 600*800*1
    num = code * 1000/(256*256*256 - 1) 
    # inverse depth
    # num = 1/num 
    # avg = 20.0
    # num = avg / (num + avg)
    num = np.reshape(num, (num.shape[0], num.shape[1]))  # 600*800

    im_depth = Image.fromarray(np.float32(num), 'F')
    if out_width != 0:
        im_depth = im_depth.resize([out_width, out_height]) # 256, 192
    im_depth_array = np.array(im_depth)    
    
    # im_depth_out = Image.fromarray(np.uint8(im_depth_array*255), 'L')
    # new_file_path = os.path.join(path_depth_imgs, 'for_visualization', depth_file)
    # im_depth_out.save(new_file_path)
    # print(depth_file, 'saved.')
    return im_depth_array

def loadFrameDepth2(dep_file_path, out_width = 0, out_height = 0):
    im = Image.open(dep_file_path)
    # r, g, b = im.split()
    im_array = np.array(im)
    num = np.float32(im_array)

    # num = 1/im_array
    # avg = 20.0
    # num = avg / (num + avg)
    num = np.reshape(num, (num.shape[0], num.shape[1]))  # 600*800

    im_depth = Image.fromarray(np.float32(num), 'F')
    if out_width != 0:
        im_depth = im_depth.resize([out_width, out_height]) # 256, 192
    im_depth_array = np.array(im_depth)    
    
    # im_depth_out = Image.fromarray(np.uint8(im_depth_array*255), 'L')
    # new_file_path = os.path.join(path_depth_imgs, 'for_visualization', depth_file)
    # im_depth_out.save(new_file_path)
    # print(depth_file, 'saved.')
    return im_depth_array

def RMSE(dep_truth, dep_pred):
    rows = dep_truth.shape[0]
    cols = dep_truth.shape[1]
    sum_err2 = 0
    num_err2 = 0
    sum_true = 0
    sum_pred = 0
    for i in range(rows):
        for j in range(cols):
            if dep_truth[i,j] > 100 or dep_pred[i,j] < 10:
            # if dep_truth[i,j] > 900 or dep_pred[i,j] > 900:
                continue
            sum_true += dep_truth[i,j]
            sum_pred += 1/dep_pred[i,j]

    scale = sum_true / sum_pred    

    for i in range(rows):
        for j in range(cols):
            # if dep_truth[i,j] > 900 or dep_pred[i,j] > 900:
            if dep_truth[i,j] > 100 or dep_pred[i,j] < 10:
                continue
            cur_pred = 1/dep_pred[i,j] * scale
            sum_err2 += ( dep_truth[i,j] - cur_pred ) * ( dep_truth[i,j] - cur_pred )
            num_err2 += 1
    
    rmse = np.sqrt(sum_err2 / num_err2)
    return rmse


depth_path_true = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02', 'CameraDepth')
# depth_path_pred = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02', 'vae_depth_output')
depth_path_pred = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', 'lsd_depth_input_visualize')
depth_path_slam = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', 'lsd_depth_output_visualize')

depth_files_slam = os.listdir(depth_path_slam)
depth_files_pred = os.listdir(depth_path_pred)
depth_files_true = os.listdir(depth_path_true)

num_eval = len(depth_files_slam)
rmse_list_slam = [None]*num_eval
rmse_list_pred = [None]*num_eval

i = 0
for depth_file_slam in depth_files_slam:
    list_str = depth_file_slam.split('.')
    seq_id = int(list_str[0])
    # print(seq_id)
    depth_file_pred = depth_files_pred[seq_id]
    depth_file_true = depth_files_true[seq_id]

    depth_true = loadFrameDepth( os.path.join(depth_path_true, depth_file_true), 640, 480 )
    depth_pred = loadFrameDepth2( os.path.join(depth_path_pred, depth_file_pred), 640, 480 )
    depth_slam = loadFrameDepth2( os.path.join(depth_path_slam, depth_file_slam), 640, 480 )
    

    rmse_pred = RMSE(depth_true, depth_pred)
    rmse_slam = RMSE(depth_true, depth_slam)
    
    rmse_list_pred[i] = rmse_pred
    rmse_list_slam[i] = rmse_slam
    print("%d %f %f "%(i, rmse_list_pred[i], rmse_list_slam[i]))
    i += 1
    
rmse_list_pred = np.array(rmse_list_pred)
rmse_list_slam = np.array(rmse_list_slam)

mrmse_pred = np.mean(rmse_list_pred)
mrmse_slam = np.mean(rmse_list_slam)

print(mrmse_pred)
print(mrmse_slam)