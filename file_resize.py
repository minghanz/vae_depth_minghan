import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image

# path_depth_imgs = os.path.join('/media', 'minghanz', 'Seagate Backup Plus Drive', 'CARLA_frames', 'town02', 'episode_0002_01', 'CameraRGB', 'image_0')
path_depth = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive2', 'cam_IMU_calib_data', 'medium', 'VINS_mono_fmt')
path_depth_imgs = os.path.join(path_depth, 'cam0')

depth_files = os.listdir(path_depth_imgs)
i = 1
for depth_file in depth_files:
    file_path = os.path.join(path_depth_imgs, depth_file)
    if os.path.isdir(file_path):
        continue
    im = Image.open(file_path)
    # im = im.resize([640, 480]) # 256, 192 [128, 96]
    # new_file_path = os.path.join(path_depth_imgs, '480p_for_lsd', depth_file)
    im = im.resize([960, 540]) # 256, 192 [128, 96]
    new_file_path = os.path.join(path_depth, 'cam0_540p_for_VI-DSO', depth_file)

    im.save(new_file_path)
    print(i, ': ', depth_file, 'saved.')
    i = i + 1