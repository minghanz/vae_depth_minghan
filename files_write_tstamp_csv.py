# generate tstamp file (data.csv) in the format of euroc (to run VI-DSO)
import os 
import shutil

import csv

def generate_tstamp_csv(image_folder, out_file_path):

    with open(out_file_path, mode = 'w') as write_to:
        
        writer = csv.writer(write_to, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['#timestamp [ns]','filename'])
        
        file_name_list = os.listdir(image_folder)
        file_name_list = sorted(file_name_list)

        for file_name in file_name_list:
            num_part = file_name.split('.')[0]
            writer.writerow([num_part, file_name])
            print(file_name)
        

def main(): 
    files_parent_folder = '/media/minghanz/Seagate_Backup_Plus_Drive2/cam_IMU_calib_data/medium/VINS_mono_fmt'
    image_folder = os.path.join(files_parent_folder, 'cam0')
    out_file_path = os.path.join(files_parent_folder, 'cam0_tstamp.csv')
    
    generate_tstamp_csv(image_folder, out_file_path)

if __name__ == "__main__":
    main()