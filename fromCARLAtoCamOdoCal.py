# Create an even file that can be read by CamOdoCal form the image category and pose.txt file (ground truth pose from CARLA) . 
# The format of CARLA: x y z pitch roll yaw
# The format of wanted CamOdoCal event file: 
# [timestamp] CAM [cam_id(0/1/2/3/...)] [file_name(***.jpg)]
# [timestamp] IMU [quaternion x] [quaternion y] [quaternion z] [quaternion w]
# [timestamp] GPS [lat] [lon] [alt]
# [timestamp] POS x y z yaw pitch roll


import shutil
import os 
import sys
import glob

## initialize the paths
# image folder path: /media/minghanz/Seagate_Backup_Plus_Drive/CARLA/_out/episode_0000_02
sequence_path = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02')
image_folder_path = os.path.join(sequence_path, 'CameraRGB')
pose_txt_path = os.path.join(sequence_path, 'poses.txt')

output_dat_path = os.path.join(sequence_path, 'event.txt')

## loading
image_files = os.listdir(image_folder_path)

pose_file = open(pose_txt_path, 'r')
pose_lines = pose_file.readlines()

event_file = open(output_dat_path, 'w')

pre_pose_num = 4
start_count = 400
end_count = 550
psudo_intv = 30
frame_num_0 = int(image_files[start_count].split('.')[0])
for i in range(pre_pose_num):
    pose_line = pose_lines[frame_num_0 - pre_pose_num + i]
    pose_items = pose_line.split()
    x = pose_items[0]
    y = pose_items[1]
    z = pose_items[2]
    pitch = pose_items[3]
    roll = pose_items[4]
    yaw = pose_items[5]

    pose_line_to_write = ' '.join( [str(i*psudo_intv), 'POSE', x, y, z, yaw, pitch, roll] )
    event_file.write(pose_line_to_write)
    event_file.write('\n')

for count, image_file_name in enumerate(image_files):
    if count < start_count:
        continue
    if count >= end_count:
        break
    frame_num = int(image_file_name.split('.')[0])

    pose_line = pose_lines[frame_num]
    pose_items = pose_line.split()
    x = pose_items[0]
    y = pose_items[1]
    z = pose_items[2]
    pitch = pose_items[3]
    roll = pose_items[4]
    yaw = pose_items[5]

    cam_line_to_write = ' '.join( [str((count - start_count + pre_pose_num)*psudo_intv), 'CAM', str(0), image_file_name ] )
    pose_line_to_write = ' '.join( [str((count - start_count + pre_pose_num)*psudo_intv), 'POSE', x, y, z, yaw, pitch, roll] )
    event_file.write(cam_line_to_write)
    event_file.write('\n')
    event_file.write(pose_line_to_write)
    event_file.write('\n')

pose_file.close()
event_file.close()