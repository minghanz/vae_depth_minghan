import tensorflow as tf
import numpy as np
# import matplotlib.pyplot as plt
import os
import sys
# from scipy.misc import imsave as ims
# from utils import *
from ops import *

from tensorflow.python import debug as tf_debug

# for saving depth prediction to disk
from PIL import Image
# for communication
import socket
import struct


class vae_depth():
    def __init__(self):

        self.batchsize = 6

        # specifying size of image
        self.im_width = 256 # 28 256
        self.im_height = 192 # 28 192
        self.seman_category = 13

        self.sem_coef = 1000000 # 10000
        self.clip_depth_sigma = True #should be set to true if dual unet is false
        self.clip_z = False
        self.dense_z = True # use fully connected layers in latent prediction
        self.encoder_cond = False # concat image features to depth encoder
        self.lin_decoder = True # linear decoder of vae

        self.usr_name = "minghanz"#'pingping' # 'minghanz'

        self.next_batch_depth, self.next_batch_gray, self.next_batch_seman = self.read_carla_tfrecord()

        ############ construct the graph                    
        dh_list, dh_last, dh_sigma, dh_last_sem, dh_sem = self.graph_unet(self.next_batch_gray)
        self.graph(self.next_batch_depth, dh_list, dh_last, batch_truth = self.next_batch_seman, dh_sigma=dh_sigma, sem_from_unet=dh_last_sem, sem_mid_unet=dh_sem)
               

    def read_carla_tfrecord(self):
        # file path
        home = os.path.expanduser('~')

        # # for running the 100k data from carla
        # carla_path = os.path.join('/media', self.usr_name, 'Seagate_Backup_Plus_Drive', 'CARLA_tfrecords' )# , 'small_subset'
        # tfrecord_train = os.path.join(carla_path, 'images_train.tfrecords')
        # tfrecord_val = os.path.join(carla_path, 'images_val.tfrecords')

        # for running the test sequence for lsd_slam (20190328)
        # self.carla_path = os.path.join(home, 'Data', 'CARLA', '_out', 'episode_0010_02') # on mcity server
        self.carla_path = os.path.join('/mnt', 'storage', 'minghanz_data', 'CARLA', '_out', 'episode_0010_02') # on mcity server
        # self.carla_path = os.path.join('/media', self.usr_name, 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0010_02') # on my laptop
        record_folder = os.path.join(self.carla_path, 'tf_records')
        tfrecord_train = os.path.join(record_folder, 'images_seq.tfrecords')
        tfrecord_val = os.path.join(record_folder, 'images_seq.tfrecords')

        raw_train_set = tf.data.TFRecordDataset(tfrecord_train)
        raw_val_set = tf.data.TFRecordDataset(tfrecord_val)

        # raw_train_set = raw_train_set.shuffle(buffer_size=1000)
        # raw_val_set = raw_val_set.shuffle(buffer_size=1000)
        
        # Create a dictionary describing the features.  
        self.image_feature_description = {
            'img_depth': tf.FixedLenFeature([], tf.string),
            'img_rgb': tf.FixedLenFeature([], tf.string),
            'img_seman': tf.FixedLenFeature([], tf.string),
        }

        parsed_train_set = raw_train_set.map(self._parse_image_function)
        parsed_val_set = raw_val_set.map(self._parse_image_function)

        # extract training data
        batched_train_set = parsed_train_set.repeat().batch(self.batchsize)
        self.train_iter = batched_train_set.make_one_shot_iterator()

        # extract validation data
        batched_valid_set = parsed_val_set.repeat().batch(self.batchsize)
        self.valid_iter = batched_valid_set.make_one_shot_iterator()

        # construct a common iterator handle to allow switching between iterators of different dataset
        self.handle = tf.placeholder(tf.string, shape=[])
        iter_generic = tf.data.Iterator.from_string_handle(self.handle, batched_train_set.output_types, batched_train_set.output_shapes) 
        self.ne_el = iter_generic.get_next()

        saveable_train_iter = tf.contrib.data.make_saveable_from_iterator(self.train_iter)
        tf.add_to_collection(tf.GraphKeys.SAVEABLE_OBJECTS, saveable_train_iter)
        saveable_valid_iter = tf.contrib.data.make_saveable_from_iterator(self.valid_iter)
        tf.add_to_collection(tf.GraphKeys.SAVEABLE_OBJECTS, saveable_valid_iter)

        tf.summary.image("input_depth", self.ne_el[0])
        tf.summary.image("input_gray", self.ne_el[1])
        # tf.summary.image("input_seman", ne_el[2][..., 7])
        return self.ne_el[0], self.ne_el[1], self.ne_el[2]

        
        


    def _parse_image_function(self, example_proto):
        # Parse the input tf.Example proto using the dictionary above.
        example =  tf.parse_single_example(example_proto, self.image_feature_description)
        img_depth = tf.cast(tf.image.decode_png(example['img_depth']), tf.float32)
        img_rgb = tf.cast(tf.image.decode_png(example['img_rgb']), tf.float32)
        img_seman = tf.cast(tf.image.decode_png(example['img_seman']), tf.float32)
        info_depth = self.extract_from_img(img_depth, 'depth')
        info_rgb = self.extract_from_img(img_rgb, 'intensity')
        info_seman = self.extract_from_img(img_seman, 'semantics')

        return (info_depth, info_rgb, info_seman)


        

    def gen_file_dataset_from_path(self, path_depth, path_gray = None, path_seman = None):
        self.filename_dataset_gray = tf.data.Dataset.list_files(path_gray, shuffle=False)
        self.filename_dataset_seman = tf.data.Dataset.list_files(path_seman, shuffle=False)
        self.filename_dataset_depth = tf.data.Dataset.list_files(path_depth, shuffle=False)
        self.filename = tf.data.Dataset.zip( (self.filename_dataset_depth, self.filename_dataset_gray, self.filename_dataset_seman) )
            
        self.filename = self.filename.shuffle(150000)
        filename_iter = self.filename.make_one_shot_iterator()
        self.next_name = filename_iter.get_next()

    def gen_iterator_from_filename(self):
        ### generate tensorflow dataset given path of image files

        # split into training set and validation set
        filename_train_set = self.filename.take(140000)
        filename_valid_set = self.filename.skip(140000)
        train_set = filename_train_set.map(lambda x, y, z: (self.extract_from_png_file(x, 'depth'), self.extract_from_png_file(y, 'intensity'), self.extract_from_png_file(z, 'semantics')))
        valid_set = filename_valid_set.map(lambda x, y, z: (self.extract_from_png_file(x, 'depth'), self.extract_from_png_file(y, 'intensity'), self.extract_from_png_file(z, 'semantics')))
            
        # extract training data
        batched_train_set = train_set.repeat().batch(self.batchsize)
        train_iter = batched_train_set.make_one_shot_iterator()

        # extract validation data
        batched_valid_set = valid_set.repeat().batch(self.batchsize)
        valid_iter = batched_valid_set.make_one_shot_iterator()

        # construct a common iterator handle to allow switching between iterators of different dataset
        handle = tf.placeholder(tf.string, shape=[])
        iter_generic = tf.data.Iterator.from_string_handle(handle, batched_train_set.output_types, batched_train_set.output_shapes) 
        ne_el = iter_generic.get_next()

        return train_iter, valid_iter, handle, ne_el


    def extract_from_png_file(self, file, mode = "depth"):
        ### mapping a filename to the corresponding meaningful data
        # read the file to image format
        img = tf.cast(tf.image.decode_png(tf.read_file(file)), tf.float32)

        img_compact = self.extract_from_img(img, mode)

        return img_compact

    def extract_from_img(self, img, mode):
        # different processing method for files of different meanings
        if mode == "depth":
            code = img[...,0:1] + img[...,1:2]*256 + img[...,2:3]*256*256
            num = code * 1000/(256*256*256 - 1) 
            avg = 15
            num = avg / (num + avg)
            num_compact = tf.image.resize_images(num, [self.im_height, self.im_width])
        elif mode == "semantics":
            num = img[...,0:1]
            num_idx = tf.image.resize_images(num, [self.im_height, self.im_width])
            num_compact = tf.cast(tf.round(num_idx), tf.int32)
            # num_compact = tf.one_hot(num_idx, self.seman_category)
        elif mode == "intensity":
            # num = tf.image.rgb_to_grayscale(img)
            # num = num / 255.0
            num = img / 255.0
            num_compact = tf.image.resize_images(num, [self.im_height, self.im_width])
        else:
            sys.exit('Error! Mode in extract_from_png_file not defined')
        return num_compact


    def unet_expander(self, h_list, name = 'expander'):
        with tf.variable_scope(name):
            dh_list = [None]*5
            # first block: conv + relu + conv + relu
            with tf.variable_scope('dh4'):
                h_mid = conv2d_nlrelu(h_list[4], 256, 256, 'conv_relu_1')
                dh_list[4] = conv2d_nlrelu(h_mid, 256, 256, 'conv_relu_2')

            # 2nd to 5th blocks: upsampling + conv + relu + concat + conv + relu
            for i in range(3, -1, -1): # 256-128-64-32-16
                dh_list[i] = up_block(dh_list[i+1], h_list[i], 'dh%d'%(i))

        return dh_list

    def unet_mid_sem(self, dh_list):
        dh_sem = [None]*3
        for i in range(2, -1, -1): # 256-128-64-32-16
            with tf.variable_scope('dh%doutput_sem'%(i)):
                dh_sem[i] = conv2d_nlrelu(dh_list[i], dh_list[i].get_shape()[-1], int(int(dh_list[i].get_shape()[-1])/2), name="conv_relu_1", ker_size=3)
                dh_sem[i] = conv2d(dh_sem[i], dh_sem[i].get_shape()[-1], self.seman_category, name="conv_2", ker_size=1)

        return dh_sem

    def unet_mid_dep_sigma(self, dh_list):
        dh_sigma = [None]*3
        for i in range(2, -1, -1):
            with tf.variable_scope('dh%doutput_dep'%(i)):
                dh_sigma[i] = conv2d_nlrelu(dh_list[i], dh_list[i].get_shape()[-1], int(int(dh_list[i].get_shape()[-1])/2), name="conv_1", ker_size=3)
                dh_sigma[i] = conv2d_nlrelu(dh_sigma[i], dh_sigma[i].get_shape()[-1], 1, name="conv_2", ker_size=3)
                # dh_sigma[i] = conv2d_nlrelu(dh_list[i], dh_list[i].get_shape()[-1], 1, name="conv_1", ker_size=3)
                if self.clip_depth_sigma:
                    dh_sigma[i] = tf.clip_by_value(dh_sigma[i], clip_value_min=0.2, clip_value_max=0.2)
        return dh_sigma

    def unet_hlast_sem(self, h_last_deconved):
        with tf.variable_scope('h_last_sem'):
            h_last = conv2d_nlrelu(h_last_deconved, 16, 16, 'conv_relu_1')
            h_last = conv2d(h_last, 16, self.seman_category, 'conv_2', ker_size=1)
            h_last_softmax = tf.nn.softmax(h_last)
            tf.summary.image("output_sem", h_last_softmax[..., 7:8])
        return h_last

    def unet_hlast_dep(self, h_last_deconved):
        with tf.variable_scope('h_last_dep'):
            h_last = conv2d_nlrelu(h_last_deconved, 16, 16, 'conv_relu_1')
            h_last = conv2d_nlrelu(h_last, 16, 1, 'conv_relu_2')
            if self.clip_depth_sigma:
                h_last = tf.clip_by_value(h_last, clip_value_min=0.2, clip_value_max=0.2)
            tf.summary.image("output_dep_uncert", h_last)
        return h_last

    def graph_unet(self, next_batch_intensity):
        ### construct u-net for intensity images

        with tf.variable_scope("intensity"):
            # _, image_matrix = self.reshape_input(next_batch_intensity)
            _, image_matrix = self.reshape_input(next_batch_intensity, name="reshape_rgb")
            # image_matrix = image_matrix - 0.5
            # initialize the output lists of each contracting block (h_list, for skip connection in U-Net) 
            # and expanding block (dh_list, for conditioning in CodeSLAM)
            h_list = [None]*5
            dh_list = [None]*5

            dh_sigma = [None]*3

            dh_sem = [None]*3

            # contracting part
            # first block: strided conv + relu
            # h_list[0] = conv2d_nlrelu_strided(image_matrix, 1, 16, 'h0')
            with tf.variable_scope('h0'):
                h_list[0] = conv2d_nlrelu_strided(image_matrix, 3, 16)
                # h_list[0] = conv2d_nlrelu_strided(image_matrix, 3, 8)
                # h_list[0] = conv2d_nlrelu(h_list[0], 8, 16, ker_size=1)
                

            # 2nd to 5th blocks: max pooling + conv + relu + conv + relu
            for i in range(4): # 16-32-64-128-256
                h_list[i+1] = down_block(h_list[i],'h%d'%(i+1))

            # expanding part
            dh_list = self.unet_expander(h_list)
            dh_sigma = self.unet_mid_dep_sigma(dh_list)
            dh_sem = self.unet_mid_sem(dh_list)
            
            h_last_deconved = deconv_relu(dh_list[0], [self.batchsize, self.im_height, self.im_width, 16], 'deconv_relu')

            
            h_last_1 = self.unet_hlast_sem(h_last_deconved)
            h_last_2 = self.unet_hlast_dep(h_last_deconved)
            return dh_list, h_last_2, dh_sigma, h_last_1, dh_sem



    def graph(self, next_batch, dh_list = None, dh_last = None, batch_truth = None, dh_sigma = None, dh_list_last = None, sem_from_unet = None, sem_mid_unet=None):
        self.n_z = 256 # 20
        
        if dh_list is None and dh_last is None:
            # Not-conditioned-on-intensity version
            with tf.variable_scope("depth"):
                input_batch_flat, image_matrix = self.reshape_input(next_batch)
                z_mean, z_stddev = self.recognition(image_matrix)
                self.guessed_z = self.sampling(z_mean, z_stddev)
                generated_images = self.generation(self.guessed_z)
                generated_flat = self.reshape_output(generated_images, 'depth')
                self.calc_gen_loss(input_batch_flat, generated_flat, power = 0, uncertainty = None)
                self.calc_losses(z_mean, z_stddev)
        else:
            # Conditioned-on-intensity version
            with tf.variable_scope("depth"):
                input_batch_flat, image_matrix = self.reshape_input(next_batch)

                z_mean, z_stddev = self.recognition_cond(image_matrix, dh_list)
                self.guessed_z = self.sampling(z_mean, z_stddev)

                # # input optimized code here
                # self.guessed_z = tf.placeholder(tf.float32, (6,256))

                if self.clip_z:
                    self.guessed_z = tf.clip_by_value(self.guessed_z, clip_value_min=0, clip_value_max=0)
                
                generated_images, mid_gen_list = self.generation_cond(self.guessed_z, dh_list)
                self.depth_prediction = generated_images

                generated_seman, mid_seman = self.generation_cond(self.guessed_z, dh_list, name="sem")
                
            self.calc_mul_gen_loss(generated_images, dh_last, mid_gen_list, dh_sigma, image_matrix, input_batch_flat)

            # # Jacobian calculation
            # self.jac_depth_to_code = tf.stack(
            # [tf.gradients(self.generated_depth_flat[:, idx], self.guessed_z)[0] for idx in self.idx_high_grad], axis=1,
            # name='jac_depth_to_code') # self.im_height*self.im_width

            # self.idx_high_grad = tf.placeholder(tf.float32, (6,2000))
            # self.jac_depth_to_code_0 = tf.map_fn(lambda idx: tf.gradients(self.generated_depth_flat[:, tf.cast(idx, tf.int32)], self.guessed_z)[0], self.idx_high_grad[0])
            # self.jac_depth_to_code_0 = tf.transpose(self.jac_depth_to_code_0, perm=[1, 0, 2])[0]
            # self.jac_depth_to_code_1 = tf.map_fn(lambda idx: tf.gradients(self.generated_depth_flat[:, tf.cast(idx, tf.int32)], self.guessed_z)[0], self.idx_high_grad[1])
            # self.jac_depth_to_code_1 = tf.transpose(self.jac_depth_to_code_1, perm=[1, 0, 2])[1]
            # self.jac_depth_to_code_2 = tf.map_fn(lambda idx: tf.gradients(self.generated_depth_flat[:, tf.cast(idx, tf.int32)], self.guessed_z)[0], self.idx_high_grad[2])
            # self.jac_depth_to_code_2 = tf.transpose(self.jac_depth_to_code_2, perm=[1, 0, 2])[2]
            # self.jac_depth_to_code_3 = tf.map_fn(lambda idx: tf.gradients(self.generated_depth_flat[:, tf.cast(idx, tf.int32)], self.guessed_z)[0], self.idx_high_grad[3])
            # self.jac_depth_to_code_3 = tf.transpose(self.jac_depth_to_code_3, perm=[1, 0, 2])[3]
            # self.jac_depth_to_code_4 = tf.map_fn(lambda idx: tf.gradients(self.generated_depth_flat[:, tf.cast(idx, tf.int32)], self.guessed_z)[0], self.idx_high_grad[4])
            # self.jac_depth_to_code_4 = tf.transpose(self.jac_depth_to_code_4, perm=[1, 0, 2])[4]
            # self.jac_depth_to_code_5 = tf.map_fn(lambda idx: tf.gradients(self.generated_depth_flat[:, tf.cast(idx, tf.int32)], self.guessed_z)[0], self.idx_high_grad[5])
            # self.jac_depth_to_code_5 = tf.transpose(self.jac_depth_to_code_5, perm=[1, 0, 2])[5]
            # self.jac_depth_to_code = tf.stack((self.jac_depth_to_code_0, self.jac_depth_to_code_1, self.jac_depth_to_code_2, self.jac_depth_to_code_3, self.jac_depth_to_code_4, self.jac_depth_to_code_5))

            idx_to_calc = np.arange(self.im_height * self.im_width).astype(np.float32)
            self.jac_depth_to_code = tf.map_fn(lambda idx: tf.gradients(self.generated_depth_flat[:, tf.cast(idx, tf.int32)], self.guessed_z)[0], idx_to_calc)
            self.jac_depth_to_code = tf.transpose(self.jac_depth_to_code, perm=[1,0,2])


            self.calc_mul_sem_loss(generated_seman, mid_seman, batch_truth, sem_from_unet=sem_from_unet, sem_mid_unet=sem_mid_unet)
            self.calc_losses(z_mean, z_stddev)

    def sem_gen_blocks(self, generated_images, mid_gen_list, dh_list, dh_list_last):
        x_pred_list = [None]*3
         
        with tf.variable_scope("sem_gen_block_0"):
            x_concat = tf.concat([generated_images, dh_list_last], -1)
            x_mid = conv2d_nlrelu(x_concat, x_concat.get_shape()[-1], 16, ker_size=3, name='conv_relu_1' )
            # x_mid = conv2d_nlrelu(x_mid, x_mid.get_shape()[-1], 16, ker_size=3, name='conv_relu_2' )
            x_pred_last = conv2d(x_mid, 16, self.seman_category, ker_size=1)
            x_pred_last_softmax = tf.nn.softmax(x_pred_last)
            tf.summary.image("output_sem_final", x_pred_last_softmax[..., 7:8])

        for i in range(3):
            with tf.variable_scope("sem_gen_block_%d"%(i+1)):
                x_concat = tf.concat([mid_gen_list[i], dh_list[i]], -1)
                x_mid = conv2d_nlrelu(x_concat, x_concat.get_shape()[-1], 16, ker_size=3, name='conv_relu_1' )
                # x_mid = conv2d_nlrelu(x_mid, x_mid.get_shape()[-1], 16, ker_size=3, name='conv_relu_2' )
                x_pred_list[i] = conv2d(x_mid, 16, self.seman_category, ker_size=1)

        return x_pred_last, x_pred_list


                


    # reshape input
    def reshape_input(self, batch_input, name = "reshape_input"):
        with tf.variable_scope(name):
            input_batch_flat = tf.reshape(batch_input, [-1, self.im_height * self.im_width], name = "truth_flat")
            if name is "reshape_rgb":
                image_matrix = tf.reshape(batch_input,[-1, self.im_height, self.im_width, 3], name = "mat")
            else:
                image_matrix = tf.reshape(batch_input,[-1, self.im_height, self.im_width, 1], name = "mat")

        return input_batch_flat, image_matrix

    # encoder
    def recognition(self, input_images):
        with tf.variable_scope("recognition"):

            # strided conv + relu
            h1 = conv2d_nlrelu_strided(input_images, 1, 16, "d_h1") # 28x28x1 -> 14x14x16 256*192*1 -> 128*96*16
            h2 = conv2d_nlrelu_strided(h1, 16, 32, "d_h2") # 14x14x16 -> 7x7x32 128*96*16 -> 64*48*32
            h3 = conv2d_nlrelu_strided(h2, 32, 64, "d_h3") # 64*48*32 -> 32*24*64
            h4 = conv2d_nlrelu_strided(h3, 64, 128, "d_h4") # 32*24*64 -> 16*12*128
            h5 = conv2d_nlrelu_strided(h4, 128, 256, "d_h5") # 16*12*128 -> 8*6*256
            
            # reshape + fully connected
            h_flat = tf.layers.flatten(h5, name = "h_flat")
            z_mean = tf.layers.dense(h_flat, self.n_z, name = "z_mean")
            z_stddev = tf.layers.dense(h_flat, self.n_z, name = "z_stddev")


        tf.summary.histogram("z_mean", z_mean)
        tf.summary.histogram("z_stddev", z_stddev)

        return z_mean, z_stddev

    # encoder when conditioned
    def recognition_cond(self, input_images, dh_list):
        # with tf.variable_scope('recognition'):

        if self.encoder_cond:
            # strided conv + relu + concat + conv + relu
            h = down_block_depth(input_images, dh_list[0], 'h_0')
            for i in range(4):
                h = down_block_depth(h, dh_list[i+1], 'h_%d'%(i+1))
        else:
            h = down_block_depth_noncond(input_images, dh_list[0], 'h_0')
            for i in range(4):
                h = down_block_depth_noncond(h, dh_list[i+1], 'h_%d'%(i+1))

        # reshape + fully connected
        if self.dense_z:
            h_flat = tf.layers.flatten(h, name = "h_flat")
            z_mean = tf.layers.dense(h_flat, self.n_z, kernel_initializer=tf.random_normal_initializer(stddev=0.02), name = "z_mean")
            z_stddev = tf.layers.dense(h_flat, self.n_z, kernel_initializer=tf.random_normal_initializer(stddev=0.02), name = "z_stddev")
        else:
            z_mean = tf.layers.separable_conv2d(h, self.n_z, (6, 8), name= "z_mean" )
            z_mean = tf.layers.flatten(z_mean)
            z_stddev = tf.layers.separable_conv2d(h, self.n_z, (6, 8), name= "z_stddev" )
            z_stddev = tf.layers.flatten(z_stddev)


        tf.summary.histogram("z_mean", z_mean)
        tf.summary.histogram("z_stddev", z_stddev)

        return z_mean, z_stddev


    #sampling
    def sampling(self, z_mean, z_stddev):
        samples = tf.random_normal([self.batchsize,self.n_z],0,1,dtype=tf.float32, name = "rand_normal")
        guessed_z = z_mean + (z_stddev * samples)

        return guessed_z
    

    # decoder
    def generation(self, z):
        with tf.variable_scope("generation"):

            # fully connected + reshape
            z_develop = tf.layers.dense(z, 8*6*256, name ='z_matrix')
            h = tf.reshape(z_develop, [-1, 6, 8, 256])

            # upsampling + conv
            for i in range(4):
                h_pre = h
                h = up_conv(h_pre, "h%d"%(i+1) ) # resize_conv2d

            # transformed conv
            h_last = last_deconv(h, [self.batchsize, self.im_height, self.im_width, 1], "h_last")    

            tf.summary.image("output_depth", h_last)
        return h_last

    # decode when conditioned
    def generation_cond(self, z, dh_list, name = ""):
        # with tf.variable_scope('generation'):

        # fully connected + reshape
        z_develop = tf.layers.dense(z, 8*6*256, name ='z_matrix'+name)
        h = tf.reshape(z_develop, [-1, 6, 8, 256])

        dep_list = [None]*5

        dep_res = [None]*3

        # concat + conv
        with tf.variable_scope('dh_4'+name):
            x_concat = tf.concat([h, dh_list[4], tf.multiply(h, dh_list[4])], -1)
            if self.lin_decoder:
                dep_list[4] = conv2d(x_concat, x_concat.get_shape()[-1], dh_list[4].get_shape()[-1]) # switch_lin
            else:
                dep_list[4] = conv2d_nlrelu(x_concat, x_concat.get_shape()[-1], dh_list[4].get_shape()[-1]) # switch_lin
                dep_list[4] = conv2d_nlrelu(dep_list[4], dep_list[4].get_shape()[-1], dep_list[4].get_shape()[-1], 'conv_relu_2') # switch_lin

            # dep_list[4] = conv2d(dep_list[4], dep_list[4].get_shape()[-1], dep_list[4].get_shape()[-1], 'conv_2') # switch_lin

        # upsampling + conv + concat + conv
        for i in range(3, -1, -1):
            if self.lin_decoder:
                dep_list[i] = up_block_depth(dep_list[i+1], dh_list[i], 'dh_%d'%(i)+name)
            else:
                dep_list[i] = up_block_depth_relu(dep_list[i+1], dh_list[i], 'dh_%d'%(i)+name)
            if i < 3:
                with tf.variable_scope('dh_%d'%(i)+name+'output'):
                    if name is "":
                        dep_res[i] = conv2d(dep_list[i], dep_list[i].get_shape()[-1], int(int(dep_list[i].get_shape()[-1])/2), name="conv_1", ker_size=3)
                        dep_res[i] = conv2d(dep_res[i], dep_res[i].get_shape()[-1], 1, name="conv_2", ker_size=3)
                    else:
                        # dep_res[i] = conv2d(dep_list[i], dep_list[i].get_shape()[-1], self.seman_category, ker_size=1)
                        dep_res[i] = conv2d_nlrelu(dep_list[i], dep_list[i].get_shape()[-1], 16)
                        dep_res[i] = conv2d(dep_res[i], dep_res[i].get_shape()[-1], self.seman_category, ker_size=1)

                    
        # transformed conv
        if name is "":
            # h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 1], "dh_last"+name)
            # h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 8], "dh_last"+name)
            # h_last = conv2d(h_last, 8, 1, "last_conv"+name, ker_size=3)
            with tf.variable_scope("dh_last"+name):
                h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 8] )
                h_last = conv2d(h_last, 8, 1, ker_size=3)

                h_last_clip = tf.clip_by_value(h_last, clip_value_min=0, clip_value_max=1)
                tf.summary.image("output_depth"+name, h_last_clip)
        else:
            # h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 16], "dh_last"+name)
            # h_last = conv2d(h_last, 16, self.seman_category, 'conv_relu_2', ker_size=1)
            with tf.variable_scope("dh_last"+name):
                h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 16])
                # h_last = conv2d(h_last, 16, self.seman_category, ker_size=1)
                h_last = conv2d_nlrelu(h_last, 16, 16)
                h_last = conv2d(h_last, 16, self.seman_category, ker_size=1)
                h_last_softmax = tf.nn.softmax(h_last)
                tf.summary.image("output_"+name, h_last_softmax[..., 7:8])

        self.udp_pred_dep = h_last
        self.udp_z = z

        return h_last, dep_res


    # reshape output
    def reshape_output(self, batch_output, name, power = 0):
        with tf.variable_scope("reshape_output" + name):
            width = int(self.im_width / (2**power))
            height = int(self.im_height / (2**power))
            generated_flat = tf.reshape(batch_output, [self.batchsize, width * height], name = "generated_flat")

        return generated_flat

    def calc_mul_gen_loss_unet(self, generated_images, mid_gen_list, image_matrix, input_batch_flat):
        with tf.variable_scope('losses_gen'):
            with tf.variable_scope('loss_gen_block_0'):
                generated_flat = self.reshape_output(generated_images, 'depth')
                gen_loss_final = self.calc_gen_loss(input_batch_flat, generated_flat, 0)

            gen_flat_list = [None]*3
            ori_flat_list = [None]*3
            gen_loss_list = [None]*3

            for i in range(3):
                with tf.variable_scope('loss_gen_block_%d'%(i+1)):
                    gen_flat_list[i] = self.reshape_output(mid_gen_list[i], 'depth', i + 1)
                    mid_gen_list_clip = tf.clip_by_value(mid_gen_list[i], clip_value_min=0, clip_value_max=1)
                    tf.summary.image('output_depth_%d'%(i+1), mid_gen_list_clip)

                    smaller_imgs = tf.image.resize_bilinear(image_matrix, [int(int(image_matrix.get_shape()[1])/(2**(i+1))), int(int(image_matrix.get_shape()[2])/(2**(i+1)))])
                    ori_flat_list[i] = self.reshape_output(smaller_imgs, 'truth', i+1)
                    gen_loss_list[i] = self.calc_gen_loss(ori_flat_list[i], gen_flat_list[i], i+1)
                    
                    tf.summary.histogram('output_depth_%d'%(i+1), gen_flat_list[i])
                    
            self.generation_loss_unet = gen_loss_final + gen_loss_list[0] + gen_loss_list[1] + gen_loss_list[2]

    def calc_mul_gen_loss(self, generated_images, dh_last, mid_gen_list, dh_list, image_matrix, input_batch_flat):
        with tf.variable_scope('losses_gen'):
            with tf.variable_scope('loss_gen_block_0'):
                self.generated_depth_flat = self.reshape_output(generated_images, 'depth')
                generated_uncertainty = self.reshape_output(dh_last, 'intensity')
                gen_loss_final = self.calc_gen_loss(input_batch_flat, self.generated_depth_flat, 0, generated_uncertainty)

            gen_flat_list = [None]*3
            uncertainty_flat_list = [None]*3
            ori_flat_list = [None]*3

            gen_loss_list = [None]*3

            for i in range(3):
                with tf.variable_scope('loss_gen_block_%d'%(i+1)):
                    # mid_gen_single_layer = conv2d(mid_gen_list[i], mid_gen_list[i].get_shape()[-1], 1)
                    
                    gen_flat_list[i] = self.reshape_output(mid_gen_list[i], 'depth', i + 1)
                    uncertainty_flat_list[i] = self.reshape_output(dh_list[i], 'uncertainty', i + 1)

                    mid_gen_list_clip = tf.clip_by_value(mid_gen_list[i], clip_value_min=0, clip_value_max=1)
                    tf.summary.image('output_depth_%d'%(i+1), mid_gen_list_clip)
                    tf.summary.image('output_uncert_%d'%(i+1), dh_list[i])

                    smaller_imgs = tf.image.resize_bilinear(image_matrix, [int(int(image_matrix.get_shape()[1])/(2**(i+1))), int(int(image_matrix.get_shape()[2])/(2**(i+1)))])
                    ori_flat_list[i] = self.reshape_output(smaller_imgs, 'truth', i+1)

                    gen_loss_list[i] = self.calc_gen_loss(ori_flat_list[i], gen_flat_list[i], i+1, uncertainty_flat_list[i])
                    
                    tf.summary.histogram('output_depth_%d'%(i+1), gen_flat_list[i])
                    tf.summary.histogram('output_uncert_%d'%(i+1), uncertainty_flat_list[i])
                    

            self.generation_loss = gen_loss_final + gen_loss_list[0] + gen_loss_list[1] + gen_loss_list[2]

    def calc_mul_sem_loss_unet(self, batch_truth, sem_from_unet, sem_mid_unet):
        with tf.variable_scope('losses_sem'):
            with tf.variable_scope('loss_sem_block_0'):
                batch_truth_dim3 = tf.reshape(batch_truth, [-1, self.im_height, self.im_width])
                seman_matrix = tf.one_hot(batch_truth_dim3, self.seman_category, name = "truth_seman")
                tf.summary.image("input_seman", seman_matrix[..., 7:8])

                seman_cost_final_unet = tf.losses.softmax_cross_entropy(seman_matrix, sem_from_unet)

            sem_true_list = [None]*3
            sem_loss_unet_list = [None]*3

            for i in range(3):
                with tf.variable_scope('loss_sem_block_%d'%(i+1)):
                    smaller_imgs = tf.image.resize_bilinear(batch_truth, [int(int(batch_truth.get_shape()[1])/(2**(i+1))), int(int(batch_truth.get_shape()[2])/(2**(i+1)))])
                    smaller_imgs = tf.cast(tf.round(smaller_imgs), tf.int32)
                    smaller_imgs = tf.reshape(smaller_imgs,[-1, smaller_imgs.get_shape()[1], smaller_imgs.get_shape()[2]])
                    sem_true_list[i] = tf.one_hot(smaller_imgs, self.seman_category, name = "mid_seman_truth_%d"%(i+1))
                    
                    sem_loss_unet = tf.losses.softmax_cross_entropy(sem_true_list[i], sem_mid_unet[i])
                    sem_mid_unet_softmax = tf.nn.softmax(sem_mid_unet[i])
                    tf.summary.image("output_sem_unet%d"%(i+1), sem_mid_unet_softmax[..., 7:8])
                    sem_loss_unet_list[i] = sem_loss_unet
  
            self.sem_unet_loss = sem_loss_unet_list[0] + sem_loss_unet_list[1] + sem_loss_unet_list[2]
            self.sem_unet_loss = self.sem_unet_loss + seman_cost_final_unet
            self.sem_unet_loss = self.sem_coef * self.sem_unet_loss


    def calc_mul_sem_loss(self, generated_images, mid_gen_list, batch_truth, sem_from_unet=None, sem_mid_unet=None):
        with tf.variable_scope('losses_sem'):
            with tf.variable_scope('loss_sem_block_0'):
                batch_truth_dim3 = tf.reshape(batch_truth, [-1, self.im_height, self.im_width])
                seman_matrix = tf.one_hot(batch_truth_dim3, self.seman_category, name = "truth_seman")
                seman_cost_final = tf.losses.softmax_cross_entropy(seman_matrix, generated_images)
                ### calculate the IOU of road segmentation
                pred_class = tf.argmax(generated_images, 3)
                self.mean_iou, self.iou_update = tf.metrics.mean_iou(batch_truth_dim3, pred_class, self.seman_category, name = 'calc_miou' )
                ##########################################
                seman_truth_roadonly = seman_matrix[..., 7:8]
                tf.summary.image("input_seman", seman_truth_roadonly)
                pred_onehot = tf.one_hot(pred_class, self.seman_category)
                pred_roadonly = pred_onehot[..., 7:8]
                self.mean_prec, self.prec_update = tf.metrics.precision(seman_truth_roadonly, pred_roadonly, name = 'calc_prec' )
                self.mean_recl, self.recl_update = tf.metrics.recall(seman_truth_roadonly, pred_roadonly, name = 'calc_recl' )
                
                if sem_from_unet is not None: 
                    seman_cost_final_unet = tf.losses.softmax_cross_entropy(seman_matrix, sem_from_unet)
                    # seman_cost_final = seman_cost_final + seman_cost_final_unet

            sem_loss_list = [None]*3
            sem_pred_list = [None]*3
            sem_true_list = [None]*3

            if sem_mid_unet is not None:   
                sem_loss_unet_list = [None]*3

            for i in range(3):
                with tf.variable_scope('loss_sem_block_%d'%(i+1)):
                    smaller_imgs = tf.image.resize_bilinear(batch_truth, [int(int(batch_truth.get_shape()[1])/(2**(i+1))), int(int(batch_truth.get_shape()[2])/(2**(i+1)))])
                    smaller_imgs = tf.cast(tf.round(smaller_imgs), tf.int32)
                    smaller_imgs = tf.reshape(smaller_imgs,[-1, smaller_imgs.get_shape()[1], smaller_imgs.get_shape()[2]])
                    sem_true_list[i] = tf.one_hot(smaller_imgs, self.seman_category, name = "mid_seman_truth_%d"%(i+1))
            
                    sem_loss_list[i] = tf.losses.softmax_cross_entropy(sem_true_list[i], mid_gen_list[i])
                    sem_softmax = tf.nn.softmax(mid_gen_list[i])
                    tf.summary.image('output_sem_%d'%(i+1), sem_softmax[...,7:8])

                    if sem_mid_unet is not None: 
                        sem_loss_unet = tf.losses.softmax_cross_entropy(sem_true_list[i], sem_mid_unet[i])
                        sem_mid_unet_softmax = tf.nn.softmax(sem_mid_unet[i])
                        tf.summary.image("output_sem_unet%d"%(i+1), sem_mid_unet_softmax[..., 7:8])
                        sem_loss_unet_list[i] = sem_loss_unet

            if sem_mid_unet is not None:     
                self.sem_unet_loss = sem_loss_unet_list[0] + sem_loss_unet_list[1] + sem_loss_unet_list[2]
                if sem_from_unet is not None:
                    self.sem_unet_loss = self.sem_unet_loss + seman_cost_final_unet
                self.sem_unet_loss = self.sem_coef * self.sem_unet_loss

            self.seman_cost = seman_cost_final + sem_loss_list[0] + sem_loss_list[1] + sem_loss_list[2]
            self.seman_cost = self.sem_coef * self.seman_cost
            
            

            
    def calc_gen_loss(self, batch_truth, batch_pred, power = 0, uncertainty = None):
        with tf.variable_scope('loss_gen_%d'%(power)):
            if uncertainty is None:
                generation_loss = tf.reduce_sum( tf.abs(batch_truth - batch_pred), 1, name = "loss_gen" )
            else:
                generation_loss = tf.multiply(tf.reduce_sum( tf.div(tf.abs(batch_truth - batch_pred), 1e-7 + uncertainty) + tf.log(1e-7 + uncertainty), 1), 4**power, name = "loss_gen" )

            generation_loss = tf.reduce_mean(generation_loss) # average over the batch size

            return generation_loss



    def calc_losses(self, z_mean, z_stddev):
        # with tf.variable_scope("loss_gen"):
            # generation_loss = tf.negative( tf.reduce_sum(batch_truth * tf.log(1e-8 + batch_pred) + 
            # (1-batch_truth) * tf.log(1e-8 + 1 - batch_pred), 1 ), name = "loss_gen" ) 

        with tf.variable_scope("loss_lat"):
            latent_loss = tf.multiply(tf.reduce_sum(tf.square(z_mean) + tf.square(z_stddev) - tf.log(tf.square(z_stddev)) - 1,1), 0.5, name = "loss_lat")
            self.latent_loss = tf.reduce_mean(latent_loss) # average over the batch size

        with tf.variable_scope("cost"):
            self.cost = tf.add_n([self.generation_loss, self.latent_loss, self.seman_cost, self.sem_unet_loss], name = "cost")
            tf.summary.scalar("sem_loss_unet", self.sem_unet_loss)
            tf.summary.scalar("sem_loss", self.seman_cost)

        # tf.summary.scalar("lat_loss", tf.reduce_mean(latent_loss))
        tf.summary.scalar("lat_loss", self.latent_loss)
        tf.summary.scalar("gen_loss", self.generation_loss)
        tf.summary.scalar("tot_loss", self.cost)


    #train
    def train(self):

        loop = 200
        continue_train = True
        toy_test = False
        if continue_train:
            initial_iter = 9800
            stage = initial_iter//loop
        else:
            initial_iter = 0
            stage = 0

        self.part_restore = False # used when the trained variables are more than last trial
        self.part_continue = False
        self.only_eval = True # only for sem_unet and sem_on_depth mode

        if self.part_restore or self.part_continue:
            # variables = tf.contrib.framework.get_variables_to_restore()
            # variables_to_restore = [v for v in variables if v.name.split('/')[0]=='intensity' and 
            # v.name.split('/')[1]!='h_last' and v.name.split('/')[1]!='conv_relu_1' and v.name.split('/')[1]!='conv_relu_2']

            # saver_res = tf.train.Saver(variables_to_restore)

            # unet parameters related to semantic understanding
            vars_to_freeze = [v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v.name.split('/')[0] == 'intensity' \
            and 'output_dep' not in v.name and 'h_last_dep' not in v.name]
            print('variables frozen: %d'%(len(vars_to_freeze)))
        
        # related to miou, precision and recall
        vars_relatedto_eval = tf.get_collection(tf.GraphKeys.LOCAL_VARIABLES, scope="losses_sem/loss_sem_block_0/calc_")
        vars_eval_initializer = tf.variables_initializer(var_list=vars_relatedto_eval)

        with tf.name_scope("optimizer"):
            self.learning_rate = tf.placeholder(tf.float32, shape=[])
            if self.part_restore or self.part_continue:
                vars_to_train = [v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v not in vars_to_freeze]
                self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.cost, 
                var_list=vars_to_train)
                for v in vars_to_train:
                    print(v.name)
            else:
                # self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.cost, 
                # var_list=[v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v.name.split('/')[0]=='intensity'])
                # self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.sem_unet_loss)
                
                self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.cost)
                # # Jacobian calculation
                # self.jac_depth_to_code = tf.stack(
                # [tf.train.AdamOptimizer().compute_gradients(self.generated_depth_flat[:, idx], self.guessed_z)[0] for idx in range(200)], axis=1,
                # name='jac_depth_to_code') # self.im_height*self.im_width

        if self.part_restore:
            variables_to_restore = [v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v.name.split('/')[-1] is not 'Adam' ]
            saver_res = tf.train.Saver(variables_to_restore)

        if self.only_eval: # not restoring the data iterator, since it could be a different dataset 
            variables_to_restore = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
            saver_eval = tf.train.Saver(variables_to_restore)

        saver = tf.train.Saver()

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True

        with tf.Session(config=config) as sess: # config=tf.ConfigProto(log_device_placement=True)
            # sess = tf_debug.LocalCLIDebugWrapperSession(sess)
            # devices = sess.list_devices()
            sess.run(tf.global_variables_initializer())
            if continue_train:
                if self.part_restore:
                    # sess.run(tf.global_variables_initializer())
                    print("All variables initialized")
                    saver_res.restore(sess, "models/model%d.ckpt"%(stage))
                else:
                    if self.only_eval:
                        saver_eval.restore(sess, "models/model%d.ckpt"%(stage))
                    else:
                        saver.restore(sess, "models/model%d.ckpt"%(stage))
                print("Model %d restored."%(stage))
            else:
                # sess.run(tf.global_variables_initializer())
                print("All variables initialized")

            sess.run(vars_eval_initializer)            

            train_handle = sess.run(self.train_iter.string_handle()) 
            valid_handle = sess.run(self.valid_iter.string_handle())

            if toy_test:
                eval_handle = train_handle
            else:
                eval_handle = valid_handle

            # ## loat the idx set of high_grad pts
            # idx_path = os.path.join(self.carla_path, 'Top_grad_idxs')
            # idx_files = os.listdir(idx_path)
            # idx_files = sorted(idx_files)

            # opti_z_path = os.path.join(self.carla_path, 'Opti_code')
            # opti_z_files = os.listdir(opti_z_path)
            # opti_z_files = sorted(opti_z_files)


            rate = 0.001
            # initial_rate = True
            offset = 0
            last_iter = 10661 #10661 #9801
            for iter in range(initial_iter, last_iter + 1):

                if iter > loop*stage:
                    stage = stage + 1
                # large dataset
                if iter >= offset + 2000:
                    rate = 0.0001
                if iter >= offset + 6600:
                    rate = 0.00001
                if iter >= offset + 20000: 
                    rate = 0.000001

                # # small dataset
                # if iter >= 1600:
                #     rate = 0.0001
                # if iter >= 2600:
                #     rate = 0.00001
                # if iter >= 4000: 
                #     rate = 0.000001
                tf.summary.scalar("learning_rate", rate)
                try:
                    if self.only_eval:
                        # # send udp
                        # s, gen_loss, lat_loss, seman_loss, sem_unet_loss, send_pred_dep, send_z = sess.run((merged_summary, 
                        # self.generation_loss, self.latent_loss, self.seman_cost, self.sem_unet_loss, self.udp_pred_dep, self.udp_z), 
                        # feed_dict = {self.handle : eval_handle})

                        ## calc miou, precision and recall, and jacobian
                        ## load idx from txt
                        # idx_high_grad = np.stack( np.loadtxt( os.path.join(idx_path, idx_files[ (self.batchsize*(iter-initial_iter) + k )%5169 ] ) ) for k in range(self.batchsize) ).astype(np.float32)
                        # print(idx_high_grad.shape)
                        s, gen_loss, lat_loss, seman_loss, sem_unet_loss, _, _, _, depth_prediction, jac_depth_to_code, code_z = sess.run((merged_summary, 
                        self.generation_loss, self.latent_loss, self.seman_cost, self.sem_unet_loss, self.iou_update, self.prec_update, self.recl_update, 
                        self.depth_prediction, self.jac_depth_to_code, self.guessed_z), 
                        feed_dict = {self.handle : eval_handle})
                        # feed_dict = {self.handle : eval_handle, self.idx_high_grad : idx_high_grad})

                        # # load optimized code: 
                        # opti_z = np.stack( np.load( os.path.join(opti_z_path, opti_z_files[ (self.batchsize*(iter-initial_iter) + k )%5169 ] ) ) for k in range(self.batchsize) ).astype(np.float32)
                        # # run the network with given z, do not output z
                        # s, gen_loss, lat_loss, seman_loss, sem_unet_loss, _, _, _, depth_prediction, jac_depth_to_code = sess.run((merged_summary, 
                        # self.generation_loss, self.latent_loss, self.seman_cost, self.sem_unet_loss, self.iou_update, self.prec_update, self.recl_update, 
                        # self.depth_prediction, self.jac_depth_to_code), 
                        # feed_dict = {self.handle : eval_handle, self.guessed_z: opti_z})

                        # # no jacobian, output z
                        # s, gen_loss, lat_loss, seman_loss, sem_unet_loss, _, _, _, depth_prediction, code_z = sess.run((merged_summary, 
                        # self.generation_loss, self.latent_loss, self.seman_cost, self.sem_unet_loss, self.iou_update, self.prec_update, self.recl_update, 
                        # self.depth_prediction, self.guessed_z), 
                        # feed_dict = {self.handle : eval_handle})

                        # ## calc miou, precision and recall, no z or jacobian
                        # s, gen_loss, lat_loss, seman_loss, sem_unet_loss, _, _, _, depth_prediction = sess.run((merged_summary, 
                        # self.generation_loss, self.latent_loss, self.seman_cost, self.sem_unet_loss, self.iou_update, self.prec_update, self.recl_update, 
                        # self.depth_prediction), 
                        # feed_dict = {self.handle : eval_handle})

                        # the three cannot be called at once 
                        mean_iou = sess.run(self.mean_iou)
                        prec = sess.run(self.mean_prec)
                        recl = sess.run(self.mean_recl)

                        ### output the Jacobian of image w.r.t. code and code
                        print(jac_depth_to_code.shape)
                        print(jac_depth_to_code.dtype)
                        for i in range(self.batchsize):
                            # txt_name = 'Jacobians/%05d_%02d.txt'%(iter, i)
                            txt_name = 'Jacobians/%05d_%02d'%(iter, i)
                            txt_path = os.path.join(self.carla_path, txt_name)
                            # np.savetxt(txt_path, jac_depth_to_code[i], fmt='%.5e')
                            np.save(txt_path, jac_depth_to_code[i])
                            # code_name = 'Depth_code/%05d_%02d.txt'%(iter, i)
                            code_name = 'Depth_code/%05d_%02d'%(iter, i)
                            code_path = os.path.join(self.carla_path, code_name)
                            # np.savetxt(code_path, code_z[i], fmt='%.5e')
                            np.save(code_path, code_z[i])
                        

                        ####### write depth to image
                        mode_1 = 256*256
                        mode_2 = 256 
                        target_folder = os.path.join(self.carla_path, 'vae_depth_output' )
                        target_folder_visualize = os.path.join(self.carla_path, 'vae_depth_output_visualize' )
                        for p in range(self.batchsize):
                            small_idx = depth_prediction < 1e-3
                            depth_prediction[small_idx] = 1e-3
                            large_idx = depth_prediction > 0.9
                            depth_prediction[large_idx] = 0.9
                            # depth_prediction = max(depth_prediction, 1e-3)
                            depth_recovered = 15 / depth_prediction[p, ...] - 15
                            large_idx = depth_recovered >1000
                            depth_recovered[large_idx] = 1000
                            depth_scaled = np.round((depth_recovered*(256*256*256-1)/1000 )).astype(np.int64)
                            chnl_1, remain_1 = np.divmod(depth_scaled, mode_1)
                            chnl_2, chnl_3 = np.divmod(remain_1, mode_2)
                            # chnl_1.fill(0)
                            # chnl_2.fill(0)
                            # chnl_3.fill(255)
                            
                            rgb_depth = np.dstack((chnl_3, chnl_2, chnl_1))
                            rgb_depth = rgb_depth.astype(np.uint8)
                            rgb_depth_img = Image.fromarray(rgb_depth)
                            filename = "%05d_%02d.png"%(iter, p)
                            file_path = os.path.join(target_folder, filename)
                            rgb_depth_img.save(file_path)
                            file_path_visual = os.path.join(target_folder_visualize, filename)
                            depth_prediction_visual = np.round(255*depth_prediction[p,...]).astype(np.uint8)
                            depth_prediction_visual = depth_prediction_visual[...,0]
                            depth_prediction_v_img = Image.fromarray(depth_prediction_visual, 'L')
                            depth_prediction_v_img.save(file_path_visual)


                        writer.add_summary(s, iter)
                        print("iter %d: genloss %f latloss %f, semloss %f, sem_unet_loss %f, rate: %f, miou %f, prec %f, recl %f" % (
                            iter, gen_loss, lat_loss, seman_loss, sem_unet_loss, rate, mean_iou, prec, recl))

                        # ##### send to udp
                        # print(num_split)
                        # print(send_pred_dep[0].dtype)
                        # to_send_pred_dep = send_pred_dep[0].tostring()
                        # file_splits = list(chunkstring(to_send_pred_dep, 8192))

                        # num_split = len(file_splits)
                        # print(num_split)
                        # print(type(to_send_pred_dep))
                        # my_socket.send(struct.pack(">i", num_split))  # should use big-endian to let matlab properly understand the number (the sequence of four bytes)
                        #                                             # strangely we do not have to specify this in FIFO case
                        # for i in range(num_split):
                        #     my_socket.send(file_splits[i])
                        #     data, addr = my_socket.recvfrom(1024)
                        # print(data)
                    
                    else:
                        sess.run(self.optimizer, feed_dict = {self.handle : train_handle, self.learning_rate : rate})
                        if iter % 10 == 0:
                            sess.run(vars_eval_initializer)

                            s, gen_loss, lat_loss, seman_loss, sem_unet_loss, _, _, _ = sess.run(
                                (merged_summary, self.generation_loss, self.latent_loss, self.seman_cost, self.sem_unet_loss, self.iou_update, self.prec_update, self.recl_update), 
                            feed_dict = {self.handle : eval_handle})

                            # the three cannot be called at once 
                            mean_iou = sess.run(self.mean_iou)
                            prec = sess.run(self.mean_prec)
                            recl = sess.run(self.mean_recl)
                            writer.add_summary(s, iter)    
                                    
                            print("iter %d: genloss %f latloss %f, semloss %f, sem_unet_loss %f, rate: %f, miou %f, prec %f, recl %f" % (
                                iter, gen_loss, lat_loss, seman_loss, sem_unet_loss, rate, mean_iou, prec, recl))
                                
                            if continue_train and iter > 1000 and (seman_loss > 20000000 or gen_loss > 1000000):
                                saver.restore(sess, "models/model%d.ckpt"%(stage-1))
                                print("Model %d restored."%(stage-1))
                                iter = loop*(stage-1) + 1
                                continue

                            # Save the variables to disk.
                            if iter % loop == 0 and iter != initial_iter:
                                save_path = saver.save(sess, "models/model%d.ckpt"%(stage))
                                print("Model saved in path: %s" % save_path)

                        # if initial_rate and np.mean(gen_loss) < 100000:
                        #     rate = 0.0001
                        #     initial_rate = False
                    # print(sess.run(self.next_name))
                    # print(sess.run(self.next_name_gray))
                    
                except tf.errors.OutOfRangeError:
                    break


# def chunkstring(string, length):
#     return (string[0+i:length+i] for i in range(0, len(string), length))

# my_socket= socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# my_socket.bind(('127.0.0.1', 8821))
# my_socket.connect(('127.0.0.1', 8822))


vae_model = vae_depth()

merged_summary = tf.summary.merge_all()
writer = tf.summary.FileWriter('.')
writer.add_graph(tf.get_default_graph())

vae_model.train()

############ train the network
# init = tf.global_variables_initializer()

# sess = tf.Session()
# sess.run(init)
# for i in range(100):
#   _, loss_value = sess.run((train, loss))
#   print(loss_value)

# print(sess.run(y_pred))
############
