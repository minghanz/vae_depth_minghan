from segmentation_models import Unet
from segmentation_models.backbones import get_preprocessing
from keras.callbacks import TensorBoard
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import sys

from time import time
import keras
from ops import *

class vae_depth():
    def __init__(self):
        self.conditioned = True # have U-Net part
        self.inten_only = False # only U-Net
        self.all_at_once = False # CodeSLAM + seman seg
        self.sem_decode = True # CodeSLAM + semantic decoder
        self.sem_on_depth = True
        self.sem_same_decode = False # only can be used when sem_on_depth and sem_decode are both true

        self.sem_unet = True
        self.unet_depth = True
        self.batchsize = 6

        # specifying size of image
        self.im_width = 256 # 28 256
        self.im_height = 192 # 28 192
        self.seman_category = 13
        self.n_z = 256

        self.sem_coef = 1000

        self.dep_sigma_offset = 1e-1 # 1e-7


        self.usr_name = "pingping"#'pingping' # 'minghanz'

        next_batch_depth, next_batch_gray, next_batch_seman = self.read_carla_tfrecord()

        preprocessing_fn = get_preprocessing('resnet34')
        next_batch_gray = preprocessing_fn(next_batch_gray)

        # batch_depth_list = self.resize_truth(next_batch_depth, mode='dep')
        # batch_seman_list = self.resize_truth(next_batch_seman, mode='sem')
        img_seman = tf.placeholder(tf.int32, shape=(None, self.im_height, self.im_width, 1))
        img = tf.placeholder(tf.float32, shape=(None, self.im_height, self.im_width, 3))
        img_depth = tf.placeholder(tf.float32, shape=(None, self.im_height, self.im_width, 1))
        tf.summary.image("input_depth", img_depth)
        tf.summary.image("input_gray", img[..., ::-1])

        with tf.variable_scope("unet"):
            model_sem, model_dep = Unet(backbone_name='resnet34', encoder_weights='imagenet', classes=self.seman_category, activation='softmax', 
            input_shape=(self.im_height, self.im_width, 3), input_tensor=img, freeze_encoder=True)
            unet_output_sem = model_sem.output
            dh_sigma_final = model_dep.output
            
            # unet_output = model(img)
            next_batch_seman_dim3 = tf.reshape(img_seman,[-1, self.im_height, self.im_width])
            batch_seman_final_truth = tf.one_hot(next_batch_seman_dim3, self.seman_category)
            tf.summary.image("input_seman", batch_seman_final_truth[..., 7:8])
            tf.summary.image("unet_pred_seman", unet_output_sem[..., 7:8])
            # seman_truth = tf.placeholder(tf.float32, shape=(None, self.im_height, self.im_width, self.seman_category))
            self.sem_final_loss = tf.reduce_mean(keras.losses.categorical_crossentropy(batch_seman_final_truth, unet_output_sem))

            mid_list = [None]*5
            mid_list[0]= model_sem.get_layer('decoder_stage3_relu2').output
            mid_list[1]= model_sem.get_layer('decoder_stage2_relu2').output
            mid_list[2]= model_sem.get_layer('decoder_stage1_relu2').output
            mid_list[3]= model_sem.get_layer('decoder_stage0_relu2').output
            mid_list[4]= model_sem.get_layer('relu1').output
            # mid_list_final= model.get_layer('decoder_stage4_relu1').output

            mid_list_dep = [None]*5
            mid_list_dep[0]= model_dep.get_layer('decoder_stage3_relu2').output
            mid_list_dep[1]= model_dep.get_layer('decoder_stage2_relu2').output
            mid_list_dep[2]= model_dep.get_layer('decoder_stage1_relu2').output
            mid_list_dep[3]= model_dep.get_layer('decoder_stage0_relu2').output
            mid_list_dep[4]= model_dep.get_layer('relu1').output

        with tf.variable_scope("unet_mid_outputs"):
            dh_sigma = [None]*3
            sem_mid = [None]*3
            for i in range(3):
                with tf.variable_scope('dep_sigma%d'%(i)):
                    dh_sigma[i] = conv2d_nlrelu(mid_list_dep[i], mid_list_dep[i].get_shape()[-1], int(int(mid_list_dep[i].get_shape()[-1])/2), name="conv_relu_1", ker_size=3)
                    dh_sigma[i] = conv2d_nlrelu(dh_sigma[i], dh_sigma[i].get_shape()[-1], 1, name="conv_relu_2", ker_size=3)
                    # dh_sigma[i] = conv2d_nlrelu(mid_list[i], mid_list[i].get_shape()[-1], 1, name="conv_1", ker_size=3)

                with tf.variable_scope('sem_mid%d'%(i)):
                    sem_mid[i] = conv2d_nlrelu(mid_list[i], mid_list[i].get_shape()[-1], 16)
                    sem_mid[i] = conv2d(sem_mid[i], sem_mid[i].get_shape()[-1], self.seman_category, ker_size=3)
            
            # with tf.variable_scope('dep_sigma_final'):
            #     dh_sigma_final = conv2d_nlrelu(mid_list_final, mid_list_final.get_shape()[-1], int(int(mid_list_final.get_shape()[-1])/2), name="conv_1", ker_size=3)
            #     dh_sigma_final = conv2d_nlrelu(dh_sigma_final, dh_sigma_final.get_shape()[-1], 1, name="conv_2", ker_size=3)
            #     # dh_sigma_final = conv2d_nlrelu(mid_list_final, mid_list_final.get_shape()[-1], 1, name="conv_1", ker_size=3)
            #     tf.summary.image('output_uncert_final', dh_sigma_final)
        if self.unet_depth:
            input_batch_flat, image_matrix = self.reshape_input(img_depth)
            self.calc_mul_gen_loss_unet(dh_sigma_final, dh_sigma, image_matrix, input_batch_flat)
            self.calc_mul_sem_loss_unet(img_seman, sem_mid)
        else:
            self.graph(img_depth, mid_list, dh_sigma_final, batch_truth = img_seman, dh_sigma=dh_sigma, sem_mid_unet=sem_mid )


        # self.total_loss = self.sem_unet_loss + self.cost


        loop = 200
        continue_train = False
        toy_test = False
        part_restore = False
        if continue_train:
            initial_iter = 7801
            stage = initial_iter//loop
        else:
            initial_iter = 0
            stage = 0

        if not part_restore:
            vars_to_freeze = [v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v.name.split('/')[0] == 'unet' and ('zero_padding' in v.name.split('/')[1] or \
            ('stage' in v.name.split('/')[1] and 'decoder' not in v.name.split('/')[1]) or 'conv0'in v.name.split('/')[1] or 'relu0'in v.name.split('/')[1] \
            or 'bn0'in v.name.split('/')[1] or 'pooling0'in v.name.split('/')[1] ) ]
        else:
            vars_to_freeze = [v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v.name.split('/')[0] == 'unet']
        
        # for v in vars_to_freeze:
        #     print(v.name)
        print('variables frozen: %d'%(len(vars_to_freeze)))


        with tf.variable_scope("optimizer"):
            self.learning_rate = tf.placeholder(tf.float32, shape=[])
            # self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.total_loss)
            # self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.sem_final_loss)
            # self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.sem_unet_loss, 
            #     var_list=[v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v not in vars_to_freeze])
            if self.unet_depth:
                self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.sem_unet_loss + self.generation_loss_unet, 
                var_list=[v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v not in vars_to_freeze])
            else:
                self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.cost, 
                    var_list=[v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v not in vars_to_freeze])

        merged_summary = tf.summary.merge_all()
        writer = tf.summary.FileWriter('.')
        writer.add_graph(tf.get_default_graph())

        saver = tf.train.Saver()

        if part_restore:
            variables_to_restore = [v for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if v.name.split('/')[-1] is not 'Adam' ]
            saver_res = tf.train.Saver(variables_to_restore)

        with tf.Session() as sess:
            if continue_train:
                if part_restore:
                    sess.run(tf.global_variables_initializer())
                    print("All variables initialized")
                    saver_res.restore(sess, "/home/" + self.usr_name + "/vae_depth/models/model%d.ckpt"%(stage))
                else:
                    saver.restore(sess, "/home/" + self.usr_name + "/vae_depth/models/model%d.ckpt"%(stage))
                print("Model %d restored."%(stage))
            else:
                sess.run(tf.global_variables_initializer())
                print("All variables initialized")

            train_handle = sess.run(self.train_iter.string_handle()) 
            valid_handle = sess.run(self.valid_iter.string_handle())
            if toy_test:
                eval_handle = train_handle
            else:
                eval_handle = valid_handle

            offset_iter = 0
            skip_iter = 0
            rate = 0.0001
            for iter in range(initial_iter - skip_iter, 10001):
                new_gray, new_depth, new_seman_raw = sess.run((next_batch_gray, next_batch_depth, next_batch_seman
                ), feed_dict = {self.handle : train_handle})

                if iter < initial_iter:
                    if iter % 10 == 0:
                        new_gray, new_depth, new_seman_raw = sess.run((next_batch_gray, next_batch_depth, next_batch_seman
                        ), feed_dict = {self.handle : eval_handle})
                        if iter % 100 == 0:
                            print("data mini-batch %d skipped"%(iter - (initial_iter - skip_iter) + 1))
                    continue

                if iter > loop*stage:
                    stage = stage + 1
                if iter >= 10000 + offset_iter:
                    rate = 0.00001
                if iter >= 15000 + offset_iter:
                    rate = 0.000001

                tf.summary.scalar("learning_rate", rate)

                sess.run(self.optimizer, feed_dict = {self.learning_rate : rate, 
                img: new_gray, img_depth : new_depth, img_seman : new_seman_raw})

                if iter % 10 == 0:
                    new_gray, new_depth, new_seman_raw = sess.run((next_batch_gray, next_batch_depth, next_batch_seman
                    ), feed_dict = {self.handle : eval_handle})

                    if self.unet_depth:
                        s, gen_loss, sem_unet_loss = sess.run((merged_summary, self.generation_loss_unet, self.sem_unet_loss), 
                        feed_dict = {img: new_gray, img_depth : new_depth, img_seman : new_seman_raw})
                        writer.add_summary(s, iter)
                        print("iter %d: genloss %f, sem_unet_loss %f, rate: %f" % (iter, gen_loss, sem_unet_loss, rate))
                    else:
                        s, gen_loss, sem_cost, lat_loss, sem_fi_loss, tot_loss = sess.run((
                        merged_summary, self.generation_loss, self.seman_cost, self.latent_loss, self.sem_unet_loss, self.cost), 
                        feed_dict = {img: new_gray, img_depth : new_depth, img_seman : new_seman_raw})

                        writer.add_summary(s, iter)

                        print("iter %d: gen_loss %f, sem_cost %f, lat_loss %f, sem_fi_loss %f, tot_loss %f, rate: %f" % (iter, 
                        gen_loss, sem_cost, lat_loss, sem_fi_loss, tot_loss, rate))

                    # if continue_train and (seman_loss > 20000000 or gen_loss > 2000000):
                    #     saver.restore(sess, "/home/" + self.usr_name + "/vae_depth/models/model%d.ckpt"%(stage-1))
                    #     print("Model %d restored."%(stage-1))
                    #     iter = loop*(stage-1) + 1
                    #     continue

                    # Save the variables to disk.
                    if iter % loop == 0:
                        save_path = saver.save(sess, "/home/" + self.usr_name + "/vae_depth/models/model%d.ckpt"%(stage))
                        print("Model saved in path: %s" % save_path)

                    # s, sem_fi_loss, = sess.run((
                    # merged_summary, self.sem_final_loss), 
                    # feed_dict = {img: new_gray, img_depth : new_depth, img_seman : new_seman_raw})

                    # writer.add_summary(s, iter)

                    # print("iter %d: sem_fi_loss %f, rate: %f" % (iter, 
                    # sem_fi_loss, rate))



    def graph(self, next_batch, dh_list = None, dh_last = None, batch_truth = None, dh_sigma = None, dh_list_last = None, sem_mid_unet=None):
        self.n_z = 256 # 20
        
        if dh_list is None and dh_last is None:
            # Not-conditioned-on-intensity version
            with tf.variable_scope("depth"):
                input_batch_flat, image_matrix = self.reshape_input(next_batch)
                z_mean, z_stddev = self.recognition(image_matrix)
                guessed_z = self.sampling(z_mean, z_stddev)
                generated_images = self.generation(guessed_z)
                generated_flat = self.reshape_output(generated_images, 'depth')
                self.generation_loss = self.calc_gen_loss(input_batch_flat, generated_flat, power = 0, uncertainty = None)
                self.latent_loss, self.cost = self.calc_losses(z_mean, z_stddev, self.generation_loss)
        else:
            # Conditioned-on-intensity version
            with tf.variable_scope("depth"):
                input_batch_flat, image_matrix = self.reshape_input(next_batch)

                z_mean, z_stddev = self.recognition_cond(image_matrix, dh_list)
                guessed_z = self.sampling(z_mean, z_stddev)
                if self.sem_same_decode:
                    generated_images, mid_gen_list, generated_seman, mid_seman = self.generation_cond(guessed_z, dh_list)
                else:
                    generated_images, mid_gen_list = self.generation_cond(guessed_z, dh_list)

                    if self.sem_decode:
                        generated_seman, mid_seman = self.generation_cond(guessed_z, dh_list, name="sem")
                    elif self.sem_on_depth:
                        generated_seman, mid_seman = self.sem_gen_blocks(generated_images, mid_gen_list, dh_list, dh_list_last)


            if self.sem_on_depth:
                self.calc_mul_gen_loss(generated_images, dh_last, mid_gen_list, dh_sigma, image_matrix, input_batch_flat)
            else:
                self.calc_mul_gen_loss(generated_images, dh_last, mid_gen_list, dh_list, image_matrix, input_batch_flat)

            if self.sem_decode:                    
                self.calc_mul_sem_loss(generated_seman, mid_seman, batch_truth, sem_mid_unet=sem_mid_unet)
                self.calc_losses(z_mean, z_stddev)
            elif self.sem_on_depth:
                self.calc_mul_sem_loss(generated_seman, mid_seman, batch_truth)
                self.calc_losses(z_mean, z_stddev)
            else:
                self.calc_losses(z_mean, z_stddev)

    # def hadamard_product_sum_output_shape(input_shapes):
    #     shape1 = list(input_shapes[0])
    #     shape2 = list(input_shapes[1])
    #     assert shape1 == shape2  # else hadamard product isn't possible
    #     return [tuple(shape1), tuple(shape2[:-1])]

    def read_carla_tfrecord(self):
        # file path
        home = os.path.expanduser('~')
        # carla_path = os.path.join(home, 'catkin_ws', 'src', 'carla', 'PythonClient', '_out', 'episode_*') # episode_0000
        # /media/minghanz/Seagate Backup Plus Drive/CARLA/_out
        # sys_root = 
        carla_path = os.path.join('/media', self.usr_name, '63d031f4-57ad-49b5-bd21-2fc983f585b5', 'data', 'CARLA_tfrecords')
        # carla_path = os.path.join('/media', self.usr_name, 'Seagate Backup Plus Drive', 'CARLA_tfrecords')
        tfrecord_train = os.path.join(carla_path, 'images_train.tfrecords')
        tfrecord_val = os.path.join(carla_path, 'images_val.tfrecords')

        raw_train_set = tf.data.TFRecordDataset(tfrecord_train)
        raw_val_set = tf.data.TFRecordDataset(tfrecord_val)

        # feature = {
        # 'img_depth': _bytes_feature(image_string_depth),
        # 'img_rgb': _bytes_feature(image_string_rgb),
        # 'img_seman': _bytes_feature(image_string_seman),
        # }
        # Create a dictionary describing the features.  
        self.image_feature_description = {
            'img_depth': tf.FixedLenFeature([], tf.string),
            'img_rgb': tf.FixedLenFeature([], tf.string),
            'img_seman': tf.FixedLenFeature([], tf.string),
        }

        parsed_train_set = raw_train_set.map(self._parse_image_function)
        parsed_val_set = raw_val_set.map(self._parse_image_function)

        # extract training data
        batched_train_set = parsed_train_set.repeat().batch(self.batchsize)
        self.train_iter = batched_train_set.make_one_shot_iterator()
        # train_iter_next = self.train_iter.get_next()

        # extract validation data
        batched_valid_set = parsed_val_set.repeat().batch(self.batchsize)
        self.valid_iter = batched_valid_set.make_one_shot_iterator()

        # construct a common iterator handle to allow switching between iterators of different dataset
        self.handle = tf.placeholder(tf.string, shape=[])
        iter_generic = tf.data.Iterator.from_string_handle(self.handle, batched_train_set.output_types, batched_train_set.output_shapes) 
        self.ne_el = iter_generic.get_next()

        # tf.summary.image("input_depth", self.ne_el[0])
        # tf.summary.image("input_gray", self.ne_el[1])
        # tf.summary.image("input_seman", ne_el[2][..., 7])
        return self.ne_el[0], self.ne_el[1], self.ne_el[2]
        # return train_iter_next[0], train_iter_next[1], train_iter_next[2]
        


    def _parse_image_function(self, example_proto):
        # Parse the input tf.Example proto using the dictionary above.
        example =  tf.parse_single_example(example_proto, self.image_feature_description)
        img_depth = tf.cast(tf.image.decode_png(example['img_depth']), tf.float32)
        # img_rgb = tf.cast(tf.image.decode_png(example['img_rgb']), tf.float32)
        img_rgb = tf.image.decode_png(example['img_rgb'])
        img_seman = tf.cast(tf.image.decode_png(example['img_seman']), tf.float32)
        info_depth = self.extract_from_img(img_depth, 'depth')
        info_rgb = self.extract_from_img(img_rgb, 'intensity')
        info_seman = self.extract_from_img(img_seman, 'semantics')

        return (info_depth, info_rgb, info_seman)

    def extract_from_img(self, img, mode):
        # different processing method for files of different meanings
        if mode == "depth":
            code = img[...,0:1] + img[...,1:2]*256 + img[...,2:3]*256*256
            num = code * 1000/(256*256*256 - 1) 
            avg = 20
            num = avg / (num + avg)
            num_compact = tf.image.resize_images(num, [self.im_height, self.im_width])
        elif mode == "semantics":
            num = img[...,0:1]
            num_idx = tf.image.resize_images(num, [self.im_height, self.im_width])
            num_compact = tf.cast(tf.round(num_idx), tf.int32)
            # num_compact = tf.reshape(num_compact,[self.im_height, self.im_width])
            # num_compact = tf.one_hot(num_compact, self.seman_category)
        elif mode == "intensity":
            # num = tf.image.rgb_to_grayscale(img)
            # num = num / 255.0
            # img = img / 255.0
            num_compact = tf.image.resize_images(img, [self.im_height, self.im_width])
            num_compact = tf.reshape(num_compact, [self.im_height, self.im_width, 3])
        else:
            sys.exit('Error! Mode in extract_from_png_file not defined')
        return num_compact

    def resize_truth(self, batch_truth, mode = 'sem'):
        truth_list = [None]*3
        for i in range(3):
            with tf.variable_scope(mode + '_truth_%d'%(i+1)):
                smaller_imgs = tf.image.resize_bilinear(batch_truth, [int(int(batch_truth.get_shape()[1])/(2**(i+1))), int(int(batch_truth.get_shape()[2])/(2**(i+1)))])
                if(mode  == 'sem'):
                    smaller_imgs = tf.cast(tf.round(smaller_imgs), tf.int32)
                    smaller_imgs = tf.reshape(smaller_imgs,[-1, smaller_imgs.get_shape()[1], smaller_imgs.get_shape()[2]])
                    truth_list[i] = tf.one_hot(smaller_imgs, self.seman_category, name = "mid_seman_truth_%d"%(i+1))
                else:
                    truth_list[i] = smaller_imgs
        
        return truth_list

    # reshape input
    def reshape_input(self, batch_input, name = "reshape_input"):
        with tf.variable_scope(name):
            input_batch_flat = tf.reshape(batch_input, [-1, self.im_height * self.im_width], name = "truth_flat")
            if name is "reshape_rgb":
                image_matrix = tf.reshape(batch_input,[-1, self.im_height, self.im_width, 3], name = "mat")
            else:
                image_matrix = tf.reshape(batch_input,[-1, self.im_height, self.im_width, 1], name = "mat")

        return input_batch_flat, image_matrix

    # encoder when conditioned
    def recognition_cond(self, input_images, dh_list):
        # with tf.variable_scope('recognition'):

        # strided conv + relu + concat + conv + relu
        h = down_block_depth(input_images, dh_list[0], 'h_0')
        for i in range(4):
            h = down_block_depth(h, dh_list[i+1], 'h_%d'%(i+1))

        # reshape + fully connected
        h_flat = tf.layers.flatten(h, name = "h_flat")
        z_mean = tf.layers.dense(h_flat, self.n_z, kernel_initializer=tf.random_normal_initializer(stddev=0.02), name = "z_mean")
        z_stddev = tf.layers.dense(h_flat, self.n_z, kernel_initializer=tf.random_normal_initializer(stddev=0.02), name = "z_stddev")

        tf.summary.histogram("z_mean", z_mean)
        tf.summary.histogram("z_stddev", z_stddev)

        return z_mean, z_stddev

    #sampling
    def sampling(self, z_mean, z_stddev):
        samples = tf.random_normal([self.batchsize,self.n_z],0,1,dtype=tf.float32, name = "rand_normal")
        guessed_z = z_mean + (z_stddev * samples)

        return guessed_z

    def generation_cond(self, z, dh_list, name = ""):
        # with tf.variable_scope('generation'):

        # fully connected + reshape
        z_develop = tf.layers.dense(z, 8*6*512, name ='z_matrix'+name)
        h = tf.reshape(z_develop, [-1, 6, 8, 512])

        dep_list = [None]*5

        dep_res = [None]*3
        if self.sem_same_decode:
            dep_sem = [None]*3

        # concat + conv
        with tf.variable_scope('dh_4'+name):
            x_concat = tf.concat([h, dh_list[4], tf.multiply(h, dh_list[4])], -1)
            dep_list[4] = conv2d(x_concat, x_concat.get_shape()[-1], dh_list[4].get_shape()[-1], ker_size=1) # switch_lin

            dep_list[4] = conv2d(dep_list[4], dep_list[4].get_shape()[-1], dep_list[4].get_shape()[-1], 'conv_2') # switch_lin

        # upsampling + conv + concat + conv
        for i in range(3, -1, -1):
            dep_list[i] = up_block_depth(dep_list[i+1], dh_list[i], 'dh_%d'%(i)+name)
            if self.sem_on_depth:
                if i < 3:
                    with tf.variable_scope('dh_%d'%(i)+name+'output'):
                        if name is "":
                            dep_res[i] = conv2d(dep_list[i], dep_list[i].get_shape()[-1], int(int(dep_list[i].get_shape()[-1])/2), name="conv_1", ker_size=3)
                            dep_res[i] = conv2d(dep_res[i], dep_res[i].get_shape()[-1], 1, name="conv_2", ker_size=1)
                        else:
                            # dep_res[i] = conv2d(dep_list[i], dep_list[i].get_shape()[-1], self.seman_category, ker_size=1)
                            dep_res[i] = conv2d_nlrelu(dep_list[i], dep_list[i].get_shape()[-1], int(dep_list[i].get_shape()[-1])/2)
                            dep_res[i] = conv2d(dep_res[i], dep_res[i].get_shape()[-1], self.seman_category, ker_size=1)

                    if self.sem_same_decode:
                        with tf.variable_scope('dh_%d'%(i)+'sem'+'output'):
                                dep_sem[i] = conv2d_nlrelu(dep_list[i], dep_list[i].get_shape()[-1], int(int(dep_list[i].get_shape()[-1])/2), ker_size=3)
                                dep_sem[i] = conv2d(dep_sem[i], dep_sem[i].get_shape()[-1], self.seman_category, ker_size=1)

        # transformed conv
        if name is "":
            # h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 1], "dh_last"+name)
            # h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 8], "dh_last"+name)
            # h_last = conv2d(h_last, 8, 1, "last_conv"+name, ker_size=3)
            with tf.variable_scope("dh_last"+name):
                h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 16] )
                h_last = conv2d(h_last, 16, 1, ker_size=1)
            
            h_last_clip = tf.clip_by_value(h_last, clip_value_min=0, clip_value_max=1)
            tf.summary.image("output_depth"+name, h_last_clip)

            if self.sem_same_decode:
                with tf.variable_scope("dh_last_sem"):
                    h_last_sem = deconv_relu(dep_list[0], [self.batchsize, self.im_height, self.im_width, 16])
                    h_last_sem = conv2d(h_last_sem, 16, self.seman_category, ker_size=1)
                    sem_softmax = tf.nn.softmax(h_last_sem)
                    tf.summary.image("output_sem", sem_softmax[..., 7:8])
        else:
            # h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 16], "dh_last"+name)
            # h_last = conv2d(h_last, 16, self.seman_category, 'conv_relu_2', ker_size=1)
            with tf.variable_scope("dh_last"+name):
                h_last = last_deconv(dep_list[0], [self.batchsize, self.im_height, self.im_width, 16])
                # h_last = conv2d(h_last, 16, self.seman_category, ker_size=1)
                h_last = conv2d_nlrelu(h_last, 16, 16)
                h_last = conv2d(h_last, 16, self.seman_category, ker_size=1)
            h_last_softmax = tf.nn.softmax(h_last)
            tf.summary.image("output_"+name, h_last_softmax[..., 7:8])

        if self.sem_on_depth:
            if self.sem_same_decode:
                return h_last, dep_res, h_last_sem, dep_sem
            else:
                return h_last, dep_res
        else:
            return h_last, dep_list
    # reshape output
    def reshape_output(self, batch_output, name, power = 0):
        with tf.variable_scope("reshape_output" + name):
            width = int(self.im_width / (2**power))
            height = int(self.im_height / (2**power))
            generated_flat = tf.reshape(batch_output, [self.batchsize, width * height], name = "generated_flat")

        return generated_flat

    def calc_mul_gen_loss_unet(self, generated_images, mid_gen_list, image_matrix, input_batch_flat):
        with tf.variable_scope('losses_gen'):
            with tf.variable_scope('loss_gen_block_0'):
                generated_flat = self.reshape_output(generated_images, 'depth')
                gen_loss_final = self.calc_gen_loss(input_batch_flat, generated_flat, 0)

            gen_flat_list = [None]*3
            ori_flat_list = [None]*3
            gen_loss_list = [None]*3

            for i in range(3):
                with tf.variable_scope('loss_gen_block_%d'%(i+1)):
                    gen_flat_list[i] = self.reshape_output(mid_gen_list[i], 'depth', i + 1)
                    mid_gen_list_clip = tf.clip_by_value(mid_gen_list[i], clip_value_min=0, clip_value_max=1)
                    tf.summary.image('output_depth_%d'%(i+1), mid_gen_list_clip)

                    smaller_imgs = tf.image.resize_bilinear(image_matrix, [int(int(image_matrix.get_shape()[1])/(2**(i+1))), int(int(image_matrix.get_shape()[2])/(2**(i+1)))])
                    ori_flat_list[i] = self.reshape_output(smaller_imgs, 'truth', i+1)
                    gen_loss_list[i] = self.calc_gen_loss(ori_flat_list[i], gen_flat_list[i], i+1)
                    
                    tf.summary.histogram('output_depth_%d'%(i+1), gen_flat_list[i])
                    
            self.generation_loss_unet = gen_loss_final + gen_loss_list[0] + gen_loss_list[1] + gen_loss_list[2]


    def calc_mul_gen_loss(self, generated_images, dh_last, mid_gen_list, dh_list, image_matrix, input_batch_flat):
        with tf.variable_scope('losses_gen'):
            with tf.variable_scope('loss_gen_block_0'):
                generated_flat = self.reshape_output(generated_images, 'depth')
                generated_uncertainty = self.reshape_output(dh_last, 'intensity')
                gen_loss_final = self.calc_gen_loss(input_batch_flat, generated_flat, 0, generated_uncertainty)

            gen_flat_list = [None]*3
            uncertainty_flat_list = [None]*3
            ori_flat_list = [None]*3

            gen_loss_list = [None]*3

            for i in range(3):
                with tf.variable_scope('loss_gen_block_%d'%(i+1)):
                    # mid_gen_single_layer = conv2d(mid_gen_list[i], mid_gen_list[i].get_shape()[-1], 1)
                    if not self.sem_on_depth:
                        mid_gen_single_layer = conv2d(mid_gen_list[i], mid_gen_list[i].get_shape()[-1], int(int(mid_gen_list[i].get_shape()[-1])/2), name="conv_1", ker_size=3)
                        mid_gen_single_layer = conv2d(mid_gen_single_layer, mid_gen_single_layer.get_shape()[-1], 1, name="conv_2", ker_size=3)
                        
                        dh_single_layer = conv2d_nlrelu(dh_list[i], dh_list[i].get_shape()[-1], 1)

                        gen_flat_list[i] = self.reshape_output(mid_gen_single_layer, 'depth', i + 1)
                        uncertainty_flat_list[i] = self.reshape_output(dh_single_layer, 'uncertainty', i + 1)

                        mid_gen_single_layer_clip = tf.clip_by_value(mid_gen_single_layer, clip_value_min=0, clip_value_max=1)
                        tf.summary.image('output_depth_%d'%(i+1), mid_gen_single_layer_clip)
                        tf.summary.image('output_uncert_%d'%(i+1), dh_single_layer)
                    else:
                        gen_flat_list[i] = self.reshape_output(mid_gen_list[i], 'depth', i + 1)
                        uncertainty_flat_list[i] = self.reshape_output(dh_list[i], 'uncertainty', i + 1)

                        mid_gen_list_clip = tf.clip_by_value(mid_gen_list[i], clip_value_min=0, clip_value_max=1)
                        tf.summary.image('output_depth_%d'%(i+1), mid_gen_list_clip)
                        tf.summary.image('output_uncert_%d'%(i+1), dh_list[i])

                    smaller_imgs = tf.image.resize_bilinear(image_matrix, [int(int(image_matrix.get_shape()[1])/(2**(i+1))), int(int(image_matrix.get_shape()[2])/(2**(i+1)))])
                    ori_flat_list[i] = self.reshape_output(smaller_imgs, 'truth', i+1)

                    gen_loss_list[i] = self.calc_gen_loss(ori_flat_list[i], gen_flat_list[i], i+1, uncertainty_flat_list[i])

                    tf.summary.histogram('output_depth_%d'%(i+1), gen_flat_list[i])
                    tf.summary.histogram('output_uncert_%d'%(i+1), uncertainty_flat_list[i])
                    

            self.generation_loss = gen_loss_final + gen_loss_list[0] + gen_loss_list[1] + gen_loss_list[2]

    def calc_mul_sem_loss_unet(self, batch_truth, sem_mid_unet):
        with tf.variable_scope('losses_sem'):

            sem_true_list = [None]*3
            sem_loss_unet_list = [None]*3

            for i in range(3):
                with tf.variable_scope('loss_sem_block_%d'%(i+1)):
                    smaller_imgs = tf.image.resize_bilinear(batch_truth, [int(int(batch_truth.get_shape()[1])/(2**(i+1))), int(int(batch_truth.get_shape()[2])/(2**(i+1)))])
                    smaller_imgs = tf.cast(tf.round(smaller_imgs), tf.int32)
                    smaller_imgs = tf.reshape(smaller_imgs,[-1, smaller_imgs.get_shape()[1], smaller_imgs.get_shape()[2]])
                    sem_true_list[i] = tf.one_hot(smaller_imgs, self.seman_category, name = "mid_seman_truth_%d"%(i+1))
                    
                    sem_loss_unet = tf.losses.softmax_cross_entropy(sem_true_list[i], sem_mid_unet[i])
                    sem_mid_unet_softmax = tf.nn.softmax(sem_mid_unet[i])
                    tf.summary.image("output_sem_unet%d"%(i+1), sem_mid_unet_softmax[..., 7:8])
                    sem_loss_unet_list[i] = sem_loss_unet
  
            self.sem_unet_loss = sem_loss_unet_list[0] + sem_loss_unet_list[1] + sem_loss_unet_list[2]
            self.sem_unet_loss = self.sem_unet_loss + self.sem_final_loss
            self.sem_unet_loss = self.sem_coef * self.sem_unet_loss


    def calc_mul_sem_loss(self, generated_images, mid_gen_list, batch_truth, sem_mid_unet=None):
        with tf.variable_scope('losses_sem'):
            with tf.variable_scope('loss_sem_block_0'):
                batch_truth_dim3 = tf.reshape(batch_truth, [-1, self.im_height, self.im_width])
                seman_matrix = tf.one_hot(batch_truth_dim3, self.seman_category, name = "truth_seman")
                tf.summary.image("input_seman", seman_matrix[..., 7:8])
                seman_cost_final = tf.losses.softmax_cross_entropy(seman_matrix, generated_images)

            sem_loss_list = [None]*3
            sem_pred_list = [None]*3
            sem_true_list = [None]*3

            if sem_mid_unet is not None:
                sem_loss_unet_list = [None]*3

            for i in range(3):
                with tf.variable_scope('loss_sem_block_%d'%(i+1)):
                    smaller_imgs = tf.image.resize_bilinear(batch_truth, [int(int(batch_truth.get_shape()[1])/(2**(i+1))), int(int(batch_truth.get_shape()[2])/(2**(i+1)))])
                    smaller_imgs = tf.cast(tf.round(smaller_imgs), tf.int32)
                    smaller_imgs = tf.reshape(smaller_imgs,[-1, smaller_imgs.get_shape()[1], smaller_imgs.get_shape()[2]])
                    sem_true_list[i] = tf.one_hot(smaller_imgs, self.seman_category, name = "mid_seman_truth_%d"%(i+1))
                    
                    if not self.sem_on_depth:
                        sem_pred_list[i] = conv2d(mid_gen_list[i], mid_gen_list[i].get_shape()[-1], self.seman_category, 'mid_seman_pred_%d'%(i+1), ker_size=1)
                        sem_loss_list[i] = tf.losses.softmax_cross_entropy(sem_true_list[i], sem_pred_list[i])
                        sem_softmax = tf.nn.softmax(sem_pred_list[i])
                        tf.summary.image('output_sem_%d'%(i+1), sem_softmax[...,7:8])
                    else:
                        sem_loss_list[i] = tf.losses.softmax_cross_entropy(sem_true_list[i], mid_gen_list[i])
                        sem_softmax = tf.nn.softmax(mid_gen_list[i])
                        tf.summary.image('output_sem_%d'%(i+1), sem_softmax[...,7:8])

                        if sem_mid_unet is not None: 
                            sem_loss_unet = tf.losses.softmax_cross_entropy(sem_true_list[i], sem_mid_unet[i])
                            sem_mid_unet_softmax = tf.nn.softmax(sem_mid_unet[i])
                            tf.summary.image("output_sem_unet%d"%(i+1), sem_mid_unet_softmax[..., 7:8])
                            sem_loss_unet_list[i] = sem_loss_unet
                    
                    
            if sem_mid_unet is not None:     
                self.sem_unet_loss = self.sem_final_loss + sem_loss_unet_list[0] + sem_loss_unet_list[1] + sem_loss_unet_list[2]
            else: 
                self.sem_unet_loss = self.sem_final_loss
            self.sem_unet_loss = self.sem_coef * self.sem_unet_loss

            self.seman_cost = seman_cost_final + sem_loss_list[0] + sem_loss_list[1] + sem_loss_list[2]
            self.seman_cost = self.sem_coef * self.seman_cost

            # semantic_loss = seman_cost_final + sem_loss_list[0] + sem_loss_list[1] + sem_loss_list[2]
            # semantic_loss = sem_loss_list[0] + sem_loss_list[1] + sem_loss_list[2]

            
    def calc_gen_loss(self, batch_truth, batch_pred, power = 0, uncertainty = None):
        with tf.variable_scope('loss_gen_%d'%(power)):
            if uncertainty is None:
                generation_loss = tf.reduce_sum( tf.abs(batch_truth - batch_pred), 1, name = "loss_gen" )
            else:
                generation_loss = tf.multiply(tf.reduce_sum( tf.div(tf.abs(batch_truth - batch_pred), self.dep_sigma_offset + uncertainty) + tf.log(self.dep_sigma_offset + uncertainty), 1), 4**power, name = "loss_gen" )

            generation_loss = tf.reduce_mean(generation_loss) # average over the batch size

            return generation_loss



    def calc_losses(self, z_mean, z_stddev):
        # with tf.variable_scope("loss_gen"):
            # generation_loss = tf.negative( tf.reduce_sum(batch_truth * tf.log(1e-8 + batch_pred) + 
            # (1-batch_truth) * tf.log(1e-8 + 1 - batch_pred), 1 ), name = "loss_gen" ) 

        with tf.variable_scope("loss_lat"):
            latent_loss = tf.multiply(tf.reduce_sum(tf.square(z_mean) + tf.square(z_stddev) - tf.log(tf.square(z_stddev)) - 1,1), 0.5, name = "loss_lat")
            self.latent_loss = tf.reduce_mean(latent_loss) # average over the batch size

        with tf.variable_scope("cost"):
            if self.all_at_once or self.sem_decode or self.sem_on_depth:
                if self.sem_unet:
                    self.cost = tf.add_n([self.generation_loss, self.latent_loss, self.seman_cost, self.sem_unet_loss], name = "cost")
                    tf.summary.scalar("sem_loss_unet", self.sem_unet_loss)
                else: 
                    self.cost = tf.add_n([self.generation_loss, self.latent_loss, self.seman_cost], name = "cost")
                tf.summary.scalar("sem_loss", self.seman_cost)
                
            else:
                self.cost = tf.add(self.generation_loss + self.latent_loss, name = "cost")

        # tf.summary.scalar("lat_loss", tf.reduce_mean(latent_loss))
        tf.summary.scalar("lat_loss", self.latent_loss)
        tf.summary.scalar("gen_loss", self.generation_loss)
        tf.summary.scalar("tot_loss", self.cost)

def make_image(tensor):
    """
    Convert an numpy representation image to Image protobuf.
    Copied from https://github.com/lanpa/tensorboard-pytorch/
    """
    from PIL import Image
    print(tensor.shape)
    tensor = tensor*255
    height, width, channel = tensor.shape
    tensor = np.reshape(tensor, (height, width)).astype(np.uint8)

    image = Image.fromarray(tensor)
    import io
    output = io.BytesIO()
    image.save(output, format='PNG')
    image_string = output.getvalue()
    output.close()
    return tf.Summary.Image(height=height,
                        width=width,
                        colorspace=channel,
                        encoded_image_string=image_string)

class TensorBoardImage(keras.callbacks.Callback):
    def __init__(self, tag, generator):
        super().__init__() 
        self.tag = tag
        self.generator = generator

    def on_epoch_end(self, epoch, logs={}):
        # Load image
        frames_arr, landmarks = next(self.generator)

        # Take just 1st sample from batch
        frames_arr = frames_arr[0:1,...]

        predictions = self.model.predict(frames_arr)
        img_pred = predictions[0][..., 7:8]

        image = make_image(img_pred)
        summary = tf.Summary(value=[tf.Summary.Value(tag=self.tag, image=image)])
        writer = tf.summary.FileWriter('./logs')
        writer.add_summary(summary, epoch)
        writer.close()

        return

vae_model = vae_depth()