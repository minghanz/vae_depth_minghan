import numpy as np
import tensorflow as tf
import sys

# def conv_transpose(x, outputShape, name):
#     with tf.name_scope(name):
#         # h, w, out, in
#         w = tf.Variable("w",[5,5, outputShape[-1], x.get_shape()[-1]], initializer=tf.truncated_normal_initializer(stddev=0.02))
#         b = tf.Variable("b",[outputShape[-1]], initializer=tf.constant_initializer(0.0))
#         convt = tf.nn.conv2d_transpose(x, w, output_shape=outputShape, strides=[1,2,2,1])
#         return convt

# def deconv2d(input_, output_shape,
#              k_h=5, k_w=5, d_h=2, d_w=2, stddev=0.02,
#              name="deconv2d"):
#     with tf.name_scope(name):
#         # filter : [height, width, output_channels, in_channels]
#         w = tf.Variable('w', [k_h, k_h, output_shape[-1], input_.get_shape()[-1]],
#                             initializer=tf.random_normal_initializer(stddev=stddev))

#         deconv = tf.nn.conv2d_transpose(input_, w, output_shape=output_shape, strides=[1, d_h, d_w, 1])

#         biases = tf.Variable('biases', [output_shape[-1]], initializer=tf.constant_initializer(0.0))
#         deconv = tf.reshape(tf.nn.bias_add(deconv, biases), deconv.get_shape())

#         return deconv

# leaky reLu unit
# def lrelu(x, leak=0.01, name="lrelu"):
#     with tf.name_scope(name):
#         f1 = 0.5 * (1 + leak)
#         f2 = 0.5 * (1 - leak)
#         return f1 * x + f2 * abs(x)

# # fully-conected layer
# def dense(x, inputFeatures, outputFeatures, scope=None, with_w=False):
#     with tf.name_scope(scope or "Linear"):
#         matrix = tf.Variable("Matrix", [inputFeatures, outputFeatures], tf.float32, tf.random_normal_initializer(stddev=0.02))
#         bias = tf.Variable("bias", [outputFeatures], initializer=tf.constant_initializer(0.0))
#         if with_w:
#             return tf.matmul(x, matrix) + bias, matrix, bias
#         else:
#             return tf.matmul(x, matrix) + bias


# # up block of depth vae decoder
# def resize_conv2d(x, name):
#     with tf.name_scope(name):
#         enlarged_x = tf.image.resize_bilinear(x, [int(x.get_shape()[1])*2, int(x.get_shape()[2])*2], name = "bilinear" )

#         w = tf.Variable("w",[5,5, enlarged_x.get_shape()[-1], int(enlarged_x.get_shape()[-1])/2], initializer=tf.truncated_normal_initializer(stddev=0.02))
#         b = tf.Variable("b",[int(enlarged_x.get_shape()[-1])/2], initializer=tf.constant_initializer(0.0))

#         conved_x = tf.nn.conv2d(enlarged_x, w, strides=[1,1,1,1], padding="SAME") + b
        
#         return conved_x


# standard convolution layer
def conv2d_strided(x, inputFeatures, outputFeatures, name = "conv_strd"):
    with tf.name_scope(name):
        # w = tf.Variable("w",[5,5,inputFeatures, outputFeatures], initializer=tf.truncated_normal_initializer(stddev=0.02))
        # b = tf.Variable("b",[outputFeatures], initializer=tf.constant_initializer(0.0))
        # conv = tf.nn.conv2d(x, w, strides=[1,2,2,1], padding="SAME") + b
        conv = tf.layers.conv2d(
        inputs = x,
        filters = outputFeatures,
        kernel_size = 5,
        strides=(2, 2),
        padding='same',
        data_format='channels_last',
        dilation_rate=(1, 1),
        activation=None,
        use_bias=True,
        kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
        bias_initializer=tf.zeros_initializer(),
        kernel_regularizer=None,
        bias_regularizer=None,
        activity_regularizer=None,
        kernel_constraint=None,
        bias_constraint=None,
        trainable=True,
        name=None,
        reuse=None
        )

        # tf.summary.histogram("w", w)
        # tf.summary.histogram("b", b)

        return conv


# down block of depth vae encoder
def conv2d_relu_strided(x, inputFeatures, outputFeatures, name = "conv_relu_strd"):
    with tf.name_scope(name):
        conved = conv2d_strided(x, inputFeatures, outputFeatures)

        conv_norm = batch_norm(conved)

        # actived = tf.nn.relu(conv_norm, "relu")
        actived = tf.nn.leaky_relu(conv_norm, 0.01)

        return actived

# down block of depth vae encoder
def conv2d_nlrelu_strided(x, inputFeatures, outputFeatures, name = "conv_nlrelu_strd"):
    with tf.name_scope(name):
        conved = conv2d_strided(x, inputFeatures, outputFeatures)

        conv_norm = batch_norm(conved)

        actived = tf.nn.relu(conv_norm, "relu")
        # actived = tf.nn.leaky_relu(conv_norm, 0.1)

        return actived

def conv2d(x, inputFeatures, outputFeatures, name = "conv", ker_size = 5):
    with tf.name_scope(name):
        # w = tf.Variable("w",[ker_size,ker_size,inputFeatures, outputFeatures], initializer=tf.truncated_normal_initializer(stddev=0.02))
        # b = tf.Variable("b",[outputFeatures], initializer=tf.constant_initializer(0.0))
        # conv = tf.nn.conv2d(x, w, strides=[1,1,1,1], padding="SAME") + b
        conv = tf.layers.conv2d(
        inputs = x,
        filters = outputFeatures,
        kernel_size = ker_size,
        strides=(1, 1),
        padding='same',
        data_format='channels_last',
        dilation_rate=(1, 1),
        activation=None,
        use_bias=True,
        kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
        bias_initializer=tf.zeros_initializer(),
        kernel_regularizer=None,
        bias_regularizer=None,
        activity_regularizer=None,
        kernel_constraint=None,
        bias_constraint=None,
        trainable=True,
        name=None,
        reuse=None
        )
        # if "dh0" in w.name or "dh1" in w.name or "dh2" in w.name:
        # if "intensity" in w.name:
        # tf.summary.histogram("w", w)
        # tf.summary.histogram("b", b)
        
        return conv

def batch_norm(x, name = "batch_norm", scale = False):
    with tf.name_scope(name):
        # mu = tf.Variable("mean", [x.get_shape()[-1]], initializer=tf.zeros_initializer())
        # sigma = tf.Variable("variance", [x.get_shape()[-1]], initializer=tf.ones_initializer())
        # beta = tf.Variable("offset", [x.get_shape()[-1]], initializer=tf.zeros_initializer())
        # epsi = 1e-4
        
        # if scale is True:
        #     gamma = tf.Variable("scale", [x.get_shape()[-1]], initializer=tf.ones_initializer())
        # else:
        #     gamma = None

        # x_norm = tf.nn.batch_normalization(x, mu, sigma, beta, scale = gamma, variance_epsilon = epsi, name=name)

        # tf.summary.histogram("mean", mu)
        # tf.summary.histogram("variance", sigma)
        # tf.summary.histogram("offset", beta)
        # if scale is True:
        #     tf.summary.histogram("scale", gamma)

        x_norm = tf.layers.batch_normalization(
        inputs=x,
        axis=-1,
        momentum=0.99,
        epsilon=0.001,
        center=True,
        scale=False,
        beta_initializer=tf.zeros_initializer(),
        gamma_initializer=tf.ones_initializer(),
        moving_mean_initializer=tf.zeros_initializer(),
        moving_variance_initializer=tf.ones_initializer(),
        beta_regularizer=None,
        gamma_regularizer=None,
        beta_constraint=None,
        gamma_constraint=None,
        training=False,
        trainable=True,
        name=None,
        reuse=None,
        renorm=False,
        renorm_clipping=None,
        renorm_momentum=0.99,
        fused=None,
        virtual_batch_size=None,
        adjustment=None
        )


        return x_norm


# conv-leaky_relu block in u-net
def conv2d_relu(x, inputFeatures, outputFeatures, name = "conv_relu"):
    with tf.name_scope(name):
        conved = conv2d(x, inputFeatures, outputFeatures)

        conv_norm = batch_norm(conved)

        # actived = tf.nn.relu(conv_norm, "relu")
        actived = tf.nn.leaky_relu(conv_norm, 0.01)
        
        return actived


# conv-relu block in u-net
def conv2d_nlrelu(x, inputFeatures, outputFeatures, name = "conv_nlrelu", ker_size = 5):
    with tf.name_scope(name):
        conved = conv2d(x, inputFeatures, outputFeatures, ker_size=ker_size)

        conv_norm = batch_norm(conved)

        actived = tf.nn.relu(conv_norm, "relu")
        
        return actived


# conv-relu block in u-net
def conv2d_sig(x, inputFeatures, outputFeatures, name = "conv_sig"):
    with tf.name_scope(name):
        conved = conv2d(x, inputFeatures, outputFeatures)

        conv_norm = batch_norm(conved, scale=True)

        actived = tf.nn.sigmoid(conv_norm)
        
        return actived

def conv2d_softmax(x, inputFeatures, outputFeatures, name = "conv_softmax", ker_size = 5):
    with tf.name_scope(name):
        conved = conv2d(x, inputFeatures, outputFeatures)

        actived = tf.nn.softmax(conved)
        
        return actived

# pool-conv-relu-conv-relu block in the down part of u-net 
def down_block(x, name = 'down_block'):
    with tf.name_scope(name):
        pooled = tf.layers.max_pooling2d(x, [2,2], 2)
        conved = conv2d_nlrelu(pooled, pooled.get_shape()[-1], int(pooled.get_shape()[-1])*2, name = 'conv_relu_1')
        conved = conv2d_nlrelu(conved, conved.get_shape()[-1], conved.get_shape()[-1], name = 'conv_relu_2')

        # conved = conv2d_nlrelu(conved, conved.get_shape()[-1], conved.get_shape()[-1], name = 'conv_relu_3')

        return conved


# upsamp-conv-relu block in the up part of u-net (only the up arrow)
def up_conv_relu(x, name = 'up_conv_relu'):
    with tf.name_scope(name):
        enlarged_x = tf.image.resize_bilinear(x, [int(x.get_shape()[1])*2, int(x.get_shape()[2])*2], name = "bilinear" )
        conved_x = conv2d_nlrelu(enlarged_x, enlarged_x.get_shape()[-1], int(enlarged_x.get_shape()[-1])/2)
        
        return conved_x

# upsamp-conv in the up block of depth vae decoder
def up_conv(x, name = 'up_conv'):
    with tf.name_scope(name):
        enlarged_x = tf.image.resize_bilinear(x, [int(x.get_shape()[1])*2, int(x.get_shape()[2])*2], name = "bilinear" )
        conved_x = conv2d(enlarged_x, enlarged_x.get_shape()[-1], int(enlarged_x.get_shape()[-1])/2)
        
        return conved_x


# upsamp-conv-relu-concat-conv-relu in the up part of u-net
def up_block(x, x_external, name = 'up_block'):
    with tf.name_scope(name):
        x_up = up_conv_relu(x)
        x_concat = tf.concat([x_up, x_external], -1)
        concat_conved = conv2d_nlrelu(x_concat, x_concat.get_shape()[-1], x_external.get_shape()[-1])

        # concat_conved = conv2d_nlrelu(x_concat, x_concat.get_shape()[-1], x_external.get_shape()[-1], name="conv_relu_2")

        return concat_conved


# last layer of depth vae
def last_deconv(x, outputShape, name = 'conv_transpose'):
    with tf.name_scope(name):
        # w = tf.Variable("w",[4,4, outputShape[-1], x.get_shape()[-1]], initializer=tf.truncated_normal_initializer(stddev=0.02))
        # # b = tf.Variable("b",[outputShape[-1]], initializer=tf.constant_initializer(0.0))
        # convt = tf.nn.conv2d_transpose(x, w, output_shape=outputShape, strides=[1,2,2,1])
        # # tf.print("w: ", w, output_stream=sys.stdout)
        # tf.summary.histogram("w", w)

        convt=tf.layers.conv2d_transpose(
        inputs=x,
        filters=outputShape[-1],
        kernel_size=4,
        strides=(2, 2),
        padding='same',
        data_format='channels_last',
        activation=None,
        use_bias=True,
        kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
        bias_initializer=tf.zeros_initializer(),
        kernel_regularizer=None,
        bias_regularizer=None,
        activity_regularizer=None,
        kernel_constraint=None,
        bias_constraint=None,
        trainable=True,
        name=None,
        reuse=None
        )


        return convt


def deconv_sig(x, outputShape, name = "deconv_sig"):
    with tf.name_scope(name):
        deconved = last_deconv(x, outputShape)

        conv_norm = batch_norm(deconved, scale=True)

        actived = tf.nn.sigmoid(conv_norm)

        return actived


def deconv_relu(x, outputShape, name = "deconv_relu"):
    with tf.name_scope(name):
        deconved = last_deconv(x, outputShape)

        conv_norm = batch_norm(deconved)

        actived = tf.nn.relu(conv_norm)

        return actived


# strided_conv-relu-concat-conv-relu in the down part of depth vae
def down_block_depth(x, x_external, name = 'down_block_depth'):
    with tf.name_scope(name):
        shrinked_x = conv2d_nlrelu_strided(x, x.get_shape()[-1], int(x_external.get_shape()[-1])/2)# x_external.get_shape()[-1])
        x_concat = tf.concat([shrinked_x, x_external], -1)
        concat_conved = conv2d_nlrelu(x_concat, x_concat.get_shape()[-1], x_external.get_shape()[-1])

        # concat_conved = conv2d_nlrelu(concat_conved, concat_conved.get_shape()[-1], concat_conved.get_shape()[-1], 'conv_relu_2')

        return concat_conved


# upsamp-conv-concat-conv in the up part of u-net
def up_block_depth(x, x_external, name = 'up_block_depth'):
    with tf.name_scope(name):
        x_up = up_conv(x) # switch_lin
        x_concat = tf.concat([x_up, x_external, tf.multiply(x_up, x_external)], -1)
        concat_conved = conv2d(x_concat, x_concat.get_shape()[-1], x_external.get_shape()[-1])  # switch_lin

        # concat_conved = conv2d(concat_conved, concat_conved.get_shape()[-1], concat_conved.get_shape()[-1], 'conv_2')  # switch_lin

        return concat_conved
