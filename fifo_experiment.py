from subprocess import Popen

import os
import struct
# import numpy as np
# import matplotlib.pyplot as plt

FIFO_OUT = "/home/minghanz/catkin_ws/src/carla/PythonClient/CARLAOK/from_py.fifo"
FIFO_IN =  "/home/minghanz/catkin_ws/src/carla/PythonClient/CARLAOK/from_mat.fifo"

def send_image(count):
    '''
    The function is to send data to matlab process through named pipe
    '''
    print("Entering sending images")
    # data_string = count.tostring()
    with open(FIFO_OUT, "wb") as f:
        # str_to_send = "hello %d"%(count)
        f.write(struct.pack("i", count) )
        # print(str_to_send)
    # print("Exiting sending images")

    print("Entering receiving lanes")
    # a = np.ndarray((3,),float)
    with open(FIFO_IN, "rb") as g:
        data = g.read(4)
        print(type(data))
        print(len(data))
        a = struct.unpack('i', data)
        print(a)
    #print("Exiting receiving lanes")

    return a

try:
    os.mkfifo(FIFO_OUT)
except OSError:
    pass
try:
    os.mkfifo(FIFO_IN)
except OSError:
    pass

for count in range(100):
    a = send_image(count + 1)