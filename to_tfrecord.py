import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
# from scipy.misc import imsave as ims
# from utils import *
# from ops import *

from tensorflow.python import debug as tf_debug

from random import shuffle
import glob
import time


# Write the raw image files to images.tfrecords.
# First, process the two images into tf.Example messages.
# Then, write to a .tfrecords file.

# def files_to_tfrecords(files_depth, files_rgb, files_seman, name="train"):
def files_to_tfrecords(files, name="train"):
    tfrecord_file = os.path.join(tfrecord_path, 'images_' + name +'.tfrecords')
    with tf.python_io.TFRecordWriter(tfrecord_file) as writer:
        print("Start writing " + tfrecord_file)
        print (time.strftime("%H:%M:%S"))
        iter = 0
        for file_depth, file_rgb, file_seman in files:
            if not (file_depth.split('/')[-1] == file_rgb.split('/')[-1] and file_rgb.split('/')[-1] == file_seman.split('/')[-1]):
                print("Three files are not synced. ")
                print(file_depth)
                print(file_rgb)
                print(file_seman)
                break

            tf_example = image_example(file_depth, file_rgb, file_seman)
            writer.write(tf_example.SerializeToString())
            iter = iter + 1
            if iter % 1000 == 0:
                print("%d files written to tfrecord"%(iter))
                print (time.strftime("%H:%M:%S"))
                print(file_depth)
                print(file_rgb)
                print(file_seman)
                

        print("Finished writing " + tfrecord_file)
        print("%d files in total."%(iter-1))
        print (time.strftime("%H:%M:%S"))

    
def split_train_val(files):
    # Divide the hata into 90% train, 10% validation
    files_train = files[0:int(0.9*len(files))]
    files_val = files[int(0.9*len(files)):]

    return files_train, files_val


def _bytes_feature(value):
  """Returns a bytes_list from a string / byte."""
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


# Create a dictionary with features that may be relevant.
def image_example(file_depth, file_rgb, file_seman):
#   image_shape = tf.image.decode_jpeg(image_string).shape
    # image_depth = tf.read_file(file_depth)
    # image_rgb = tf.read_file(file_rgb)
    # image_seman = tf.read_file(file_seman)
    image_string_depth = open(file_depth, 'rb').read()
    image_string_rgb = open(file_rgb, 'rb').read()
    image_string_seman = open(file_seman, 'rb').read()


    feature = {
        'img_depth': _bytes_feature(image_string_depth),
        'img_rgb': _bytes_feature(image_string_rgb),
        'img_seman': _bytes_feature(image_string_seman),
    }

    return tf.train.Example(features=tf.train.Features(feature=feature))

  


# file path
home = os.path.expanduser('~')
# carla_path = os.path.join(home, 'catkin_ws', 'src', 'carla', 'PythonClient', '_out', 'episode_*') # episode_0000
# /media/minghanz/Seagate Backup Plus Drive/CARLA/_out
# sys_root = 
usr_name = 'minghanz' # 'pingping' # 'minghanz'
# carla_path = os.path.join('/media', usr_name, 'Seagate Backup Plus Drive', 'CARLA', '_out', 'town01', 'episode_0000*')
# tfrecord_path = os.path.join('/media', usr_name, 'Seagate Backup Plus Drive', 'CARLA', 'small_subset')

# for transforming test sequence into carla records 
carla_path = os.path.join('/media', usr_name, 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02')
tfrecord_path = os.path.join('/media', usr_name, 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02', 'tf_records')

# specify rgb file folder and read intensity data from it
files_path_depth = os.path.join(carla_path, 'CameraDepth', '*.png')
files_path_rgb = os.path.join(carla_path, 'CameraRGB', '*.png')
files_path_seman = os.path.join(carla_path, 'CameraSemantics', '*.png')

shuffle_data = False  # shuffle the addresses before saving
# read addresses and labels from the 'train' folder
files_depth = sorted(glob.glob(files_path_depth))
files_rgb = sorted(glob.glob(files_path_rgb))
files_seman = sorted(glob.glob(files_path_seman))

print(len(files_depth))
print(len(files_rgb))
print(len(files_seman))
# print(files_depth[0].split('/')[-1])
# print(files_rgb[0].split('/')[-1])
# print(files_seman[0].split('/')[-1])
# print(files_depth[0].split('/')[-1] == files_rgb[0].split('/')[-1])
i = 0
i_diff = 0
for i in range(len(files_depth)):
    if not (files_depth[i].split('/')[-1] == files_rgb[i].split('/')[-1] and files_rgb[i].split('/')[-1] == files_seman[i].split('/')[-1]):
        i_diff = i_diff + 1
        print("i_diff: %d"%(i_diff))
        print(files_depth[i])
        print(files_rgb[i])
        print(files_seman[i])
    if i % 10000 == 0:
        print("i: %d"%(i))
        
print("lood ended")
print("i_diff: %d"%(i_diff))
print("i: %d"%(i))



# to shuffle data
c = list(zip(files_depth, files_rgb, files_seman))
if shuffle_data:
    shuffle(c)
    # files_depth, files_rgb, files_seman = zip(*c)

# # split train set and validation set
# c_train, c_val = split_train_val(c)

# # transform train set and validation set into tfrecords respectively
# files_to_tfrecords(c_train, name="train")
# files_to_tfrecords(c_val, name="val")

files_to_tfrecords(c, name="seq")

# ###################################### obselete
# files_depth_train, files_depth_val = split_train_val(files_depth)
# files_rgb_train, files_rgb_val = split_train_val(files_rgb)
# files_seman_train, files_seman_val = split_train_val(files_seman)

# files_to_tfrecords(files_depth_train, files_rgb_train, files_seman_train, name="train")
# files_to_tfrecords(files_depth_val, files_rgb_val, files_seman_val, name="val")
###################################################




