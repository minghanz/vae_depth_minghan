import os
import csv

################### generate pure gyro csv file from imu0.csv for the use in crisp (calibrating rotation between imu and camera)

csv_read_path = '/media/minghanz/Seagate_Backup_Plus_Drive2/cam_IMU_calib_data/medium/VINS_mono_fmt/imu0.csv'
csv_write_path = '/media/minghanz/Seagate_Backup_Plus_Drive2/cam_IMU_calib_data/medium/VINS_mono_fmt/imu0_gyro.csv'

with open(csv_read_path) as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=',') 
    ### automatically read the first line as keys to the dictionary
    ### https://realpython.com/python-csv/

    with open(csv_write_path, mode = 'w') as write_to:
        csv_writer = csv.writer(write_to, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                # print(f'Column names are {", ".join(row)}')
                print('Column names are ', *row)
                line_count += 1
                
            ### No need to skip the first line
            wx = row['omega_x']
            wy = row['omega_y']
            wz = row['omega_z']
            csv_writer.writerow([wx, wy, wz])
            line_count += 1
        print('Processed {:d} lines.'.format(line_count))