
# Pythono3 code to rename multiple  
# files in a directory or folder 
  
# importing os module 
import os 
import shutil

from files_read_imu_json_write_csv import gen_true_cam_stamp
# Function to rename multiple files 
def main(): 
    
#     files_folder = os.path.join('/media', 'minghanz', 'Seagate Backup Plus Drive', 'CARLA_frames', 'town02', 'episode_0002_01', 'CameraDepth') #'CameraRGB', 'image_0')
    # files_folder = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02', 'vae_depth_output_visualize') #'CameraRGB', 'image_0')
    # files_folder = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02', 'CameraRGB') #'CameraRGB', 'image_0')
    # files_to_folder = os.path.join('/media', 'minghanz', 'Seagate_Backup_Plus_Drive', 'CARLA', '_out', 'episode_0000_02', 'CameraRGB_renamed')
    # files_parent_folder = '/media/minghanz/Seagate_Backup_Plus_Drive/cam_IMU_calib_data/parking_lot'
    # files_parent_folder = '/media/minghanz/Seagate_Backup_Plus_Drive/cam_IMU_calib_data/large/images'
    files_parent_folder = '/media/minghanz/Seagate_Backup_Plus_Drive2/cam_IMU_calib_data/medium'
    files_folder = os.path.join(files_parent_folder, 'images', 'img')
    # files_to_folder = os.path.join(files_parent_folder, 'images','image_0')
    files_to_folder = os.path.join(files_parent_folder, 'VINS_mono_fmt','cam0')
    file_names = os.listdir(files_folder)

    ## Both methods are equivalent to sort files named in number but not padding zeros at the front
    # # method 1: name -> digit -> sorted arg ->sort names
    # file_name_digit = [int(file_names[i].split('.')[0]) for i in range(len(file_names))]
    # file_name_idx = sorted(range(len(file_name_digit)),key=file_name_digit.__getitem__)
    # file_names = [file_names[i] for i in file_name_idx]

    # method 2: sort names using digit as key
    file_names = sorted(file_names, key=lambda x:int(x.split('.')[0]) )

    # print(file_names)

    # read a txt file of names to be changed to for the files
    path_to_tstamps = os.path.join(files_parent_folder, 'images', 'timestamp.txt')
    path_to_clock_offset = os.path.join(files_parent_folder, 'clock_offset.json')
    t_stamps = gen_true_cam_stamp(path_to_clock_offset, path_to_tstamps)

    extension = file_names[0].split('.')[-1]
    for i, filename in enumerate(file_names): 
        src = os.path.join(files_folder, filename)
        # # name it as the sequential number padding zeros in front
        # target_name = "%06d."%(i)+extension

        # name it using name list in name_file
        target_name = "%d000."%(t_stamps[i])+extension

        dst = os.path.join(files_to_folder, target_name)

        # print(src + " " + dst)
        
        # rename() function will 
        # rename all the files 
        # os.rename(src, dst) 
        
        # copy the file to another place with a new name
        shutil.copy2(src, dst)
        if i%100==0:
            print(filename, '-->', target_name)
  
# Driver Code 
if __name__ == '__main__': 
      
    # Calling main() function 
    main() 
