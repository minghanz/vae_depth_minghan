# read sf motors position.json file (GPS position) and generate csv file in cartesian space (xyz)
import json
import os
import csv
import utm

def gen_pos_in_VINS_mono_fmt(pos_file_in, pos_file_out=None):
    with open(pos_file_in) as json_file:
        data = json.load(json_file)
        with open(pos_file_out, mode = 'w') as write_to:
            writer = csv.writer(write_to, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(['timestamp','x','y','z'])
            for p in data:
                altitude = p["altitude"]
                latitude = p["latitude"]
                longitude = p["longitude"]
                loc = utm.from_latlon(latitude, longitude)
                east = loc[0]
                north = loc[1]
                x = str(east) # right
                y = str(north) # front
                z = str(altitude) # up
                
                sec = p["header"]["stamp"]["secs"]
                sec_str = "%d"%(sec)
                nsec = p["header"]["stamp"]["nsecs"]
                nsec_str = "%09d"%(nsec)

                t_stamp_string = sec_str + nsec_str
                print(t_stamp_string)

                writer.writerow([t_stamp_string, x, y, z])

    return


def main():
    files_parent_folder = '/media/minghanz/Seagate_Backup_Plus_Drive2/cam_IMU_calib_data/medium'

    path_pos = os.path.join(files_parent_folder, 'position.json')
    path_pos_out = os.path.join(files_parent_folder, 'VINS_mono_fmt', 'pos.csv')
    gen_pos_in_VINS_mono_fmt(path_pos, path_pos_out)

# Driver Code 
if __name__ == '__main__': 
      
    # Calling main() function 
    main() 
