import cv2
import numpy as np
import os

def loadIntrCalib():
    K = np.array([[2295.1996, 0, 912.9091], [0, 2334.5939, 473.1027], [0, 0, 1]])
    d = np.array([-0.6383586846644567, 0.3206206065116029, 0.0005098941946709474, 0.010951854341725242])
    return K, d

def loadAndUndist(img_filepath, K, d):
    img = cv2.imread(img_filepath)
    img_undist = cv2.undistort(img, K, d)
    img = cv2.resize(img, (960, 540)) #size: (width, height)
    img_undist = cv2.resize(img_undist, (960, 540)) 
    cv2.imshow('original', img)
    cv2.imshow('undistorted', img_undist)
    cv2.waitKey(0)

def main():
    img_folder = '/media/minghanz/Seagate_Backup_Plus_Drive/cam_IMU_calib_data/open_area/image_0'
    K, d = loadIntrCalib()

    # img_filepath = os.path.join(img_folder, '000098.jpg')
    img_files = os.listdir(img_folder)
    for i, img_file in enumerate(img_files):
        img_filepath = os.path.join(img_folder, img_file)

        loadAndUndist(img_filepath, K, d)
        print(i)

# Driver Code 
if __name__ == '__main__': 
      
    # Calling main() function 
    main() 
