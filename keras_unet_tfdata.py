from segmentation_models import Unet
from segmentation_models.backbones import get_preprocessing
from keras.callbacks import TensorBoard
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
import sys

from time import time
import keras
from ops_k import *

class vae_depth():
    def __init__(self):
        self.conditioned = True # have U-Net part
        self.inten_only = False # only U-Net
        self.all_at_once = False # CodeSLAM + seman seg
        self.sem_decode = True # CodeSLAM + semantic decoder
        self.sem_on_depth = True
        self.sem_same_decode = True # only can be used when sem_on_depth and sem_decode are both true

        self.batchsize = 3

        # specifying size of image
        self.im_width = 256 # 28 256
        self.im_height = 192 # 28 192
        self.seman_category = 13
        self.n_z = 256

        self.usr_name = "minghanz"#'pingping' # 'minghanz'

        next_batch_depth, next_batch_gray, next_batch_seman = self.read_carla_tfrecord()

        # prepare data
        x = next_batch_gray
        y = next_batch_seman

        preprocessing_fn = get_preprocessing('resnet34')
        


        with tf.Session() as sess:
            train_handle = sess.run(self.train_iter.string_handle()) 
            valid_handle = sess.run(self.valid_iter.string_handle())

            def create_data_generator():
                while True:
                    newx, newy = sess.run((x, y), feed_dict = {self.handle : train_handle})
                    newx = preprocessing_fn(newx)
                    yield newx, newy

            def val_data_generator():
                while True:
                    newx, newy = sess.run((x, y), feed_dict = {self.handle : valid_handle})
                    newx = preprocessing_fn(newx)
                    yield newx, newy
            

            datagen = create_data_generator()
            dataval = val_data_generator()

            # prepare model
            # img_input = keras.layers.Input(shape=(self.im_height, self.im_width, 3), name='img_input')
            model = Unet(backbone_name='resnet34', encoder_weights='imagenet', classes=self.seman_category, activation='softmax', 
            input_shape=(self.im_height, self.im_width, 3), freeze_encoder=True)
            # unet_output = model(img_input)

            model.compile('Adam', 'categorical_crossentropy', ['categorical_accuracy'])
            # model.compile('Adam', 'binary_crossentropy', ['binary_accuracy'])

            tbi_callback = TensorBoardImage('Image Example', dataval)
            # tensorboard = TensorBoard(log_dir="logs/{}".format(time()), write_images=True)\

            # next_train_data = next(datagen)
            

            # train model
            # model.fit(x, y, epochs=10, steps_per_epoch=30, batch_size=self.batchsize)
            model.fit_generator(datagen, epochs=10, steps_per_epoch=30, callbacks=[tbi_callback]) #, callbacks=[tbi_callback], validation_data = dataval, validation_steps = 30, 


    def read_carla_tfrecord(self):
        # file path
        home = os.path.expanduser('~')
        # carla_path = os.path.join(home, 'catkin_ws', 'src', 'carla', 'PythonClient', '_out', 'episode_*') # episode_0000
        # /media/minghanz/Seagate Backup Plus Drive/CARLA/_out
        # sys_root = 
        carla_path = os.path.join('/media', self.usr_name, 'Seagate Backup Plus Drive', 'CARLA_tfrecords')
        tfrecord_train = os.path.join(carla_path, 'images_train.tfrecords')
        tfrecord_val = os.path.join(carla_path, 'images_val.tfrecords')

        raw_train_set = tf.data.TFRecordDataset(tfrecord_train)
        raw_val_set = tf.data.TFRecordDataset(tfrecord_val)

        # feature = {
        # 'img_depth': _bytes_feature(image_string_depth),
        # 'img_rgb': _bytes_feature(image_string_rgb),
        # 'img_seman': _bytes_feature(image_string_seman),
        # }
        # Create a dictionary describing the features.  
        self.image_feature_description = {
            'img_depth': tf.FixedLenFeature([], tf.string),
            'img_rgb': tf.FixedLenFeature([], tf.string),
            'img_seman': tf.FixedLenFeature([], tf.string),
        }

        parsed_train_set = raw_train_set.map(self._parse_image_function)
        parsed_val_set = raw_val_set.map(self._parse_image_function)

        # extract training data
        batched_train_set = parsed_train_set.repeat().batch(self.batchsize)
        self.train_iter = batched_train_set.make_one_shot_iterator()
        # train_iter_next = self.train_iter.get_next()

        # extract validation data
        batched_valid_set = parsed_val_set.repeat().batch(self.batchsize)
        self.valid_iter = batched_valid_set.make_one_shot_iterator()

        # construct a common iterator handle to allow switching between iterators of different dataset
        self.handle = tf.placeholder(tf.string, shape=[])
        iter_generic = tf.data.Iterator.from_string_handle(self.handle, batched_train_set.output_types, batched_train_set.output_shapes) 
        self.ne_el = iter_generic.get_next()

        tf.summary.image("input_depth", self.ne_el[0])
        tf.summary.image("input_gray", self.ne_el[1])
        # tf.summary.image("input_seman", ne_el[2][..., 7])
        return self.ne_el[0], self.ne_el[1], self.ne_el[2]
        # return train_iter_next[0], train_iter_next[1], train_iter_next[2]
        


    def _parse_image_function(self, example_proto):
        # Parse the input tf.Example proto using the dictionary above.
        example =  tf.parse_single_example(example_proto, self.image_feature_description)
        img_depth = tf.cast(tf.image.decode_png(example['img_depth']), tf.float32)
        # img_rgb = tf.cast(tf.image.decode_png(example['img_rgb']), tf.float32)
        img_rgb = tf.image.decode_png(example['img_rgb'])
        img_seman = tf.cast(tf.image.decode_png(example['img_seman']), tf.float32)
        info_depth = self.extract_from_img(img_depth, 'depth')
        info_rgb = self.extract_from_img(img_rgb, 'intensity')
        info_seman = self.extract_from_img(img_seman, 'semantics')

        return (info_depth, info_rgb, info_seman)

    def extract_from_img(self, img, mode):
        # different processing method for files of different meanings
        if mode == "depth":
            code = img[...,0:1] + img[...,1:2]*256 + img[...,2:3]*256*256
            num = code * 1000/(256*256*256 - 1) 
            avg = 20
            num = avg / (num + avg)
            num_compact = tf.image.resize_images(num, [self.im_height, self.im_width])
        elif mode == "semantics":
            num = img[...,0:1]
            num_idx = tf.image.resize_images(num, [self.im_height, self.im_width])
            num_compact = tf.cast(tf.round(num_idx), tf.int32)
            num_compact = tf.reshape(num_compact,[self.im_height, self.im_width])
            num_compact = tf.one_hot(num_compact, self.seman_category)
        elif mode == "intensity":
            # num = tf.image.rgb_to_grayscale(img)
            # num = num / 255.0
            # img = img / 255.0
            num_compact = tf.image.resize_images(img, [self.im_height, self.im_width])
        else:
            sys.exit('Error! Mode in extract_from_png_file not defined')
        return num_compact

    def resize_truth(self, batch_truth, mode = 'sem'):
        truth_list = [None]*3
        for i in range(3):
            with tf.name_scope(mode + '_truth_%d'%(i+1)):
                smaller_imgs = tf.image.resize_bilinear(batch_truth, [int(int(batch_truth.get_shape()[1])/(2**(i+1))), int(int(batch_truth.get_shape()[2])/(2**(i+1)))])
                if(mode  == 'sem'):
                    smaller_imgs = tf.cast(tf.round(smaller_imgs), tf.int32)
                    smaller_imgs = tf.reshape(smaller_imgs,[-1, smaller_imgs.get_shape()[1], smaller_imgs.get_shape()[2]])
                    truth_list[i] = tf.one_hot(smaller_imgs, self.seman_category, name = "mid_seman_truth_%d"%(i+1))
                else:
                    truth_list[i] = smaller_imgs
        
        return truth_list

def make_image(tensor):
    """
    Convert an numpy representation image to Image protobuf.
    Copied from https://github.com/lanpa/tensorboard-pytorch/
    """
    from PIL import Image
    print(tensor.shape)
    tensor = tensor*255
    height, width, channel = tensor.shape
    tensor = np.reshape(tensor, (height, width)).astype(np.uint8)

    image = Image.fromarray(tensor)
    import io
    output = io.BytesIO()
    image.save(output, format='PNG')
    image_string = output.getvalue()
    output.close()
    return tf.Summary.Image(height=height,
                        width=width,
                        colorspace=channel,
                        encoded_image_string=image_string)

class TensorBoardImage(keras.callbacks.Callback):
    def __init__(self, tag, generator):
        super().__init__() 
        self.tag = tag
        self.generator = generator

    def on_epoch_end(self, epoch, logs={}):
        # Load image
        frames_arr, landmarks = next(self.generator)

        # Take just 1st sample from batch
        frames_arr = frames_arr[0:1,...]

        predictions = self.model.predict(frames_arr)
        img_pred = predictions[0][..., 7:8]

        image = make_image(img_pred)
        summary = tf.Summary(value=[tf.Summary.Value(tag=self.tag, image=image)])
        writer = tf.summary.FileWriter('./logs')
        writer.add_summary(summary, epoch)
        writer.close()

        return

vae_model = vae_depth()