##### recursively copy all jpg files to a new folder using a new name according to the data and seq information in its path
import shutil
import os 
import sys
import glob

src_path = sys.argv[1]

target_path = sys.argv[2]

def find_date_seq(dir):
    folder_chain = dir.split('/')
    num_found = False
    pos = -1
    while True:
        num_found = any(char.isdigit() for char in folder_chain[pos] ) # check if the string include a digit
        if num_found:
            break
        pos -= 1
    num_seq = folder_chain[pos]
    print(num_seq)
    return num_seq


# ## collecting all images
# total_count = 0
# for root, dirs, files in os.walk(src_path):
#     if 'all_labelled' in root: # find a target folder 
#         # find out its sequence number
#         num_seq = find_date_seq(root)
        
#         # copy image files to a new location with sequence info appended ahead of the name
#         for file in files:
#             file_new_name = num_seq + '_' + file
#             # print(file_new_name)
#             total_count += 1
#             shutil.copyfile( os.path.join(root, file), os.path.join(target_path, file_new_name) )
# print('total_count', total_count)


## collecting all front_60_1.txt
# wanted_files = glob.glob(src_path +'/**/front_60_1.txt', recursive=True)
# for file in wanted_files:
#     print(file) 
total_count = 0
for root, dirs, files in os.walk(src_path):
    for file in files:
        if file == "front_60_1.txt":
            print(os.path.join(root, file))
            num_seq = find_date_seq(root)
            file_new_name = num_seq + '.txt'
            # print(file_new_name)
            shutil.copyfile( os.path.join(root, file), os.path.join(target_path, file_new_name) )
            total_count += 1

print('total_count', total_count)