### generate euroc style timestamp file from KITTI timestamp and image folder
import numpy as np
import os 
import shutil

import csv

def generate_tstamp_csv(image_folder, timestamp_path, out_file_path):

    with open(out_file_path, mode = 'w') as write_to:
        
        writer = csv.writer(write_to, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['#timestamp [ns]','filename'])
        
        file_name_list = os.listdir(image_folder)
        file_name_list = sorted(file_name_list)

        tstamp_list = []
        with open(timestamp_path) as tstamp_file:
            for line in tstamp_file.readlines():
                time = np.datetime64(line)
                tstamp = time.astype('O')
                tstamp_list.append(tstamp)

        for i, file_name in enumerate(file_name_list):
            num_part = tstamp_list[i]
            writer.writerow([num_part, file_name])
            print(file_name)
        

def main(): 
    files_parent_folder = '/media/minghanz/Seagate_Backup_Plus_Drive2/KITTI/raw_for_IMUcalib/2011_09_26/2011_09_26_drive_0001_extract/image_00'
    image_folder = os.path.join(files_parent_folder, 'data')
    timestamp_path = os.path.join(files_parent_folder, 'timestamps.txt')
    out_file_path = os.path.join(files_parent_folder, 'cam0_tstamp.csv')
    
    generate_tstamp_csv(image_folder, timestamp_path, out_file_path)

if __name__ == "__main__":
    main()