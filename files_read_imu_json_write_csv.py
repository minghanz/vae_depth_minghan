# read sf motors imu file and generate VINS-mono compatible imu file (similar to euroc style)
import json
import os
import csv

def load_json_clock_offset(filename):
    with open(filename) as json_file:
        data = json.load(json_file)

        t_list = []
        t_true_list = []
        
        for p in data:
            sec_off = int(p["clock_diff"]["sec"])
            nsec_off = int(p["clock_diff"]["nsec"])
            # t_offset = int(round(sec_off*1e6 + nsec_off*1e-3 ))

            sec = int(p["header"]["stamp"]["secs"])
            nsec = int(p["header"]["stamp"]["nsecs"])
            t = int(round(sec*1e6 + nsec*1e-3 ))

            isec_true = sec + sec_off
            nsec_true = nsec + nsec_off

            t_true = int(round(isec_true*1e6 + nsec_true*1e-3) )

            t_list.append(t)
            t_true_list.append(t_true)

            # print(t)
            # print(t_offset)
            # print(t_true)
            # print('-----------------------')

    return t_list, t_true_list
        
def load_timestamp_and_offset(path_file_timestamp, t_list, t_true_list):
    file_of_tstamp = open(path_file_timestamp, 'r')
    i_list = 0

    t_cam_list = []
    for line in file_of_tstamp:
        # sec = line.split('.')[0]
        # subsec = line.split('.')[1]
        # sublen = len(subsec)-1
        # nsec = round(int(subsec) * 10** (-(sublen-9)))
        # isec = round(int(sec))
        # t_now = int(isec*1e6 + nsec*1e-3)

        t_now = int(round(float(line)*1e6)) 

        # print(isec)
        # print(nsec)
        # print(t_now)
        # print(line)

        while i_list+1 < len(t_list) and t_now > t_list[i_list+1] :
            i_list += 1

        if t_list[i_list] < t_now and t_list[i_list+1] > t_now:
            t_cam = (t_now - t_list[i_list])/(t_list[i_list+1] - t_list[i_list]) * t_true_list[i_list+1] + \
                        (t_list[i_list+1] - t_now)/(t_list[i_list+1] - t_list[i_list]) * t_true_list[i_list]
            t_cam_list.append( round(t_cam) )
        elif t_now <= t_list[i_list]:
            t_cam_list.append(t_true_list[i_list] + t_now - t_list[i_list] )
        elif t_now >= t_list[i_list+1]:
            t_cam_list.append(t_true_list[i_list+1] + t_now - t_list[i_list+1] )

    file_of_tstamp.close()
    return t_cam_list

def gen_true_cam_stamp(clock_offset_file, tstamp_file):
    t_list, t_true_list = load_json_clock_offset(clock_offset_file)

    t_cam_list = load_timestamp_and_offset(tstamp_file, t_list, t_true_list)
    return t_cam_list


def gen_IMU_in_VINS_mono_fmt(imu_file_in, imu_file_out=None):
    with open(imu_file_in) as json_file:
        data = json.load(json_file)
        with open(imu_file_out, mode = 'w') as write_to:
            writer = csv.writer(write_to, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(['timestamp','omega_x','omega_y','omega_z','alpha_x','alpha_y','alpha_z'])
            for p in data:
                w0 = p["angular_velocity"][0]
                w1 = p["angular_velocity"][1]
                w2 = p["angular_velocity"][2]
                a0 = p["linear_acceleration"][0]
                a1 = p["linear_acceleration"][1]
                a2 = p["linear_acceleration"][2]
                
                sec = p["header"]["stamp"]["secs"]
                sec_str = "%d"%(sec)
                nsec = p["header"]["stamp"]["nsecs"]
                nsec_str = "%09d"%(nsec)

                t_stamp_string = sec_str + nsec_str
                print(t_stamp_string)

                writer.writerow([t_stamp_string, w0, w1, w2, a0, a1, a2])

    return




def main():
    files_parent_folder = '/media/minghanz/Seagate_Backup_Plus_Drive2/cam_IMU_calib_data/medium'

    # path_to_file_of_names = os.path.join(files_parent_folder, 'clock_offset.json')
    # t_list, t_true_list = load_json_clock_offset(path_to_file_of_names)

    # path_file_timestamp = os.path.join(files_parent_folder, 'images', 'timestamp.txt')
    # t_cam_list = load_timestamp_and_offset(path_file_timestamp, t_list, t_true_list)

    path_imu = os.path.join(files_parent_folder, 'imu.json')
    path_imu_out = os.path.join(files_parent_folder, 'VINS_mono_fmt', 'imu0.csv')
    gen_IMU_in_VINS_mono_fmt(path_imu, path_imu_out)

# Driver Code 
if __name__ == '__main__': 
      
    # Calling main() function 
    main() 
