import os

def remove_file(dir, file_list):
    for file in file_list:
        file_path = os.path.join(dir, file)
        print('Removing %s' %(file_path))
        if os.path.exists(file_path):
            os.remove(file_path)
        else:
            print("The file does not exist")

# file path
home = os.path.expanduser('~')
carla_path = os.path.join(home, 'catkin_ws', 'src', 'carla', 'PythonClient', '_out') # episode_0000

epi_folders = os.listdir(carla_path)
print(epi_folders)

for epis in epi_folders:
    epi_path = os.path.join(carla_path, epis)
    if os.path.isdir(epi_path):
        epi_depth_path = os.path.join(epi_path, 'CameraDepth')
        epi_RGB_path = os.path.join(epi_path, 'CameraRGB')
        epi_sem_path = os.path.join(epi_path, 'CameraSemSeg')
        
        files_depth = os.listdir(epi_depth_path)
        files_RGB = os.listdir(epi_RGB_path)
        files_sem = os.listdir(epi_sem_path)

        files_intersec = list( (set(files_depth).intersection(set(files_RGB))).intersection(set(files_sem)) )
        files_depth_only = list(set(files_depth).difference(set(files_intersec)))
        files_RGB_only = list(set(files_RGB).difference(set(files_intersec)))
        files_sem_only = list(set(files_sem).difference(set(files_intersec)))

        remove_file(epi_depth_path, files_depth_only)
        remove_file(epi_RGB_path, files_RGB_only)
        remove_file(epi_sem_path, files_sem_only)
        
        